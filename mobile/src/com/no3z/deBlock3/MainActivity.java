package com.no3z.deBlock3;
 
import java.util.logging.Level;
import java.util.logging.LogManager;
import com.jme3.app.AndroidHarness;
import com.jme3.system.android.AndroidConfigChooser.ConfigType;
import android.os.Bundle;
import android.content.pm.ActivityInfo;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.content.BroadcastReceiver;
import deBlock3.Controller;
import deBlock3.Main;
import tv.ouya.console.api.OuyaController;
import tv.ouya.console.api.OuyaIntent;
import tv.ouya.console.api.OuyaFacade;
import android.content.Context;
import android.content.Intent;
import com.jme3.renderer.android.TextureUtil;

public class MainActivity extends AndroidHarness{
 
    /*
     * Note that you can ignore the errors displayed in this file,
     * the android project will build regardless.
     * Install the 'Android' plugin under Tools->Plugins->Available Plugins
     * to get error checks and code completion for the Android project files.
     */
    static private boolean isOuyaDevice = OuyaFacade.getInstance().isRunningOnOUYAHardware();
         
    private int iPlayer = -1;
    
    public MainActivity(){
        // Set the application class to run
        appClass = "deBlock3.Main";
        // Try ConfigType.FASTEST; or ConfigType.LEGACY if you have problems
        eglConfigType = ConfigType.BEST;
        // Exit Dialog title & message
        handleExitHook = false;
        exitDialogTitle = "Exit deBlock3?";
        exitDialogMessage = "Press Yes";
        // Enable verbose logging
        eglConfigVerboseLogging = false;
        // Choose screen orientation
        screenOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        // Enable MouseEvents being generated from TouchEvents (default = true)
        mouseEventsEnabled = true;
        joystickEventsEnabled = true;
        // Set the default logging level (default=Level.INFO, Level.ALL=All Debug Info)
        LogManager.getLogManager().getLogger("").setLevel(Level.INFO);
        
        splashPicID = R.drawable.splash;      

        TextureUtil.ENABLE_COMPRESSION = false;
    }
 
    public class MyBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(!isOuyaDevice) return;
        if(intent.getAction().equals(OuyaIntent.ACTION_MENUAPPEARING)) {
             System.out.println("[deBlock3]: ACTION_MENUAPPEARING");
        }
    }
}
    
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        if(!isOuyaDevice) 
        {
            ((Main)this.getJmeApplication()).setTargetPlatform(Main.eTargetPlatform.ANDROID);
            return;
        }
        
        ((Main)this.getJmeApplication()).setTargetPlatform(Main.eTargetPlatform.OUYA);
        OuyaController.init(this);
    }
    
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) 
    {		
        if(isOuyaDevice)
        {
            int controllerId = event.getDeviceId();
            if(event.getAction() == 0){
                    OuyaController.onKeyDown(event.getKeyCode(), event);
            }
            if(event.getAction() == 1){
                    OuyaController.onKeyUp(event.getKeyCode(), event);
            }
            
            Controller controller = getController(controllerId);
            if(controller == null ) {  return super.dispatchKeyEvent(event); }
            if(!controller.ButtonStateEvent(event.getKeyCode(),event.getAction()))
                return super.dispatchKeyEvent(event);
            else 
                return true;
        }
        return super.dispatchKeyEvent(event);
    }			
    
    SparseArray<Controller> controllers = new SparseArray<Controller>();

    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
//        if(isOuyaDevice)
//        {
//            int controllerId = event.getDeviceId();
//            boolean handled = OuyaController.onGenericMotionEvent(event);
//            Controller controller = getController(controllerId);
//            if(controller == null ) {  return super.onGenericMotionEvent(event); }
//            controller.updateSticks();
//            return handled || super.onGenericMotionEvent(event);
//        }
        return super.onGenericMotionEvent(event);
    }
        
    private Controller getController(int deviceId) 
    {        
        Controller controller = controllers.get(deviceId);        
        int player = OuyaController.getPlayerNumByDeviceId(deviceId);      

        if (controller == null ) {
            if (deviceId < 0) {
                // no device id? Should probably throw an error here
                return null;
            }
            if(iPlayer == -1) {
                controller = new Controller(deviceId);

                if ( ( (Main)this.getJmeApplication()).addController(controller) ) {
                    controllers.put(deviceId, controller);
                    iPlayer = player;
                }
            }
        }
        if(iPlayer != -1){
            return controller;
        }
        else { return null; }
    }
    
}
