name=Guitar and Bass
midifile=guitarandbass.mid
audiofile=guitarandbass.ogg
stage_texture=Scroll(grid2.png;128;0.25)
bpm=80.0
player_model=box.j3o
player_tex=concrete.png
player_scale=0.8,0.8,0.8
player_texhit=concrete_hit.png
player_tex_revert=concrete_revert.png
tracks={0,4}:{2,5}:{8,4}:{16,3}:{24,4}
texts={2,Wilcomen to this level,2.0,32}:{8,Avoid them things!,1.0,26}:{16,Doing good man!,2.0,26}:{24,Watch out with those horns!,2.0,32}:{44,Good... good,2.0,32}
camera={0,9.0,4.0,-29.0,0.65014243,2.4500017,0.02499949,50.0,1.777,0.0}:{4,9.0,5.7200394,-27.440056,16.000042,1.2000008,-1.8676299E-7,50.0,1.777,0.0}:{8,12.540081,5.670038,-27.440056,6.400157,-15.074913,-1.06721714E-7,50.0,1.777,0.0}:{12,5.14,6.8700657,-27.440056,12.325098,14.999916,-2.1344337E-7,50.0,1.777,0.0}:{16,4.669901,3.88,-27.440056,11.900104,3.8000066,-2.1344343E-7,50.0,1.777,0.0}:{22,4.669901,8.060093,-27.440056,17.750015,3.800007,-1.06721714E-7,50.0,1.777,0.0}:{24,8.169981,9.850134,-27.440056,22.59994,3.800007,-2.1344343E-7,50.0,1.777,0.0}:{26,8.169981,9.850134,-27.440056,22.599941,2.4500012,5.8250127,50.0,1.777,0.0}:{28,8.169981,9.850134,-27.440056,22.599941,2.4500015,-6.000014,50.0,1.777,0.0}:{30,8.169981,9.850134,-27.440056,22.599941,2.4500015,0.024999509,50.0,1.777,0.0}:{36,8.169981,6.050047,-27.440056,17.075026,2.4500017,0.024999531,50.0,1.777,0.0}:{40,8.169981,9.500126,-27.440056,25.924889,2.4500015,0.024999555,50.0,1.777,0.0}:{46,8.169981,2.900001,-27.440056,0.65014243,2.4500012,0.024999484,50.0,1.777,0.0}
tr0_ranges=95.0,99.0
tr0={0,BAD,user(sceneinc=38.0'Y=1.0'Z=50.0;1.0'1.0'1.0;1.0'1.0'-50.0),(1.0'1.0'1.0),200.0,5.0,global/ouch.ogg,0,1,plane.j3o,forbidden.png}:{8,skip}:{34,BAD,user(sceneinc=38.0'Y=1.0'Z=50.0;1.0'1.0'1.0;1.0'1.0'-50.0),(1.0'1.0'1.0),200.0,5.0,global/ouch.ogg,0,1,plane.j3o,forbidden.png}:{44,skip}
tr1_ranges=24.0,46.0
tr1={0,NORMAL,user(user=-38.0'sin=1.0'range=-10.0;1.0'1.0'6.0;40.0'1.0'-1.0),(0.25'0.75'1.0),200.0,1.0,none,0,0,plane.j3o,NONE}:{4,GOOD,PosDirVel(sceneinc=5.0'Y=1.0'Z=30.0;1.0'1.0'1.0;1.0'1.0'-50.0),(1.0'1.0'1.0),200.0,1.0,global/powerup.ogg,0,0.25,plane.j3o,star.png}:{8,BAD,PosDirVel(trackpp=5.0'Y=5.0'Z=30.0;1.0'1.0'1.0;1.0'1.0'-50.0),(1.0'1.0'1.0),200.0,1.0,global/bocine.ogg,0,0.8,plane.j3o,forbidden.png}:{12,NORMAL,user(user=-38.0'sin=1.0'range=-10.0;1.0'1.0'6.0;40.0'1.0'-1.0),(0.25'0.75'1.0),200.0,1.0,none,0,0,box.j3o,NONE}:{16,NORMAL,user(user=-38.0'range=1.0'trackpp=15.0;1.0'1.0'6.0;40.0'1.0'-1.0),(0.25'0.75'2.0),200.0,0.05,none,0,0,box.j3o,NONE}:{22,GOOD,seek(range=5.0'Y=5.0'Z=30.0;1.0'1.0'1.0;1.0'1.0'1.0),(1.0'1.0'1.0),200.0,1.0,global/powerup.ogg,0,0.25,plane.j3o,star.png}:{24,skip}:{44,BAD,PosDirVel(sceneinc=5.0'Y=1.0'Z=30.0;1.0'1.0'1.0;1.0'1.0'-50.0),(1.0'1.0'1.0),200.0,1.0,global/ouch.ogg,0,1,plane.j3o,forbidden.png}
tr2_ranges=36.0,44.0
tr2={0,GOOD,PosDirVel(X=38.0'user=1.0'range=-5.0;1.0'1.0'6.0;-40.0'1.0'-1.0),(0.25'0.75'1.0),200.0,1.0,none,0,0,box.j3o,NONE}:{12,BAD,PosDirVel(sceneinc=10.0'Y=5.0'Z=30.0;1.0'1.0'1.0;1.0'1.0'-50.0),(1.0'1.0'1.0),200.0,1.0,global/ouch.ogg,0,1,plane.j3o,forbidden.png}:{16,BAD,user(trackpp=0.0'user=1.0'Z=30.0;1.0'1.0'1.0;1.0'1.0'-50.0),(1.0'1.0'1.0),200.0,1.0,global/ouch.ogg,0,1,plane.j3o,forbidden.png}:{24,skip}:{44,GOOD,PosDirVel(sceneinc=10.0'Y=1.0'Z=30.0;1.0'1.0'1.0;1.0'1.0'-50.0),(1.0'1.0'1.0),200.0,1.0,global/powerup.ogg,0,0.25,plane.j3o,star.png}
tr3_ranges=55.0,71.0
tr3={24,BAD,PosDirVel(sceneinc=30.0'Y=1.0'range=-30.0;1.0'1.0'1.0;1.0'1.0'-60.0),(1.0'1.0'1.0),200.0,1.0,global/ouch.ogg,0,1,plane.j3o,forbidden.png}:{40,GOOD,PosDirVel(sceneinc=30.0'Y=1.0'range=-30.0;1.0'1.0'1.0;1.0'1.0'-50.0),(1.0'1.0'1.0),200.0,1.0,global/powerup.ogg,0,0.25,plane.j3o,star.png}
tr4_ranges=74.0,83.0
tr4={32,HEAL,user(sceneinc=20.0'Y=10.0'Z=30.0;1.0'1.0'1.0;1.0'-14.75'-47.75),(1.0'2.0'1.0),200.0,100.0,global/powerup.ogg,0,0.25,plane.j3o,apple.png}:{34,GOOD,user(sceneinc=20.0'Y=10.0'Z=30.0;1.0'1.0'1.0;1.0'-14.75'-47.75),(1.0'2.0'1.0),200.0,100.0,global/powerup.ogg,0,0.25,plane.j3o,star.png}:{44,BAD,user(X=20.0'Y=5.0'Z=30.0;1.0'1.0'1.0;1.0'1.0'-50.0),(1.0'1.0'1.0),200.0,1.0,global/bocine.ogg,0,0.8,plane.j3o,forbidden.png}
