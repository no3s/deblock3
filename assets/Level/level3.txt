name=FunkGasBrotherTrucker
midifile=funkgas/funkgas.mid
audiofile=funkgas/funkgas.ogg
bpm=70.0
player_model=box.j3o
player_tex=concrete.png
player_scale=0.8,0.8,0.8
player_texhit=concrete_hit.png
player_tex_revert=concrete_revert.png
stage_model=floor.j3o
stage_model_scale=1.0,1.0,1.0
stage_model_translation=0.0,0.0,0.0
stage_texture=Scroll(funkgas/tarmac.png;256;0.825)
stage_sphere_texture=funkgas/envmap.png
stage_movement_size=7.5
tracks={0,4}
texts={2,Funk Gas,2.0,32}:{4,Brother Trucker!,2.0,32}
camera={0,12.154205,5.348007,-34.036716,7.925162,1.1000018,-0.5250033,49.87507,1.777,0.0}
tr0_ranges=0.0,127.0
tr0={0,NORMAL,PosDirVel(X=0.0'Y=1.0'Z=30.0;1.0'1.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,none,0.0,0.0,box.j3o,NONE,}
tr0_60={0,GOOD,PosDirVel(user=0.0'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/funk.wav,0.0,1.0,plane.j3o,funkgas/funk.png,}
tr0_62={0,GOOD,PosDirVel(user=7.5'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/funk.wav,0.0,1.0,plane.j3o,funkgas/funk.png,}
tr0_64={0,GOOD,PosDirVel(user=15.0'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/funk.wav,0.0,1.0,plane.j3o,funkgas/funk.png,}
tr0_65={0,GOOD,PosDirVel(user=22.5'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/funk.wav,0.0,1.0,plane.j3o,funkgas/funk.png,}
tr0_67={0,GOOD,PosDirVel(user=0.0'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/gas.wav,0.0,1.0,plane.j3o,funkgas/gas.png,}
tr0_69={0,GOOD,PosDirVel(user=7.5'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/gas.wav,0.0,1.0,plane.j3o,funkgas/gas.png,}
tr0_71={0,GOOD,PosDirVel(user=15.0'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/gas.wav,0.0,1.0,plane.j3o,funkgas/gas.png,}
tr0_72={0,GOOD,PosDirVel(user=22.5'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/gas.wav,0.0,1.0,plane.j3o,funkgas/gas.png,}
tr0_77={0,GOOD,PosDirVel(user=0.0'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/brother.wav,0.0,1.0,plane.j3o,funkgas/brother.png,}
tr0_79={0,GOOD,PosDirVel(user=7.5'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/brother.wav,0.0,1.0,plane.j3o,funkgas/brother.png,}
tr0_81={0,GOOD,PosDirVel(user=15.0'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/brother.wav,0.0,1.0,plane.j3o,funkgas/brother.png,}
tr0_83={0,GOOD,PosDirVel(user=22.5'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/brother.wav,0.0,1.0,plane.j3o,funkgas/brother.png,}
tr0_84={0,GOOD,PosDirVel(user=0.0'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/trucker.wav,0.0,1.0,plane.j3o,funkgas/trucker.png,}
tr0_86={0,GOOD,PosDirVel(user=7.5'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/trucker.wav,0.0,1.0,plane.j3o,funkgas/trucker.png,}
tr0_88={0,GOOD,PosDirVel(user=15.0'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/trucker.wav,0.0,1.0,plane.j3o,funkgas/trucker.png,}
tr0_89={0,GOOD,PosDirVel(user=22.5'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,funkgas/trucker.wav,0.0,1.0,plane.j3o,funkgas/trucker.png,}
tr1_ranges=0.0,127.0
tr1={0,BAD,user(sceneinc=7.5'user=7.5'Z=30.0;0.0'1.75'1.0;1.0'13.5'-62.5),(1.0'1.0'1.0),200.0,22.0,NONE,0.0,0.0,plane.j3o,funkgas/brother.png,}
tr1_72={0,BAD,PosDirVel(user=0.0'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,global/ouch.ogg,0.0,1.0,plane.j3o,forbidden.png,}
tr1_74={0,BAD,PosDirVel(user=7.5'Y=1.0'user=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,global/ouch.ogg,0.0,1.0,plane.j3o,forbidden.png,}
tr1_76={0,BAD,PosDirVel(user=15.0'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,global/ouch.ogg,0.0,1.0,plane.j3o,forbidden.png,}
tr1_77={0,BAD,PosDirVel(user=22.5'Y=1.0'Z=30.0;0.0'0.0'1.0;1.0'1.0'-62.5),(1.0'1.0'1.0),200.0,1.0,global/ouch.ogg,0.0,1.0,plane.j3o,forbidden.png,}

