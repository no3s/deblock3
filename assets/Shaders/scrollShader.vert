uniform mat4 g_WorldViewProjectionMatrix;
attribute vec3 inPosition;
attribute vec2 inTexCoord;
varying vec2 texCoord;
varying float tunel;

uniform float g_Time;
uniform float m_scaleTexture;
uniform float m_scrollOffset;

void main(){
    gl_Position = g_WorldViewProjectionMatrix * vec4(inPosition, 1.0);
    tunel = inTexCoord.x;
    texCoord = vec2(inTexCoord.x, inTexCoord.y) * vec2(m_scaleTexture,m_scaleTexture);
    texCoord -= vec2(m_scrollOffset,0.0);
}