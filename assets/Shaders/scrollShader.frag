varying vec2 texCoord;
varying float tunel;
uniform sampler2D m_ColorMap;
uniform float m_distance;

void main(){    
    vec4 texColor = texture2D(m_ColorMap, texCoord);
    if(tunel < m_distance) texColor.rgba *= (tunel/m_distance);
    gl_FragColor = vec4(texColor.rgba);
}
