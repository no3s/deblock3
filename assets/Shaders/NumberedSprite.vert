uniform mat4 g_WorldViewProjectionMatrix;

attribute vec3 inPosition;
attribute vec2 inTexCoord;
varying vec2 texCoordAni;
varying vec2 texCoordAni2;
varying float tunel;

// if these are passed as ints, then it doesn't work for some reason
uniform int m_numTilesU;
uniform int m_numTilesV;
uniform int m_tile;

void main(){

gl_Position = g_WorldViewProjectionMatrix * vec4(inPosition, 1.0);
texCoordAni = inTexCoord;
texCoordAni2 = inTexCoord;
 tunel = inTexCoord.x;

int iNumTilesU = int(m_numTilesU);
int iNumTilesV = int(m_numTilesV);

int numTilesTotal = iNumTilesU * iNumTilesV;

int selectedTile = int(m_tile);

 // the “1 – ” bit is because otherwise it goes from right to left
texCoordAni.x = -(1.0 - float((texCoordAni.x + mod(float(selectedTile),  float(iNumTilesU))) / float(iNumTilesU))); ///selectedTile;
texCoordAni.y = (-texCoordAni.y - float(selectedTile / iNumTilesU)) / float(iNumTilesV); ///selectedTile;

}