varying vec2 texCoordAni;
uniform sampler2D m_AniTexMap;
uniform float m_distance;
varying float tunel;

void main(){

vec4 AniTex = texture2D(m_AniTexMap, vec2(texCoordAni));

if(m_distance < 1.0 && tunel < m_distance) AniTex.rgba *= (tunel/m_distance);

gl_FragColor.rgb = AniTex.rgb;

gl_FragColor.a = AniTex.a;

}