package deBlock3;

import ingamedebug.StageDebugGUI;
import deBlock3.Loaders.MidiAssetLoader;
import deBlock3.Loaders.LevelAssetLoader;
import deBlock3.Loaders.GameStageLoader;
import deBlock3.AppStates.GameState;
import deBlock3.AppStates.MenuState;
import com.jme3.app.Application;
import com.jme3.audio.AudioNode;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeSystem;
import com.jme3.system.Timer;
import deBlock3.AppStates.HighScoreState;
import deBlock3.AppStates.StartState;
import deBlock3.StageData.HighScore;
import java.io.InputStream;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import jme3tools.savegame.SaveGame;

public class Main extends Application {

    private static final Logger logger = Logger.getLogger(Main.class.getName());

    private GameState       gameState = null;
    private MenuState       menuState = null;
    private StartState      startState = null;
    private HighScoreState  highScoreState = null;    
    private GameStageSerializableData gameLevelData = null;   
    private Controller      ouyaController = null;
    final private String    userHome = System.getProperty("user.home");
    private StageData       mStageData = null;
    private int             iCurrentStage = 0;    
    private Boolean         bEnableDebug = false;
    public enum eGameState { START, MENU, GAME, HIGHSCORE };    
    public enum eTargetPlatform { DESKTOP, ANDROID, OUYA };
    private eGameState      gameStateEnum = eGameState.START;
    private eTargetPlatform gTargetPlatform = eTargetPlatform.DESKTOP;    
    public StageDebugGUI    stageDebugGUI = null;   
    public ScheduledThreadPoolExecutor executor = null;    
    
    protected Node          guiNode = new Node("Gui Node");
    private AudioNode       moveSound;
    private AudioNode       okSound;
    private AudioNode       errorSound;            
    
    public Boolean getDebugEnabled() { return bEnableDebug; }
    public eTargetPlatform getTargetPlatform() { return gTargetPlatform; }
    public eGameState getGameStateEnum() { return gameStateEnum; }
    public StageData getStageData() { return mStageData; }
    public ViewPort getGUIViewPort() { return guiViewPort; }   
    public Node getGuiNode() { return guiNode; }           
    public GameState getGameState() { return gameState; }
        
    @Override
    public ViewPort getViewPort() { return viewPort; }   
      
    @Override
    public Timer getTimer() { return timer; }
          
    public void setTargetPlatform(eTargetPlatform ePlatform) {        
         gTargetPlatform = ePlatform;                                                                 
         if(ePlatform != eTargetPlatform.DESKTOP) { bEnableDebug = false; }      
    }
       
    public boolean addController(Controller controller) {                     
        ouyaController = controller; ouyaController.setGame(this);
        return true;                      
     }
               
    public Main() {
        try { Class.forName("javax.sound.midi.MidiSystem"); } 
        catch (ClassNotFoundException e) { }                                    
        executor = new ScheduledThreadPoolExecutor(2);      
    }

    public void playOKSound() { 
     final Callable<Void> function = new Callable<Void>() {                            
            public Void call() throws Exception { okSound.setVolume(0.35f); okSound.play(); return null; } 
        };
        enqueue(function);
    }
    
    public void playMoveSound() { 
         final Callable<Void> function = new Callable<Void>() {                            
            public Void call() throws Exception { moveSound.setVolume(0.35f); moveSound.play(); return null; } 
        };
        enqueue(function);
    }
     
    public void playErrorSound() {
        final Callable<Void> function = new Callable<Void>() {                            
            public Void call() throws Exception { errorSound.setVolume(0.35f); errorSound.play(); return null; } 
        };
        enqueue(function);
    }
    
   
    @Override
    public void start(){
        AppSettings appSet = new AppSettings(true);
        appSet.setSettingsDialogImage("Images/title.png");
        appSet.setTitle("deBlock3");
        appSet.setVSync(true);
        if (settings == null) { setSettings(appSet); }
        
        this.setPauseOnLostFocus(false);        
        if (!JmeSystem.showSettingsDialog(settings, false)) { return; }
        super.start();
    }
   
    @Override
    public void initialize() {
        super.initialize();
        getAssetManager().registerLoader(MidiAssetLoader.class, "mid");
        getAssetManager().registerLoader(LevelAssetLoader.class, "txt");
        if(bEnableDebug) { LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME).setLevel(Level.ALL); }
        else { LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME).setLevel(Level.INFO); }        
        guiNode.setQueueBucket(Bucket.Gui);
        guiNode.setCullHint(CullHint.Never);
        guiViewPort.attachScene(guiNode);
        GameStageLoader.getInstance().LoadMainFile((InputStream)getAssetManager().loadAsset("Level/"+"main.txt"), this);    
        gameLevelData = new GameStageSerializableData();
        SaveGame.loadGame(userHome, "highScores.dat", getAssetManager());
        menuState = new MenuState(this);
        gameState = new GameState(this);            
        startState = new StartState(this);
        highScoreState = new HighScoreState(this);                
        moveSound = new AudioNode(getAssetManager(),"Sounds/GUI/move.ogg",false);                
        okSound = new AudioNode(getAssetManager(),"Sounds/GUI/pushok.ogg",false);
        errorSound = new AudioNode(getAssetManager(),"Sounds/GUI/error.ogg",false);        
        getStateManager().attach(startState);        
    }
   
    public void destroyGame()
    {
        getStateManager().detach(startState);
        getStateManager().detach(menuState);
        getStateManager().detach(gameState);
        getStateManager().detach(highScoreState);        
        try { stop(); } catch (Exception e) { }      
        executor.shutdown();      
        if(gTargetPlatform == eTargetPlatform.DESKTOP) { destroy(); }
    }

    @Override
    public void update() {
        super.update();
        float tpf = timer.getTimePerFrame() * speed;
        stateManager.update(tpf);
        guiNode.updateLogicalState(tpf);
        guiNode.updateGeometricState();
        stateManager.render(renderManager);
        renderManager.render(tpf, true);
    }

    public void loadHighScore() {   
      if(gTargetPlatform == eTargetPlatform.DESKTOP && stageDebugGUI != null) { stageDebugGUI.exitStage(); stageDebugGUI = null; }
      getStateManager().detach(gameState);
      getStateManager().detach(startState);
      getStateManager().detach(menuState);
      getStateManager().attach(highScoreState);
      gameStateEnum = eGameState.HIGHSCORE;
    }

    public void loadMenu() {  
      if(gTargetPlatform == eTargetPlatform.DESKTOP && stageDebugGUI != null) { stageDebugGUI.exitStage(); stageDebugGUI = null; }
      getStateManager().detach(highScoreState);
      getStateManager().detach(gameState);
      getStateManager().detach(startState);
      getStateManager().attach(menuState);
      gameStateEnum = eGameState.MENU;
    }

    public void loadStart() {                 
      if(gTargetPlatform == eTargetPlatform.DESKTOP && stageDebugGUI != null) {  stageDebugGUI.exitStage(); stageDebugGUI = null; }
      getStateManager().detach(highScoreState);
      getStateManager().detach(gameState);
      getStateManager().detach(menuState);
      getStateManager().attach(startState);
      gameStateEnum = eGameState.START;
    }

    public void loadGame() {      
      setStageData(getCurrentStage());
      getStateManager().detach(menuState);
      getStateManager().detach(startState);
      getStateManager().attach(gameState);
      getStateManager().detach(highScoreState);
      gameStateEnum = eGameState.GAME;      
      if(gTargetPlatform == eTargetPlatform.DESKTOP && bEnableDebug) {   stageDebugGUI = new StageDebugGUI(this); stageDebugGUI.setGUIStage(mStageData); }
    }

    public void pauseGame(boolean paused) {
        menuState.setEnabled(paused);
        startState.setEnabled(paused);
        gameState.setEnabled(paused);
    }

    public void setStageData(int iStageNumber) {        
        if(iStageNumber < 0) { iStageNumber = 0; }
        if(iStageNumber > GameStageLoader.getInstance().numStagesLoaded()-1) { iStageNumber = GameStageLoader.getInstance().numStagesLoaded()-1; }
        iCurrentStage = iStageNumber;
        mStageData = GameStageLoader.getInstance().getStageAt(iCurrentStage);   
    }
   
    public int getCurrentStage() { return iCurrentStage; }

    public String setCurrentStage() { return GameStageLoader.getInstance().getStageAt(iCurrentStage).getLabel(); }

    public Boolean getIsCurrentLevelCleared() { return  GameStageLoader.getInstance().getStageAt(iCurrentStage).getStageCleared(); }

    public Boolean getIsPrevStageCleared() { return iCurrentStage == 0 ? true : GameStageLoader.getInstance().getStageAt(iCurrentStage-1).getStageCleared(); }               

    public void setHighScore(int score){highScoreState.setHighScore(score);}   

    public void saveHighScore(){SaveGame.saveGame(userHome, "highScores.dat", gameLevelData);}

    public String getStageHighScore() {
        String temp = "";
        for(int i=0; i < StageData.MAX_HIGHSCORES; i++)
        {
            HighScore highScore = GameStageLoader.getInstance().getStageAt(iCurrentStage).getHighScore(i);
            if(highScore.score > 0) {
             temp = temp + highScore.date + "              " + highScore.playerName + "              " + highScore.score + "\n";
            }
        }
        return temp;
    }
         
    private String RandomTexturesArray[] = 
    {
        "Scroll(grid2.png;96;0.25;0.87)"
        ,"Scroll(floor1.jpg;512;0.25;0.87)"
        ,"Scroll(funkgas/tarmac.png;256;0.825;0.6)"
        ,"Scroll(grass.jpg;512;0.25;0.87)"
        ,"Scroll(concrete.png;512;0.25;1.0)"
    };

    public String getRandomTexture() { return (RandomTexturesArray[new Random().nextInt(RandomTexturesArray.length)]); }

    public static void main(String... args) {
       new Main().start();
    }         
}