/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3;

import com.jme3.asset.TextureKey;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author no3z
 */
public class TestScene extends SceneAbstract {

    private static final Logger logger = Logger.getLogger(SceneAbstract.class.getName());
    
    private RigidBodyControl    floor_phy;

    private Material floor_mat = null;

//    private MotionPath tPathCallback = null;
//    private MotionEvent tMotionControlCallback = null;    
//    
    private float scroll = 0.0f;
    private  Geometry floor_geo;
    private float scrollVelocity = 0.10f;
    
    public TestScene(Main game, String filename) {
        super(game, filename);        
    }            
        
    @Override
    public void initialize(Player player)
    {
        super.initialize(player);                         
        initFloor();    
                             
        offsetArray = (float)((new Random().nextInt(game.getStageData().getAvailableTracksAtMeasure(0))) * (float)game.getStageData().getStageGapBetweenTracksSize());        
        
        scroll = 0.0f;
        
        if(floor_mat.getParam("scrollOffset") != null)
        {
            scrollVelocity = (Float)floor_mat.getParam("scrollOffset").getValue();
        }
        
//        tPathCallback = new MotionPath();        
//        tMotionControlCallback = new MotionEvent(floor_geo, tPathCallback);        
//        tMotionControlCallback.setInitialDuration(2.0f);
//        tMotionControlCallback.setSpeed(0.50f);          
//        tPathCallback.addListener(new MotionPathListener() {
//        public void onWayPointReach(MotionEvent control, int wayPointIndex) {
//                if (tPathCallback.getNbWayPoints() == wayPointIndex + 1) {                    
//                    tMotionControlCallback.stop();                    
//                }
//            }
//         });
           
        logger.log(Level.FINE," Random: {0} Scroll {1} ScrollVelocity {2}", new Object[]{offsetArray,scroll,scrollVelocity});
    }

    public void incSceneOffsetTrack()
    {
        offsetArray += game.getStageData().getStageGapBetweenTracksSize();
        
        if(offsetArray > getPositionMaxX())
        {
            offsetArray = 0.0f;
        }        
    }
  
  public void initFloor() {      
   
    Box floor = new Box(Vector3f.ZERO, 1000f, 0.25f, 1000f);         
    Geometry floor_geo2 = new Geometry("Floor", floor);   
      
    floor_geo = StageData.createModelsGeometries(game.getAssetManager(),game.getStageData().getStageModel());
             
    floor_geo.setLocalTranslation(game.getStageData().getStageModelTranslation());
                       
    floor_geo.scale(
            game.getStageData().getStageModelScale().x,
            game.getStageData().getStageModelScale().y,
            game.getStageData().getStageModelScale().z);
        
       
    floor_mat = StageData.createModelsMaterials(getGame().getAssetManager(),mStageData.getStageTexture());  
    
    floor_geo.setMaterial(floor_mat);     
    
    
    if(!game.getStageData().getStageEnvSphereTexture().contains("NONE"))
    {
        TextureKey key = new TextureKey("Textures/" + mStageData.getStageEnvSphereTexture(), false);
        key.setGenerateMips(true);
        Texture tex = getGame().getAssetManager().loadTexture(key);
            
        this.rootNode.attachChild(SkyFactory.createSky(getGame().getAssetManager(), tex, new Vector3f(100.5f,100.5f,-1), true));
    }
    
    
    this.rootNode.attachChild(floor_geo);
            
    /* Make the floor physical with mass 0.0f! */
    floor_phy = new RigidBodyControl(0.0f);
    floor_geo2.addControl(floor_phy);    
    floor_phy.setPhysicsLocation(new Vector3f(0.0f, -0.1f, 0.0f));
    floor_phy.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_02);
    floor_phy.setCollideWithGroups(PhysicsCollisionObject.COLLISION_GROUP_01);
    floor_phy.addCollideWithGroup(PhysicsCollisionObject.COLLISION_GROUP_03);
    bulletAppState.getPhysicsSpace().add(floor_phy);    
  }
  
  void sceneTracksChanged(int numTracks)
  {      
//        final Integer t = numTracks;      
//        tPathCallback.clearWayPoints();                   
//        Vector3f t1 = new Vector3f(floor_geo.getLocalTranslation());
//        tPathCallback.addWayPoint(t1);                      
//        t1.setX((t-1)*2.5f);
//        tPathCallback.addWayPoint(t1);       
//        tMotionControlCallback.play();    
  }
  
  void finalizeScene()
  {
      rootNode.detachChild(floor_geo);
      if(floor_phy != null) {
        bulletAppState.getPhysicsSpace().remove(floor_phy);
      }
      floor_mat = null;
      floor_phy = null;
  }
  
  void updateScene(float tpf) { 
      if(floor_mat.getParam("scrollOffset") != null)
      {          
          scroll += tpf * scrollVelocity;
          if(scroll > 1.0f) { scroll = scroll - 1.0f; }          
          floor_mat.setFloat("scrollOffset", scroll);
      }
  }

  
  
}

