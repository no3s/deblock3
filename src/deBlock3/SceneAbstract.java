/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3;

import ingamedebug.SceneDebugInputMapper;
import deBlock3.Midi.MidiPlayerHandler;
import deBlock3.Midi.MidiInputHandler;
import deBlock3.AppStates.GameState;
import deBlock3.Controls.PhysicsCollisionControl;
import com.jme3.bullet.BulletAppState;
import com.jme3.cinematic.MotionPath;
import com.jme3.cinematic.MotionPathListener;
import com.jme3.cinematic.PlayState;
import com.jme3.cinematic.events.MotionEvent;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial.CullHint;
import com.leff.midi.util.MetronomeTick;
import java.util.concurrent.Callable;


/**
 *
 * @author no3z
 */
public abstract class SceneAbstract implements Callable {        
    
    protected Main game = null;
    public Player player = null;
    protected BulletAppState bulletAppState = new BulletAppState();
    protected MidiPlayerHandler midiPlayerEventDispatch = null;
    protected MidiInputHandler midiInputEventDispatch = null;
    protected PhysicsCollisionControl physControl;
    protected Node rootNode = new Node("Root Node");
    protected CameraNode cameraNode;    
    protected StageData mStageData = null;
    private Boolean bInitialized = false;    
    
    protected int numTracks;
    
    protected float offsetArray = -5.0f;
    protected float fPositionXMax = 0.0f;
    
    protected int num_tracks_in_file = 0;  
    protected Quaternion actualCameraRotation;    
    private int current_camera_pos = 0;
    private MotionPath tPath = null;
    private MotionEvent tMotionEvent = null;
    static private StageData.StageCameraPosition currentLevelCam;
        
    private SceneDebugInputMapper sdim;
    public float debugDelay = 0.0f;
    
    SceneAbstract(Main game, String levelTxtFile)
    {
        this.game = game;  
        game.getStateManager().attach(bulletAppState);
        rootNode.setCullHint(CullHint.Dynamic);
        
        // bDriverAvailable = false; 
        
        if(game.getTargetPlatform() == Main.eTargetPlatform.DESKTOP) {
           // bulletAppState.getPhysicsSpace().enableDebug(game.getAssetManager());       
        }   
    }
    
    public void initialize(Player player)
    {
        mStageData = game.getStageData();

        physControl = new PhysicsCollisionControl(bulletAppState, this);
        
        cameraNode = new CameraNode("camera", getGame().getCamera());               
        
        this.player = player;        
        
        MidiMessageProcessorSingleton.getInstance().setUpScene(this, mStageData);

        
        if(mStageData.hasMidiFile()) {
            midiPlayerEventDispatch = new MidiPlayerHandler();
            midiPlayerEventDispatch.initialize();
            mStageData.initialize(rootNode, this);  
            midiPlayerEventDispatch.initializeMinMaxNotes();
        }                      
        else {
            mStageData.initialize(rootNode, this);  
        }
                                
        currentLevelCam = game.getStageData().getCameraPosition(0);               
        cameraNode.setLocalTranslation(currentLevelCam.cameraPosition);     
        actualCameraRotation = new Quaternion().fromAngles(currentLevelCam.cameraRotation.x * FastMath.DEG_TO_RAD,currentLevelCam.cameraRotation.y* FastMath.DEG_TO_RAD,currentLevelCam.cameraRotation.z* FastMath.DEG_TO_RAD);
        cameraNode.setLocalRotation(actualCameraRotation);  
        cameraNode.getCamera().setFrustumPerspective(currentLevelCam.cameraFrustrum.x,currentLevelCam.cameraFrustrum.y,0.01f,1000.0f);
                   
        if(game.getTargetPlatform() == Main.eTargetPlatform.DESKTOP ) {
            if( !getStageData().hasMidiFile() )
              {
                        midiInputEventDispatch = MidiInputHandler.getInstance();
                        midiInputEventDispatch.start();
              }
            
            sdim = new SceneDebugInputMapper(this,currentLevelCam.cameraRotation, currentLevelCam.cameraFrustrum.x,currentLevelCam.cameraFrustrum.y);
        }
                        
        tPath = new MotionPath();        
        tMotionEvent = new MotionEvent(cameraNode, tPath);        
        tMotionEvent.setInitialDuration(1.5f);
        tMotionEvent.setSpeed(1.0f);  
        tPath.addListener(new MotionPathListener() {

         public void onWayPointReach(MotionEvent control, int wayPointIndex) {
             if (tPath.getNbWayPoints() == wayPointIndex + 1) {                                                                      
                   StageData.StageCameraPosition cam = game.getStageData().getCameraPosition(game.getStageData().getCurrentCameraPositionPoints(getMeasure())); 
                 Quaternion q = new Quaternion().fromAngles(cam.cameraRotation.x * FastMath.DEG_TO_RAD,cam.cameraRotation.y* FastMath.DEG_TO_RAD,cam.cameraRotation.z* FastMath.DEG_TO_RAD);
                 cameraNode.setLocalRotation(q);
                 tMotionEvent.stop();                    
                 buff = false;
                 tPath.clearWayPoints();
             }
         }
        });
        
        numTracks = game.getStageData().getNumTracks();
        num_tracks_in_file = game.getStageData().getAvailableTracksAtMeasure(0);
        fPositionXMax = (num_tracks_in_file-1) * game.getStageData().getStageGapBetweenTracksSize();
    }
        
    abstract void updateScene(float tpf);
    
    Boolean buff = false;
    public void update(float tpf) {
        if(!bInitialized){                                                
            if(midiPlayerEventDispatch != null) {
                midiPlayerEventDispatch.play();
                getGame().getStateManager().getState(GameState.class).setGameText("Start", 3.0f, false);                                         
            }            
            else if(game.getTargetPlatform() == Main.eTargetPlatform.DESKTOP)
            {
                getGame().getStateManager().getState(GameState.class).setGameText("Wait MIDI input", 3.0f, false);                                         
            }
            bInitialized = true;            
        }

        StageData.Texts t = game.getStageData().getTextForMeasure(this.getMeasure());
        if(t != null)
        {
            getGame().getStateManager().getState(GameState.class).setGameText(t.text,t.time,t.size);
        }
        
        MidiMessageProcessorSingleton.getInstance().update(tpf);
                            
        updateScene(tpf);        
        player.update(tpf);
                
        int camPointAtMeasure = game.getStageData().getCurrentCameraPositionPoints(getMeasure());
          
        if(buff)
        {           
            float p = (tMotionEvent.getTime() / tMotionEvent.getDuration()*5.0f)*tpf;  
            if(p  > 0.97f) p = 1.0f;
            cameraNode.getLocalRotation().nlerp(actualCameraRotation, p);            
        }
        
        if(game.getDebugEnabled() && !mStageData.hasMidiFile()) {
              if(current_camera_pos != camPointAtMeasure) {
                StageData.StageCameraPosition cam = game.getStageData().getCameraPosition(camPointAtMeasure); 
                cameraNode.getCamera().setFrustumPerspective(cam.cameraFrustrum.x,cam.cameraFrustrum.y,0.01f,1000.0f);   
                cameraNode.setLocalTranslation(new Vector3f(cam.cameraPosition));
                Quaternion q = new Quaternion().fromAngles(cam.cameraRotation.x * FastMath.DEG_TO_RAD,cam.cameraRotation.y* FastMath.DEG_TO_RAD,cam.cameraRotation.z* FastMath.DEG_TO_RAD);
                cameraNode.setLocalRotation(q);
                current_camera_pos = camPointAtMeasure;
              }
            
        }
        else 
        {
            if(current_camera_pos != camPointAtMeasure) {
                current_camera_pos = camPointAtMeasure;

                StageData.StageCameraPosition cam = game.getStageData().getCameraPosition(current_camera_pos); 

                if(!currentLevelCam.cameraFrustrum.equals(cam.cameraFrustrum)) {
                     cameraNode.getCamera().setFrustumPerspective(cam.cameraFrustrum.x,cam.cameraFrustrum.y,0.01f,1000.0f);        
                }

                currentLevelCam = cam;

                tPath.addWayPoint(cameraNode.getLocalTranslation());
                tPath.addWayPoint(currentLevelCam.cameraPosition);    
                actualCameraRotation= new Quaternion().fromAngles(currentLevelCam.cameraRotation.x * FastMath.DEG_TO_RAD,currentLevelCam.cameraRotation.y* FastMath.DEG_TO_RAD,currentLevelCam.cameraRotation.z* FastMath.DEG_TO_RAD);                                
                tMotionEvent.play(); 
                
                buff = true;
            }
        }
        int tTracks = game.getStageData().getAvailableTracksAtMeasure(this.getMeasure());
        if(num_tracks_in_file != tTracks)
        {
            
            num_tracks_in_file = tTracks;
            fPositionXMax = (num_tracks_in_file-1) * game.getStageData().getStageGapBetweenTracksSize();

            if(player.getPosX() >= num_tracks_in_file)
            {
                player.movePlayerToPosX(fPositionXMax);
            }
            sceneTracksChanged(num_tracks_in_file);
        }
        
    }    
    
    abstract void sceneTracksChanged(int numTracks);
    
    public void finished()
    {               
        getGame().enqueue(this);
    }
    
    abstract void finalizeScene();
    
    public void stop()
    {
        finalizeScene();
        bulletAppState.getPhysicsSpace().removeAll(rootNode);
        
        for(int i = 0; i > rootNode.getQuantity(); ++i)
        {
            rootNode.detachChildAt(i);
        }
        rootNode.detachAllChildren();
        
        player.finalizePlayer();        
        bulletAppState = null;
        physControl = null;
        rootNode = null;
        cameraNode = null;
        bInitialized = false;
        
        if(mStageData.hasMidiFile()) {
            midiPlayerEventDispatch.stop();
            midiPlayerEventDispatch = null;
        }
        
        mStageData = null;     
        
        if(game.getTargetPlatform() == Main.eTargetPlatform.DESKTOP) {
            if(midiInputEventDispatch != null)
            {
                midiInputEventDispatch.pause();
                midiInputEventDispatch = null;
            }
            
        }
    }
    
    public Node getRootNode() {return this.rootNode;}
    public BulletAppState getBulletAppState() { return this.bulletAppState; }
    public Main getGame() { return this.game; }
    public Player getPlayer() { return this.player; }
    public CameraNode getCameraNode() { return this.cameraNode; }
    public StageData getStageData() { return mStageData; }

    
    public MetronomeTick getMetronome()
    {
        if(midiPlayerEventDispatch != null) {
            return midiPlayerEventDispatch.getMetronome();
        }
        return null;
    }
    
    public String getMetronomeString()
    {
        if(midiPlayerEventDispatch != null) {
            return midiPlayerEventDispatch.getMetronome().toString();
         }else if (midiInputEventDispatch != null) {
             
            return "Beat: " + midiInputEventDispatch.getMeasure() + " Time: " + midiInputEventDispatch.mtcTime + " lastnote: " + midiInputEventDispatch.lastnote;
        }
        else {
            return "CP: " + sdim.cam_pos + " CD: " + sdim.cam_dir + " FR: " + sdim.frustrum + " BM: " + getMeasure();
        }                    
    }
    
     public int getNumMidiTracks()
    {
        if(midiPlayerEventDispatch != null) {
            return midiPlayerEventDispatch.getNumTracks() - 1;
        }
        return mStageData.getNumTracks();
    }
    
    public int getMeasure()
    {
        if(midiPlayerEventDispatch != null) {
            return midiPlayerEventDispatch.getMetronome().getMeasure();
        }else if (midiInputEventDispatch != null) {
            return midiInputEventDispatch.getMeasure();
        }
        return 1;
    }

    public int getFrequency()
    {
        if(midiPlayerEventDispatch != null) {
            return midiPlayerEventDispatch.getMetronome().getFrequency();
        }
        return 96;
    }
    
    public long getFrequencyDurationInMs() {
        if(midiPlayerEventDispatch != null) {
            return midiPlayerEventDispatch.getFrequencyDurationInMs();
        }
        return 500;
    }    
    
    public float getBPM()
    {
        if(midiPlayerEventDispatch != null) {
            return midiPlayerEventDispatch.getBPM();
        } else {
            return mStageData.bpm;
        }        
    }
    
    public void pauseAudio(boolean pause)
    {
        if(midiPlayerEventDispatch != null) {
            midiPlayerEventDispatch.pause(pause);
        }
    }
    
    public int getNumTracks() { return numTracks; }
    
    public int getNumTracksInFile() { return num_tracks_in_file; }
        
    public float getPositionMaxX() { return fPositionXMax; }
    
    public float getOffsetArray() { return offsetArray; }
        
    public Vector2f getMinMaxNotesForTrack(int track) {
        return this.mStageData.getMinMaxNotesForTrack(track);
    }
    
    public void setAudioVolume(float volume)
    {
        if(midiPlayerEventDispatch != null) {
            midiPlayerEventDispatch.setAudioVolume(volume);
        }
    }
    
    abstract public void incSceneOffsetTrack();

    
    static public float getCameraPosZ()
    {
        return currentLevelCam.cameraPosition.z;
    }
        
    public Object call() throws Exception {
        finalizeScene();
        if(mStageData.checkHighScore(player.getPoints()))
        {
            getGame().setHighScore(player.getPoints());
            getGame().loadHighScore();
        }
        else
        {        
            getGame().loadMenu();
        }
        return null;
    }
}
