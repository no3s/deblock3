package deBlock3.Loaders;

import deBlock3.StageData;
import deBlock3.Main;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;



public class GameStageLoader 
{        
    private ArrayList<StageData> levels = new ArrayList<StageData>();
 
    protected GameStageLoader() {
      // Exists only to defeat instantiation.
    }
      
    public static GameStageLoader getInstance() {
      if(instance == null) {
         instance = new GameStageLoader();
      }
      return instance;
   }
    
    public void LoadMainFile(InputStream inputStream, Main game) {  
        int index = 0;
        Scanner s = new Scanner(inputStream);
        levels.clear();
        while(s.hasNextLine())
        {          
            String st = s.nextLine();
            if(!st.isEmpty() && !st.contains("#")) {
            StageData temp = new StageData((InputStream)game.getAssetManager().loadAsset("Level/"+st),"Level/"+st,game);
            levels.add(index,temp);
            index++;
            }
        }
    }
            
    private static GameStageLoader instance = null;
       
    public int numStagesLoaded()
    {
        return levels.size();
    }
    
    public StageData getStageAt(int index)
    {
       return levels.get(index);
    }         

}

