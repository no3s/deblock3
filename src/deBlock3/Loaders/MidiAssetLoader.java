/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.Loaders;

/**
 *
 * @author no3z
 */
import com.jme3.asset.AssetInfo;
import com.jme3.asset.AssetLoader;
import java.io.IOException;
import java.io.InputStream;

public class MidiAssetLoader implements AssetLoader{
    
    public Object load(AssetInfo assetInfo) throws IOException {

    InputStream in = assetInfo.openStream();
    if (in == null) return 0;
    
    return in;    
}
    
}

