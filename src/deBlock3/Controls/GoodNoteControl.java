/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.Controls;

import com.leff.midi.event.NoteOn;
import deBlock3.StageData;
import deBlock3.Player;

/**
 *
 * @author no3z
 */
public class GoodNoteControl extends AbstractNoteControl{
    
                
    public GoodNoteControl(String name, NoteOn note, StageData.MutableProperties mProp)
    {
        super(name, note, mProp);          
    }     
    
    public void performCollision(Player player)
    {        
        if(mProperties.options.isEmpty() || mProperties.options.equals("NONE") || !mProperties.options.contains("POINTS:")) { player.addPoints(1); return; }
        
        String temp = this.mProperties.options.substring(this.mProperties.options.lastIndexOf("POINTS:")+7);
        if(!temp.isEmpty() && isNumeric(temp))
        {
            Integer t = Integer.parseInt(temp);
            player.addPoints(t);
        }else {
            player.addPoints(1);
        }
    }
}
