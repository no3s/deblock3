/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.Controls;

import com.jme3.audio.AudioSource;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.system.NanoTimer;
import com.jme3.system.Timer;
import com.leff.midi.event.NoteOn;
import deBlock3.StageData.MutableProperties;
import deBlock3.Player;
import deBlock3.SceneAbstract;
import java.util.concurrent.Callable;

/**
 *
 * @author no3z
 */
public abstract class AbstractNoteControl extends RigidBodyControl{
    
    protected Geometry geometry;
    protected String name;
    protected NoteOn note = null;
    protected float duration = 1.0f;
    protected static float fInitialPosition = 30.0f;
    public boolean bProcessedByHit = false;
    public boolean bDeleted = false;    
    protected static CollisionShape noteCollisionShape;
    protected Vector3f direction = new Vector3f(1.0f,1.0f,-1.0f);
    protected Vector3f position = new Vector3f();    
    protected MutableProperties mProperties = null;
    protected String control;
    private SceneAbstract scene = null;
    private  Timer timer;
    double tStart = 0.0;
    
    AbstractNoteControl(String name, NoteOn note, MutableProperties mProp)
    {
        this.name = name;
        this.note = note;
        this.duration = (float)note.getDuration() / 100.0f;   
        this.mProperties = mProp;
        timer = new NanoTimer();
        tStart = timer.getTimeInSeconds();
        
    }
    
        
    public String getName() {return name;}
    public NoteOn getNote() {return note;}
    public int getChannel() { return note.getChannel(); }
    public Geometry getGeometry() {return geometry;}    
    public abstract void performCollision(Player player);

    
    public void initialize(SceneAbstract scene)
    {       
        this.scene = scene;
        geometry = new Geometry(name,mProperties.objectGeometry.getMesh());
        geometry.setLocalRotation(mProperties.objectGeometry.getLocalRotation());
        geometry.setLocalTranslation(mProperties.objectGeometry.getLocalTranslation());                
        geometry.setLocalScale(mProperties.objectGeometry.getLocalScale());
        geometry.setCullHint(mProperties.objectGeometry.getCullHint());
        geometry.setQueueBucket(mProperties.objectGeometry.getQueueBucket());
                

        {
            geometry.scale(mProperties.scale.x,mProperties.scale.y,mProperties.scale.z);
            noteCollisionShape = new BoxCollisionShape(mProperties.scale);
        }
        
        if(mProperties.material == null)
        {
            mProperties.material = new Material(scene.getGame().getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md"); 
            mProperties.material.setColor("Color", ColorRGBA.randomColor());
        }
                
        if(mProperties.texture.equals("NONE") && mProperties.options.contains("RGB:") )
        {            
            String temp = this.mProperties.options.substring(this.mProperties.options.lastIndexOf("RGB:")+4);
            if(!temp.isEmpty())
            {
                String delim = "[,]";
                String[] tokens = temp.split(delim);        
                for (int i = 0; i < tokens.length; i++)
                {           
                    //Avoid brackets            
                    if(tokens.length != 3) {   break; }                                                  
                    mProperties.material.setColor("Color", new ColorRGBA(Float.parseFloat(tokens[0].trim()),Float.parseFloat(tokens[1].trim()),Float.parseFloat(tokens[2].trim()),1.0f));
                }
            }
        }
        
        if(mProperties.texture.equals("NONE") && mProperties.options.contains("RANDOMRGB") )
        {
            Material mat = new Material(scene.getGame().getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md"); 
            mat.setColor("Color", ColorRGBA.randomColor());
            geometry.setMaterial(mat);
        } else {
            geometry.setMaterial(mProperties.material);  
        }
                                      
        scene.getRootNode().attachChild(geometry);             
        this.position.set(initializeObjectPosition());                        
        this.direction.set(getDirectionOfNote()); 
        
        geometry.setLocalTranslation(position);
        setCollisionShape(noteCollisionShape);      
                
        setPhysicsLocation(position);
        setMass(mProperties.mass);                          
                        
        setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_03);
        setCollideWithGroups(PhysicsCollisionObject.COLLISION_GROUP_01 );
        addCollideWithGroup(PhysicsCollisionObject.COLLISION_GROUP_02 );  
        addCollideWithGroup(PhysicsCollisionObject.COLLISION_GROUP_03 );  
        setLinearVelocity(LinearVelocityForNote());
    }
    

    
    protected Vector3f initializeObjectPosition()
    {
        Vector3f temp = new Vector3f(scene.getStageData().getMutableProperties(note.getChannel(),note.getNoteValue(),scene.getMeasure()).position);
        Vector3f scale = new Vector3f(scene.getStageData().getMutableProperties(note.getChannel(),note.getNoteValue(),scene.getMeasure()).scale); 
        
        String x_control_type = mProperties.getControlTypes().xtype.toLowerCase();
        if(x_control_type.equals("sceneinc")) { temp.x = PosSceneInc(); }
        else if(x_control_type.equals("trackinc")) { temp.x = PosTrackInc(); }
        else if(x_control_type.equals("trackpp")) { temp.x = PosTrackPingPong(); }
        else if(x_control_type.equals("player")) { temp.x = PosPlayer(); }
        else if(x_control_type.equals("sin")) { temp.x = PosSin(temp.x); }
        else if(x_control_type.equals("range")) { temp.x = PosXRange() - temp.x; }
        
        String y_control_type = mProperties.getControlTypes().ytype.toLowerCase();
        if(y_control_type.equals("sceneinc")) { temp.y = (PosSceneInc()*temp.y) + scale.y; }
        else if(y_control_type.equals("trackinc")) { temp.y = (PosTrackInc()*temp.y) + scale.y; }
        else if(y_control_type.equals("trackpp")) { temp.y = (PosTrackPingPong()*temp.y) + scale.y; }
        else if(y_control_type.equals("player")) { temp.y = PosPlayer(); }
        else if(y_control_type.equals("sin")) { temp.y = PosSin(temp.y) + scale.y; }
        else if(y_control_type.equals("range")) { temp.y = PosYRange() - temp.y; }
        
        
        String z_control_type = mProperties.getControlTypes().ztype.toLowerCase();
        if(z_control_type.equals("sceneinc")) { temp.z = PosSceneInc()+temp.z; }
        else if(z_control_type.equals("trackinc")) { temp.z = PosTrackInc()+temp.z; }
        else if(z_control_type.equals("trackpp")) { temp.z = PosTrackPingPong()+temp.z; }
        else if(z_control_type.equals("player")) { temp.z = PosPlayer(); }
        else if(z_control_type.equals("sin")) { temp.z = PosSin(temp.z); }
        else if(z_control_type.equals("range")) { temp.z = PosXRange() - temp.z; }
        
        control = mProperties.control.toLowerCase();
        return temp;
        
    }    
    
    protected Vector3f getDirectionOfNote()
    {      
        return scene.getStageData().getMutableProperties(note.getChannel(),note.getNoteValue(),scene.getMeasure()).direction;         
    }        
     
    protected Vector3f LinearVelocityForNote()
    {                                     
        if(control.equals("seek")) 
        {                                                    
            Vector3f heading = new Vector3f(scene.getPlayer().getPlayerGeometry().getLocalTranslation().subtract(position));      
            Vector3f temp = scene.getStageData().getMutableProperties(note.getChannel(),note.getNoteValue(),scene.getMeasure()).velocity;                       
            return direction.mult(new Vector3f( (heading.x/temp.x),(heading.y/temp.y), (heading.z/temp.z) ) );                         
        } 
        else 
        {                    
             Vector3f tV = new Vector3f(scene.getStageData().getMutableProperties(note.getChannel(),note.getNoteValue(),scene.getMeasure()).velocity);
             return direction.mult(tV);                          
        }
    }
    
    //--------------------------------------POSITION AUXILIARY FUNCTIONS
    
    protected float PosSceneInc()
    {  
        scene.incSceneOffsetTrack();             
        return scene.getOffsetArray();
                  
    }
    
    protected float PosTrackInc()
    {  
        scene.getStageData().incTrackOffsetPosition(getChannel());            
        return scene.getStageData().getTrackOffsetPosition(getChannel());                   
    }
          
    protected float PosTrackPingPong()
    {  
        scene.getStageData().pingpongTrackOffsetPosition(getChannel());           
        return scene.getStageData().getTrackOffsetPosition(getChannel());                 
    }
        
    protected float PosPlayer()
    {          
        return scene.getPlayer().getPosX()*scene.getStageData().getStageGapBetweenTracksSize();  
    }
           
    
    protected float PosSin(float temp)
    {                 
        double timeSin = Math.sin(scene.getGame().getTimer().getTimeInSeconds());        
        float centerPos = (temp / 2) ;        
        timeSin =  (centerPos + 1) + (timeSin * centerPos);        
        return (float)timeSin;  
    }
        
    protected float PosYRange()
    {
        Vector2f ranges = scene.getMinMaxNotesForTrack(getChannel());       
        int diff = (int)ranges.y - (int)ranges.x;        
        if(diff == 0) { diff = 1; }        
        int scaledNote = note.getNoteValue() - (int)ranges.x;        
        if(scaledNote == 0) { scaledNote = 1; }        
        float scaler = 12.0f/diff;        
        float noteResult = scaledNote*scaler;                  
        if(noteResult < 1.0f) {noteResult = 1.0f;}
        return noteResult;
    }
          
    protected float PosXRange()
    {       
        Vector2f ranges = scene.getMinMaxNotesForTrack(getChannel());        
        int diff = (int)ranges.y - (int)ranges.x;                
        if(diff == 0) { diff = 2; }        
        int scaledNote = note.getNoteValue() - (int)ranges.x;                                     
        if(scaledNote == 0) { scaledNote = 1; }        
        float scaler =  ((float)(scene.getNumTracksInFile()-1.0f)) / diff;        
        float noteResult = scaledNote*scaler;                        
        return (float)((int)noteResult) * scene.getStageData().getStageGapBetweenTracksSize();         
    }
    
    
    //--------------------------------------SOUND STUFF
    //Put the wait to sync sound in a thread, then enque the sound 
    //to be played in the render thread.
    
    public boolean isSoundPlaying()
    {
        if(mProperties.sound == null) return false;
        if(mProperties.sound.getStatus() == AudioSource.Status.Playing) return true;
        return false;
    }
        
        
    public final Callable<Void> playSoundThread = new Callable<Void>() {
           @Override
            public Void call() throws Exception {                            
                    if(mProperties.sound != null) 
                    { // && mp.sound.getStatus() != AudioSource.Status.Playing){                            

                        float time = timer.getTimeInSeconds();
                        float freq = scene.getFrequencyDurationInMs()*0.0005f;
                        while(time > 0.0f) { time -=  freq; }
                        time = -time;
                        Timer t = new NanoTimer();
                        while( t.getTimeInSeconds() < time) {}
                      scene.getGame().enqueue(playSoundEnque); 
                  }
                  return null;
            }
    };
        
     public final Callable<Void> stopSoundTimer = new Callable<Void>() {
           @Override
            public Void call() throws Exception {   
                             
               mProperties.sound.stop();
                 
               return null;
            }
    };
     
    public final Callable<Void> playSoundEnque = new Callable<Void>() {
           @Override
            public Void call() throws Exception {   
               if(mProperties.sound != null) {
                
                mProperties.sound.setVolume(mProperties.soundVolume);                                      
                mProperties.sound.setTimeOffset(mProperties.soundStartTime);                   
                mProperties.sound.playInstance();                
                
               }
            return null;
            }
    };   
    
    protected static boolean isNumeric(String str)
{
    for (char c : str.toCharArray())
    {
        if (!Character.isDigit(c)) return false;
    }
    return true;
}
}
