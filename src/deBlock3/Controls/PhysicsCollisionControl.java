
package deBlock3.Controls;

import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionGroupListener;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.scene.Spatial;
import deBlock3.Main;
import deBlock3.Player;
import deBlock3.SceneAbstract;


public class PhysicsCollisionControl extends RigidBodyControl implements PhysicsCollisionListener, PhysicsCollisionGroupListener
{      
    private SceneAbstract mScene;
    
    public PhysicsCollisionControl(BulletAppState bulletAppState, SceneAbstract scene) {
        super();
        this.mScene = scene;
        bulletAppState.getPhysicsSpace().addCollisionListener(this);        
    }
    
    public void collision(PhysicsCollisionEvent event) 
    {               
         if( event.getNodeA().getName().equals("Floor") ){
             return;
         }
         Spatial player = null;
         Spatial object = null;
         
          if( event.getNodeA().getName().equals("player") )
            {
                player = event.getNodeA();
                object = event.getNodeB();
            }
          else if( event.getNodeB().getName().equals("player") )
            {
                player = event.getNodeB();
                object = event.getNodeA();
            }
                    
          if(player != null && object != null)
          {                
                AbstractNoteControl noteControl = object.getControl(AbstractNoteControl.class);
         
                if(noteControl == null) return;
                
                if(noteControl.bProcessedByHit == false)
                {   
//                    if(mScene.getGame().getTargetPlatform() == Main.eTargetPlatform.ANDROID)
//                    {
//                        mScene.getGame().enqueue(noteControl.playSoundEnque);                         
//                    }
//                    else {
                        mScene.getGame().executor.submit(noteControl.playSoundThread); 
                    //}
                    
                    Player thePlayer = mScene.getPlayer();   
                    
                    if(thePlayer.getStatus().equals("PLAY")) 
                    {                                              
                        noteControl.performCollision(thePlayer);                                        
                    }
                    
                    //On simpleNotes do not delete the object
                    if(noteControl instanceof SimpleNoteControl)
                    {                                       
                        noteControl.bDeleted = false;      
                    }
                    else
                    {
                        noteControl.bDeleted = true;                        
                    }                    
                    
                    noteControl.bProcessedByHit = true;
                }
            }
    }    
    
    public boolean collide(PhysicsCollisionObject nodeA, PhysicsCollisionObject nodeB) {
        return false;
    }
    
}