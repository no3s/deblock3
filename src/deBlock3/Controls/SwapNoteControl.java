
package deBlock3.Controls;

import com.leff.midi.event.NoteOn;
import deBlock3.StageData;
import deBlock3.Player;

/**
 *
 * @author no3z
 */
public class SwapNoteControl extends AbstractNoteControl{
    
                
    public SwapNoteControl(String name, NoteOn note, StageData.MutableProperties mProp)
    {
        super(name, note, mProp);          
    }     
    
    public void performCollision(Player player)
    {
        if(mProperties.options.isEmpty() || mProperties.options.equals("NONE")) 
        {
            player.setControlSwapped(!player.getControlSwapped());
            return; 
        }
           
        if(mProperties.options.contains("SWAP:")) 
        {
            String temp = this.mProperties.options.substring(this.mProperties.options.lastIndexOf("SWAP:")+5);
            if(!temp.isEmpty()) {
                Boolean t = Boolean.parseBoolean(temp);
                player.setControlSwapped(t);                
            } else {
                player.setControlSwapped(!player.getControlSwapped());
            }
        }
    }
    
}
