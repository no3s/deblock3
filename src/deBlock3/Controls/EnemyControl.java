/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.Controls;

import com.leff.midi.event.NoteOn;
import static deBlock3.Controls.AbstractNoteControl.isNumeric;
import deBlock3.StageData;
import deBlock3.Player;


/**
 *
 * @author no3z
 */
public class EnemyControl  extends AbstractNoteControl  {
                        
    public EnemyControl(String name, NoteOn note, StageData.MutableProperties mProp)
    {
        super(name, note, mProp);          
    }    
    
    public void performCollision(Player player)
    {                
        if(mProperties.options.isEmpty() || mProperties.equals("NONE")) 
        {
            player.addEnergy(-1);
            player.hitplayer();
            return; 
        }
        
        if(mProperties.options.contains("BAD:")) {
            String temp = this.mProperties.options.substring(this.mProperties.options.lastIndexOf("BAD:")+4);
            if(!temp.isEmpty() && isNumeric(temp))
            {
                Integer t = Integer.parseInt(temp);
                player.addEnergy(-t);
                player.hitplayer();
            }
        } else {
            player.addEnergy(-1);
            player.hitplayer();
        }

    }
}
