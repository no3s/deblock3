package deBlock3.Controls;

import com.jme3.bullet.BulletAppState;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.Savable;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import deBlock3.SceneAbstract;
import java.io.IOException;


public class NodeRemoveByTimerControl extends AbstractControl implements Savable, Cloneable {

    private long deltaTime = 0;
    private float countTime = 0;
    private BulletAppState bulletState = null;
    
    public NodeRemoveByTimerControl(){}
    
    public NodeRemoveByTimerControl(BulletAppState bulletState, long deltaTime){
        this.deltaTime = deltaTime;
        this.bulletState = bulletState;    
    }
    
    @Override
    protected void controlUpdate(float tpf){
     if(spatial != null) {
         countTime += tpf*60.0f; 
         
         AbstractNoteControl noteControl = spatial.getControl(AbstractNoteControl.class);
         
         if(noteControl == null) 
         {
            spatial.removeFromParent();
            bulletState.getPhysicsSpace().remove(spatial);    
            return;
         }
         
         if(noteControl.isSoundPlaying() == true) return;
         
         if(countTime > deltaTime || noteControl.bDeleted == true /*|| SceneAbstract.getCameraPosZ() > noteControl.getGeometry().getLocalTranslation().z*/)
         {
            spatial.removeFromParent();
            bulletState.getPhysicsSpace().remove(spatial);
             
         }
     }
    }
 
    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        //spatial.setUserData("deltaTime", i); // example
    }

    @Override
    public Control cloneForSpatial(Spatial spatial){
    final NodeRemoveByTimerControl control = new NodeRemoveByTimerControl(bulletState,deltaTime);
    /* Optional: use setters to copy userdata into the cloned control */
    // control.setIndex(i); // example
    control.setSpatial(spatial);
    return control;
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp){
    /* Optional: rendering manipulation (for advanced users) */
    }

    @Override
    public void read(JmeImporter im) throws IOException {
    super.read(im);
    // im.getCapsule(this).read(...);
    }

    @Override
    public void write(JmeExporter ex) throws IOException {
    super.write(ex);
    // ex.getCapsule(this).write(...);
    }

}

