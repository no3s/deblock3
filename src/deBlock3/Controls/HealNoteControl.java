/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.Controls;

import com.leff.midi.event.NoteOn;
import static deBlock3.Controls.AbstractNoteControl.isNumeric;
import deBlock3.StageData;
import deBlock3.Player;

/**
 *
 * @author no3z
 */
public class HealNoteControl extends AbstractNoteControl{
    
                
    public HealNoteControl(String name, NoteOn note, StageData.MutableProperties mProp)
    {
        super(name, note, mProp);          
    }     
    
    public void performCollision(Player player)
    {
        if(mProperties.options.isEmpty() || mProperties.options.equals("NONE")) 
        {
            player.addEnergy(1);
            return; 
        }
           
        if(mProperties.options.contains("HEAL:")) 
        {
            String temp = this.mProperties.options.substring(this.mProperties.options.lastIndexOf("HEAL:")+5);
            if(!temp.isEmpty() && isNumeric(temp))
            {
                Integer t = Integer.parseInt(temp);
                player.addEnergy(t);
            }
        } else {
            player.addEnergy(1);
        }
    }
    
}
