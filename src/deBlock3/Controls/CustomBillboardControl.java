package deBlock3.Controls;


import com.jme3.scene.control.AbstractControl;

import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import java.io.IOException;

public class CustomBillboardControl extends AbstractControl {

    private Matrix3f orient;
    private Vector3f look;
    private Vector3f left;

    public CustomBillboardControl() {
        super();
        orient = new Matrix3f();
        look = new Vector3f();
        left = new Vector3f();
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        CustomBillboardControl control = new CustomBillboardControl();
        control.setSpatial(spatial);
        return control;
    }

    @Override
    protected void controlUpdate(float tpf) {
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        Camera cam = vp.getCamera();
        rotateBillboard(cam);
    }
    
    private void fixRefreshFlags(){
        // force transforms to update below this node
        spatial.updateGeometricState();
        
        // force world bound to update
        Spatial rootNode = spatial;
        while (rootNode.getParent() != null){
            rootNode = rootNode.getParent();
        }
        rootNode.getWorldBound(); 
    }

    /**
     * rotate the billboard based on the type set
     *
     * @param cam
     *            Camera
     */
    private void rotateBillboard(Camera cam) {
                rotateAxial(cam, Vector3f.UNIT_X);
        }


    private void rotateAxial(Camera camera, Vector3f axis) {
        // Compute the additional rotation required for the billboard to face
        // the camera. To do this, the camera must be inverse-transformed into
        // the model space of the billboard.
        look.set(camera.getLocation()).subtractLocal(
                spatial.getWorldTranslation());   
        spatial.getParent().getWorldRotation().mult(look, left); // coopt left for our own
        // purposes.
        left.x *= 1.0f / spatial.getWorldScale().x;
        left.y *= 1.0f / spatial.getWorldScale().y;
        left.z *= 1.0f / spatial.getWorldScale().z;

        // squared length of the camera projection in the xz-plane
        float lengthSquared = left.x * left.x + left.z * left.z;
        if (lengthSquared < FastMath.FLT_EPSILON) {
            // camera on the billboard axis, rotation not defined
            return;
        }

        // The billboard must be oriented to face the camera before it is
        // transformed into the world.        
        spatial.setLocalRotation(orient);
        spatial.rotate(-90.0f, 0.0f,0.0f);
        fixRefreshFlags();
    }



    @Override
    public void write(JmeExporter e) throws IOException {
        super.write(e);
        OutputCapsule capsule = e.getCapsule(this);
        capsule.write(orient, "orient", null);
        capsule.write(look, "look", null);
        capsule.write(left, "left", null);
    }

    @Override
    public void read(JmeImporter e) throws IOException {
        super.read(e);
        InputCapsule capsule = e.getCapsule(this);
        orient = (Matrix3f) capsule.readSavable("orient", null);
        look = (Vector3f) capsule.readSavable("look", null);
        left = (Vector3f) capsule.readSavable("left", null);      
    }
}
