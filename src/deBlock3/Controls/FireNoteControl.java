package deBlock3.Controls;

import com.leff.midi.event.NoteOn;
import deBlock3.StageData;
import deBlock3.MidiMessageProcessorSingleton;
import deBlock3.Player;

/**
 *
 * @author no3z
 */
public class FireNoteControl extends AbstractNoteControl{
    
    private Integer channel;
    private Integer note;
                
    public FireNoteControl(String name, NoteOn note, StageData.MutableProperties mProp)
    {
        super(name, note, mProp);          
    }     
    
    public void performCollision(Player player)
    {        
        if(!this.mProperties.options.isEmpty())
        {
            if(parseChannelNote(mProperties.options))
            {            
                MidiMessageProcessorSingleton.getInstance().create(0, channel, note, 0);
            }
        }
    }
    
    private Boolean parseChannelNote(String temp)
    {     
        if((temp.contains("NONE") || temp.isEmpty()) && !temp.contains("NOTE:")) { return false; }
                
        temp = temp.substring(temp.lastIndexOf("NOTE:")+5);
        
        String delim = "[,]";
        String[] tokens = temp.split(delim);
        
        for (int i = 0; i < tokens.length; i++)
        {           
            //Avoid brackets            
            if(tokens.length != 2) {   return false; }                                    
            channel = Integer.parseInt(tokens[0].trim());
            note = Integer.parseInt(tokens[1].trim());
            return true;
        }
        return false;
    }
    
}
