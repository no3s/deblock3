/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3;

import com.leff.midi.event.MidiEvent;
import com.leff.midi.event.NoteOn;
import deBlock3.Controls.AbstractNoteControl;
import deBlock3.Controls.CustomBillboardControl;
import deBlock3.Controls.EnemyControl;
import deBlock3.Controls.FireNoteControl;
import deBlock3.Controls.GoodNoteControl;
import deBlock3.Controls.HealNoteControl;
import deBlock3.Controls.NodeRemoveByTimerControl;
import deBlock3.Controls.SimpleNoteControl;
import deBlock3.Controls.SwapNoteControl;
import deBlock3.StageData.MutableProperties;
import java.util.Stack;
import java.util.logging.Logger;

/**
 *
 * @author no3z
 */
public class MidiMessageProcessorSingleton {        

    private static final Logger logger = Logger.getLogger(MidiMessageProcessorSingleton.class.getName());
    
    private static MidiMessageProcessorSingleton instance = null;
    protected SceneAbstract scene = null;
    protected StageData mStageData;
    final protected Stack<NoteOn> noteOnDeQueue = new Stack<NoteOn>();
    
    protected MidiMessageProcessorSingleton() { // Exists only to defeat instantiation.
    }
      
    public static MidiMessageProcessorSingleton getInstance() {
      if(instance == null) { instance = new MidiMessageProcessorSingleton(); }
      return instance;
   }
    
    public void setUpScene(SceneAbstract scene, StageData mLevel)
    {
        this.mStageData = mLevel;
        this.scene = scene;
    }    

    public StageData     getStageData() {return mStageData;}
    public SceneAbstract getScene() {return scene;}
    
    public void create(long tick, int channel, int key, int position) 
     {    
        NoteOn note = new NoteOn(tick, channel, key, position);
        noteOnDeQueue.push(note);
     }
     
    public void create(MidiEvent event) 
    {    
        if(event.getClass().equals(NoteOn.class))
        {          
            NoteOn note = (NoteOn)event;    
            if(note.getVelocity() != 0) { 
                noteOnDeQueue.push(note);
                //System.out.println(note.getChannel() + " "+ note.getNoteValue());
            }
        }                          
    }
            
  public void update(float tpf)
  {

     while(!noteOnDeQueue.empty())
     {
         //calculateNoteArray();
         NoteOn note = noteOnDeQueue.pop();
         int channel = note.getChannel();
  
         if(getStageData().isTrackDefined(channel) == false ) 
         {
            //System.out.println("Discard note on channel: " + channel + " max channels: " + getStageData().getNumTracks() );
             continue;
         }
         String cannonName = "ch:"+channel+"_note_"+note.getNoteValue();  
         NoteOnGeometry noteOnGeometry = new NoteOnGeometry(cannonName, note);
         if(noteOnGeometry.skip == false) {
            noteOnGeometry.call();        
         }
     }
  }
  
   private class NoteOnGeometry {

        public Boolean skip = false;
       
        private NodeRemoveByTimerControl remove_control  = null; 
        
        private AbstractNoteControl noteControl = null;
                
        private CustomBillboardControl abstractControl = null;
        
        public NoteOnGeometry(String name, NoteOn note) {
            
            MutableProperties mP = mStageData.getMutableProperties(note.getChannel(), note.getNoteValue(), scene.getMeasure());
            
            if(mP == null) { skip = true; return; }
            
            if(mP.skip) { skip = true; return; }
            
            String tracktype = mP.type;            
           
            float durationFactor = mP.objectDuration;
           
            long temp = (long)(note.getDuration());
            if(durationFactor > 0.0)
            {
                temp = (long)durationFactor;
            }
            

            if(temp < 0) temp = 200;
            
            remove_control  =  new NodeRemoveByTimerControl(scene.getBulletAppState(),temp);            
            
            if(tracktype.equals("BAD")) { noteControl = new EnemyControl(name, note, mP); }
            else if(tracktype.equals("GOOD")) { noteControl = new GoodNoteControl(name, note, mP ); }      
            else if(tracktype.equals("HEAL")) { noteControl = new HealNoteControl(name, note, mP ); }    
            else if(tracktype.equals("FIRE")) { noteControl = new FireNoteControl(name, note, mP ); }      
            else if(tracktype.equals("SWAP")) { noteControl = new SwapNoteControl(name, note, mP ); }      
            else { noteControl = new SimpleNoteControl(name, note, mP); }
            
            if(mP.material != null && mP.material.getParam("AniTexMap") != null)
            {                
                abstractControl = new CustomBillboardControl();                
            }
                        
        }

        public Void call(){                                         
           noteControl.initialize(scene);                                                  
                   
           noteControl.getGeometry().addControl(noteControl);
                      
           noteControl.getGeometry().addControl(remove_control);            
         
           if(abstractControl != null)
           {
               noteControl.getGeometry().addControl(abstractControl);
//               abstractControl.setAlignment(BillboardControl.Alignment.AxialZ);
           }
           
           scene.getBulletAppState().getPhysicsSpace().add(noteControl.getGeometry());        
            
           return null;
          }
    }
}
