/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3;

import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.export.Savable;
import deBlock3.StageData.HighScore;
import deBlock3.Loaders.GameStageLoader;
import java.io.IOException;

/**
 *
 * @author no3z
 */
    public class GameStageSerializableData implements Savable
    {
       public static final int SAVABLE_VERSION = 8;
       
       public GameStageSerializableData(){}
        
       public void write(JmeExporter ex) throws IOException {           
        OutputCapsule capsule = ex.getCapsule(this);
        
        capsule.write(SAVABLE_VERSION, "SAVABLE_VERSION", 0);
        
        for(int i = 0; i < GameStageLoader.getInstance().numStagesLoaded(); ++i)
        {
            StageData mStageData = GameStageLoader.getInstance().getStageAt(i);
                                     
            capsule.write(mStageData.getStageCleared() ? "TRUE" : "FALSE", mStageData.getLabel() + ":StageCleared", "FALSE");
            
            for(int j = 0; j < StageData.MAX_HIGHSCORES; j++)
            {
                int score = 0; String name = "no3z"; String date = "";
                HighScore hs = mStageData.getHighScore(j);
                
                if(hs != null) {
                    score = hs.score;
                    name = hs.playerName;
                    date = hs.date;
                }
                capsule.write(name, mStageData.getLabel() + ":HighScore_PlayerName" + j, "no3z");
                capsule.write(score, mStageData.getLabel() + ":HighScore" + j, 0); 
                capsule.write(date, mStageData.getLabel() + ":HighScore_Date" + j, ""); 
            }
        }
       }

       public void read(JmeImporter im) throws IOException {
           InputCapsule capsule = im.getCapsule(this);
           
           int version = capsule.readInt("SAVABLE_VERSION", 0);
           
           if(version == SAVABLE_VERSION)
           {
                for(int i = 0; i < GameStageLoader.getInstance().numStagesLoaded(); ++i)
                {
                    StageData mStage = GameStageLoader.getInstance().getStageAt(i);   
                    
                    String cleared = capsule.readString(mStage.getLabel() + ":StageCleared", "FALSE");
                    
                    if(cleared.contains("FALSE")) { mStage.setStageCleared(false); }
                    else { mStage.setStageCleared(true); }
                                            
                    for(int j = 0; j < StageData.MAX_HIGHSCORES; j++)
                    {
                         String hsPlayer = capsule.readString(mStage.getLabel() + ":HighScore_PlayerName" + j, "no3z");
                         int highScore = capsule.readInt(mStage.getLabel() + ":HighScore" + j, 0);
                         String date = capsule.readString(mStage.getLabel() + ":HighScore_Date" + j, "");
                         mStage.setHighScoreMap(j, highScore, hsPlayer, date);                                
                    }
                }
           }
        }           
    }
    