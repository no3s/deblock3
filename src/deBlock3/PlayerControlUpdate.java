package deBlock3;

import com.jme3.cinematic.MotionPath;
import com.jme3.cinematic.MotionPathListener;
import com.jme3.cinematic.events.MotionEvent;
import com.jme3.input.InputManager;
import com.jme3.input.controls.ActionListener;
import com.jme3.math.Vector3f;
import java.util.Stack;

/**
 *
 * @author no3z
 */
public abstract class PlayerControlUpdate {   
    
    protected SceneAbstract scene = null;
    protected Player player = null;
    final protected Stack<PlayerControlInformation> controlQueue = new Stack<PlayerControlInformation>();
    
    protected boolean bExecuting_motion_path = false;
    protected int iPosX = 0;
    protected int iPosZ = MAX_Z_POSITION;
    static public int MAX_Z_POSITION = -3;
    static public int MIN_Z_POSITION = -3;
        
    private Vector3f walkDirection = new Vector3f();
    private Vector3f nextPoint = new Vector3f();
    private MotionPath tPath = null;
    private MotionEvent tMotionControl = null;
    private float fMoveSpeed = 1.0f;
    private float fMoveDuration = 1.0f;
    
    protected int fScreenWidth;
    protected int fScreenHeight;
    
    PlayerControlUpdate(Player player){
        this.player = player;
        this.scene = player.getScene();
        
        fMoveDuration = scene.getFrequency()/2;    
        fMoveSpeed = 1.2f;
        tPath = new MotionPath();        
        tMotionControl = new MotionEvent(scene.getPlayer().getPlayerGeometry(), tPath);        
        tMotionControl.setInitialDuration(fMoveDuration);
        tMotionControl.setSpeed(fMoveSpeed);  
        tPath.addListener(new MotionPathListener() {

         public void onWayPointReach(MotionEvent control, int wayPointIndex) {
             if (tPath.getNbWayPoints() == wayPointIndex + 1) {
                 bExecuting_motion_path = false;
                 tMotionControl.stop();
                 control.getSpatial().setLocalTranslation(scene.getGame().getStageData().getStageGapBetweenTracksSize() * iPosX, 1.0f, 5.0f * iPosZ);
             }
         }
        });
        
        fScreenWidth = scene.getGame().getContext().getSettings().getWidth();
        fScreenHeight = scene.getGame().getContext().getSettings().getHeight();
    }
    
    Vector3f updatePlayer(float tpf)
    {
        while(!controlQueue.empty())
        {
            PlayerControlInformation pci = controlQueue.pop();
            
            if( bExecuting_motion_path || pci.value) { controlQueue.clear(); break; }                
        
            bExecuting_motion_path = true;
            
            fMoveDuration = 50.0f / (float)scene.getFrequencyDurationInMs();        
            tMotionControl.setInitialDuration(fMoveDuration);
            tMotionControl.setSpeed(fMoveSpeed);                                       
            tPath.clearWayPoints();
            walkDirection.set(scene.getGame().getStageData().getStageGapBetweenTracksSize() * iPosX, 1.0f, 5.0f * iPosZ);
            tPath.addWayPoint(walkDirection);      
            nextPoint.set(walkDirection);
        
            if (pci.binding.equals("move_box_x_pos") && !pci.value) {
                //iPosX -= 1;
                iPosX = iPosX + (player.getControlSwapped() ? 1 : -1);
                nextPoint.set(scene.getGame().getStageData().getStageGapBetweenTracksSize() * iPosX, 1.0f, 5.0f * iPosZ);
            } 
            else if (pci.binding.equals("move_box_x_neg") && !pci.value) {
                //iPosX += 1;
                iPosX = iPosX + (player.getControlSwapped() ? -1 : 1);
                nextPoint.set(scene.getGame().getStageData().getStageGapBetweenTracksSize() * iPosX, 1.0f, 5.0f * iPosZ);
            } 
            else if (pci.binding.equals("move_box_z_pos") && !pci.value) {
                //iPosZ += 1;
                iPosZ = iPosZ + (player.getControlSwapped() ? -1 : 1);
                nextPoint.set(scene.getGame().getStageData().getStageGapBetweenTracksSize() * iPosX, 1.0f, 5.0f * iPosZ);
            } 
            else if (pci.binding.equals("move_box_z_neg") && !pci.value) {
                //iPosZ -= 1;
                iPosZ = iPosZ + (player.getControlSwapped() ? 1 : -1);
                nextPoint.set(scene.getGame().getStageData().getStageGapBetweenTracksSize() * iPosX, 1.0f, 5.0f * iPosZ);
            }
        
            int numTracks =  scene.getNumTracksInFile();                    
            
            if(iPosX > numTracks-1) 
            {
                iPosX = numTracks-1;
                bExecuting_motion_path = false;
                continue;
            }
        
            if(iPosX < 0)
            {
                iPosX = 0;
                bExecuting_motion_path = false;
                continue; 
            }
        
            if(iPosZ > MAX_Z_POSITION) 
            {
                iPosZ = MAX_Z_POSITION;
                bExecuting_motion_path = false;
                continue; 
            }
                
            if(iPosZ < MIN_Z_POSITION)
            {
                iPosZ = MIN_Z_POSITION;
                bExecuting_motion_path = false;
                continue; 
            }
        
            tPath.addWayPoint(nextPoint);

            tMotionControl.play();   
            
            controlQueue.clear();
        }                
        
        walkDirection = tMotionControl.getSpatial().getLocalTranslation();                              
        return walkDirection;
    }
    
    public void clearMappings()
    {
         InputManager inputManager = scene.getGame().getInputManager();
         inputManager.clearMappings();
    }
    
    abstract public ActionListener AsActionListener();
    
    int getPosX() { return iPosX;}
    void setPoxX(int px) { iPosX = px; }
    int getPosZ() { return iPosZ;}
       
    public String getPlayerPosition(){
        return (iPosX + "," + iPosZ);
    }
    
    public void addPlayerControlInfo(String b, boolean value, float tpf)
    {
        controlQueue.push(new PlayerControlInformation(b, value, tpf));     
    }
    
    class PlayerControlInformation {
        PlayerControlInformation(String bind, boolean value, float tpf) 
        {
            this.binding = bind;
            this.value = value;
            this.tpf = tpf;
        }
        
        protected String binding;
        protected boolean value;
        protected float tpf;        
    }
    
    
}
