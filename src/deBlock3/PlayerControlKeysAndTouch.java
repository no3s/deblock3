package deBlock3;

import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.TouchInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.TouchListener;
import com.jme3.input.controls.TouchTrigger;
import com.jme3.input.event.TouchEvent;

public class PlayerControlKeysAndTouch extends PlayerControlUpdate implements ActionListener, TouchListener { 



    public ActionListener AsActionListener() {
        return this;
    }
        
    PlayerControlKeysAndTouch(Player player){
        super(player);
        setUpKeys();        
    }
    
     private void setUpKeys() {
        InputManager inputManager = scene.getGame().getInputManager();
        inputManager.addMapping("move_box_x_neg", new KeyTrigger(KeyInput.KEY_A));        
        inputManager.addMapping("move_box_x_pos", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("move_box_x_neg", new KeyTrigger(KeyInput.KEY_LEFT));
        inputManager.addMapping("move_box_x_pos", new KeyTrigger(KeyInput.KEY_RIGHT));
        inputManager.addMapping("move_box_z_pos", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("move_box_z_neg", new KeyTrigger(KeyInput.KEY_S));
        
        inputManager.addListener(this, "move_box_x_pos");
        inputManager.addListener(this, "move_box_x_neg");
        inputManager.addListener(this, "move_box_z_pos");
        inputManager.addListener(this, "move_box_z_neg");
        
        inputManager.addMapping("Touch", new TouchTrigger(0));
        inputManager.addMapping("TouchEscape", new TouchTrigger(TouchInput.KEYCODE_BACK));
        inputManager.addListener(this, new String[]{"Touch"});
        inputManager.addListener(this, new String[]{"TouchEscape"});
    }                         
        
    public void onAction(String binding, boolean value, float tpf) 
    {         
        controlQueue.push(new PlayerControlInformation(binding, value, tpf));        
    }
     
    public void onTouch(String binding, TouchEvent evt, float tpf) {
        float x;
        float y;
        float pressure;
        switch(evt.getType())
        {            
            case DOWN:
                x = evt.getX();
                y = evt.getY();            
                
                //if (x>0&&x<fScreenWidth*0.5&&y>0&&y<fScreenHeight*0.5)
                if (x>0&&x<fScreenWidth*0.5&&y>0&&y<fScreenHeight)
                {
                    controlQueue.push(new PlayerControlInformation("move_box_x_neg", false, tpf));  
                }

                //if (x>fScreenWidth*0.5&&x<fScreenWidth&&y>0&&y<fScreenHeight*0.5)
                if (x>fScreenWidth*0.5&&x<fScreenWidth&&y>0&&y<fScreenHeight)
                { 
                    controlQueue.push(new PlayerControlInformation("move_box_x_pos", false, tpf));      
                }
                
//                if (x>0&&x<fScreenWidth&&y>fScreenHeight*0.75&&y<fScreenHeight)
//                { 
//                    player.getScene().getGame().loadMenu();
//                }
                                
            break;
               
            case KEY_UP:
                if (binding.equals("TouchEscape")) 
                {
                     player.getScene().getGame().loadMenu();
                }
            break;
            default:
            break;
        }
    }
}
