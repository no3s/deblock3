/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3;

import com.jme3.system.NanoTimer;
import deBlock3.AppStates.GameState;
import deBlock3.AppStates.HighScoreState;
import deBlock3.Main.eGameState;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import tv.ouya.console.api.OuyaController;

/**
 *
 * @author no3z
 */
public class Controller implements Callable {
    
    private static final Logger logger = Logger.getLogger(Controller.class.getName());
    
    Main game = null;
    public Controller(int dId) { 
        deviceId = dId;        
    }
    
    public void setGame(Main g) { game = g; }
    /**
     * Take out the lock on this object. No input can be received until this lock is released
     */
    public static final AtomicInteger lock = new AtomicInteger();
    private static final NanoTimer timer = new NanoTimer();
    
    private boolean sticksCentred;    
    
    private int deviceId = -1;

    /**
     * Was O button pressed down
     */
    public boolean BUTTON_O = false;
    public boolean BUTTON_U = false;
    public boolean BUTTON_Y = false;
    public boolean BUTTON_A = false;
    
    //mouse
    public float Pointer_X = 0f;
    public float Pointer_Y = 0f;
        
    public float AXIS_L2 = 0f;
    public float AXIS_LS_X = 0f;
    public float AXIS_LS_Y = 0f;
    public float AXIS_R2 = 0f;
    public float AXIS_RS_X = 0f;
    public float AXIS_RS_Y = 0f;
    public boolean BUTTON_DPAD_DOWN = false;
    public boolean BUTTON_DPAD_LEFT = false;
    public boolean BUTTON_DPAD_RIGHT = false;
    public boolean BUTTON_DPAD_UP = false;
    public boolean BUTTON_L1 = false;
    public boolean BUTTON_L2 = false;
    public boolean BUTTON_L3 = false;
    public boolean BUTTON_R1 = false;
    public boolean BUTTON_R2 = false;
    public boolean BUTTON_R3 = false;
    public boolean BUTTON_MENU = false;
    
    public boolean MAX_CONTROLLERS = false;
    public float STICK_DEADZONE = 0.25f;
    //Left Stick position on X axis
    private float LS_X = 0;
    //Left Stick position on Y axis
    private float LS_Y = 0;
    //Right Stick position on X axis
    private float RS_X = 0;
    //Right Stick position on Y axis
    private float RS_Y = 0;
    
    private int keyEvent = -1;
    private float elapsedTime = 0.161083f * 3.0f; //1.61083f;
    
    /**
     * Records the positions of the left and right sticks when a
     * {@link MainActivity#onGenericMotionEvent(android.view.MotionEvent)}
     * is triggered <p> Note: Calls static methods of {@link OuyaController}
     */
    public void updateSticks() {   
        if(game.getGameStateEnum() == eGameState.GAME) return;
        OuyaController ouyacontroller = OuyaController.getControllerByDeviceId(deviceId);
        if (ouyacontroller != null) {
            LS_X = ouyacontroller.getAxisValue(OuyaController.AXIS_LS_X);
            LS_Y = ouyacontroller.getAxisValue(OuyaController.AXIS_LS_Y);
            RS_X = ouyacontroller.getAxisValue(OuyaController.AXIS_RS_X);
            RS_Y = ouyacontroller.getAxisValue(OuyaController.AXIS_RS_Y);
        } else {
            return;            
        }

    if (LS_X * LS_X + LS_Y * LS_Y < OuyaController.STICK_DEADZONE * OuyaController.STICK_DEADZONE) {
        LS_X = LS_Y = 0.0f;
    }

    
    if (RS_X * RS_X + RS_Y * RS_Y < OuyaController.STICK_DEADZONE * OuyaController.STICK_DEADZONE) {
        RS_X = RS_Y = 0.0f;
    }
       
    if(LS_X > 0.35)
        {              
            if(timer.getTimeInSeconds() > elapsedTime) { UP_RIGHT(game.getGameStateEnum()); doCount(); }              
        }
    else if(LS_X < -0.35)
        { 
            if(timer.getTimeInSeconds() > elapsedTime) { DOWN_LEFT(game.getGameStateEnum());  doCount();}            
        }
    else if(RS_X > 0.35)
        {             
            if(timer.getTimeInSeconds() > elapsedTime) { UP_RIGHT(game.getGameStateEnum());  doCount();}            
        }
    else if(RS_X < -0.35)
        { 
            if(timer.getTimeInSeconds() > elapsedTime) { DOWN_LEFT(game.getGameStateEnum());  doCount(); }            
        }
    }
    
    private void doCount()
    {         
        timer.reset();
        timer.update();
    }
    
    private Boolean UP_RIGHT(eGameState state)
    {
        if(state == eGameState.MENU)
        {
            int cl = game.getCurrentStage();
            game.setStageData(game.getCurrentStage()+1);                                                           
            if( game.getCurrentStage() == cl) { game.playErrorSound();} 
            else { game.playMoveSound(); }   
            return true;
        }  
        else if(state == eGameState.HIGHSCORE)
        {
            HighScoreState gs = game.getStateManager().getState(HighScoreState.class);
            if(gs == null) { logger.log(Level.SEVERE,"No game state set"); return false; }       
            game.playMoveSound();
            gs.nextLetter();
            return true;
        }
        else if(state == eGameState.GAME)
        {
            if(game.getGameState().getScene() != null 
            && game.getGameState().getScene().getPlayer() != null)
            {
                game.getGameState().getScene().getPlayer().playerInput.addPlayerControlInfo("move_box_x_pos", false, 0.01f);
            }
            return true;
        }
        return false;
    }
    
    private Boolean DOWN_LEFT(eGameState state)
    {
        if(state == eGameState.MENU)
        {
            int cl = game.getCurrentStage();
            game.setStageData(game.getCurrentStage()-1);    
            if( game.getCurrentStage() == cl) { game.playErrorSound();} 
            else { game.playMoveSound(); }             
            return true;
        }  
        else if(state == eGameState.HIGHSCORE)
        {
            HighScoreState gs = game.getStateManager().getState(HighScoreState.class);
            if(gs == null) { logger.log(Level.SEVERE,"No game state set"); return false; }       
            game.playMoveSound();
            gs.prevLetter();
            return true;
        }
        else if(state == eGameState.GAME)
        {
            if(game.getGameState().getScene() != null 
            && game.getGameState().getScene().getPlayer() != null)
            {        
                game.getGameState().getScene().getPlayer().playerInput.addPlayerControlInfo("move_box_x_neg", false, 0.01f);                
            }
            return true;
        }        
        return false;
    }
    
    //controller button press
    public Boolean ButtonStateEvent(int keyCode, int keyAction) {
        if(keyAction == 1){//down
                return false;
        }
	
        eGameState state = game.getGameStateEnum();
        
        keyEvent = keyCode;
        switch (keyCode) {
            case OuyaController.BUTTON_O:
                game.enqueue(this); return true;                                   
            case OuyaController.BUTTON_U:
                return DOWN_LEFT(state); 
            case OuyaController.BUTTON_Y:
            	game.enqueue(this); return true;    
            case OuyaController.BUTTON_A:
            	return UP_RIGHT(state); 
            case OuyaController.BUTTON_DPAD_DOWN:
            	return DOWN_LEFT(state); 
            case OuyaController.BUTTON_DPAD_LEFT:
            	return DOWN_LEFT(state); 
            case OuyaController.BUTTON_DPAD_RIGHT:
            	return UP_RIGHT(state); 
            case OuyaController.BUTTON_DPAD_UP:
            	return UP_RIGHT(state); 
            case OuyaController.BUTTON_L1:            	
            	//DOWN_LEFT(state);
                break;
            case OuyaController.BUTTON_L2:
            	//BUTTON_L2 = bstate;
                break;
            case OuyaController.BUTTON_L3:
            	//BUTTON_L3 = bstate;
                break;
            case OuyaController.BUTTON_R1:
                //UP_RIGHT(state);
                break;
            case OuyaController.BUTTON_R2:
            	//BUTTON_R2 = bstate;
                break;
            case OuyaController.BUTTON_R3:
            	//BUTTON_R3 = bstate;
                break;
            case OuyaController.BUTTON_MENU:
            	if(state == eGameState.MENU)
                {
                   game.enqueue(this);     
                   return true;
                }  
                else
                {
                    GameState gs = game.getStateManager().getState(GameState.class);
                    if(gs == null)  { logger.log(Level.SEVERE,"No game state set"); return false; }                   
                    gs.getScene().finished();
                    return true;
                }              
        }
        return false;
    }

    
    public Object call() throws Exception {
        eGameState state = game.getGameStateEnum();
        switch (keyEvent) {
            case OuyaController.BUTTON_O:
                if(state == eGameState.MENU) {
                    if(game.getIsPrevStageCleared()) { game.playOKSound(); game.loadGame(); } 
                    else { game.playErrorSound(); }        
                }
                else if(state == eGameState.START)
                {
                    game.playOKSound();
                    game.loadMenu();
                }  
                else if(state == eGameState.HIGHSCORE)
                {
                    HighScoreState gs = game.getStateManager().getState(HighScoreState.class);
                    if(gs != null) { 
                        game.playOKSound();
                        gs.OKLetter();
                    }
                }
                break;
            case OuyaController.BUTTON_Y:
                if(state == eGameState.HIGHSCORE)
                {
                    HighScoreState gs = game.getStateManager().getState(HighScoreState.class);
                    if(gs != null) { 
                        game.playErrorSound();
                        gs.delLetter();
                    }
                }
                break;  
            case OuyaController.BUTTON_MENU:
                if(state == eGameState.MENU)
                {
                    game.playErrorSound();
                    game.loadStart();  
                }  
                break;
            default:
                break;
        }
        return null;
    }
}
