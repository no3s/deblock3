package deBlock3;

import deBlock3.AppStates.GameState;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.cinematic.MotionPath;
import com.jme3.cinematic.MotionPathListener;
import com.jme3.cinematic.events.MotionEvent;
import com.jme3.material.Material;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitorAdapter;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;

public class Player {
    
    private SceneAbstract scene = null;
    private RigidBodyControl player;
    private Geometry geometry = null;                    
    public PlayerControlUpdate playerInput = null;
                    
    private int iPoints = 0;
    static private int MAX_ENERGY = 4;
    static private int MAX_LIVES = 2;
    private int iLives = MAX_LIVES;
    private int iEnergy = MAX_ENERGY;

    
    private MotionPath tPathCallback = null;
    private MotionEvent tMotionControlCallback = null;    
    
    private MotionPath tPathDie = null;
    private MotionEvent tMotionControlDie = null;    
    
    private Boolean bDoHitSecurityTime = false;    
    private float hittime = 0.75f;
    private float hittimecounter = 0.0f;
    
    private Boolean  bControlSwapped = false;
    
    private Boolean bIsBoxSoRotate = false;
    
    private String stStatus; //Can be: HIT, RESPAWN, PLAY, DEAD

    private Float rotVelocity = 1.5f;
    
    Material mat_normal = null;    
    Material mat_hit = null;
    Material mat_revert = null;
    
    public Player(SceneAbstract scene)
    {        
        this.scene = scene;                                            
    }
    
    public void initialize(){                
                
        if(scene.getStageData().getPlayerModel().contains("box.j3o"))
        {
            Box box = new Box(1.0f,1.0f,1.0f);
            geometry = new Geometry("box",box);    
            bIsBoxSoRotate = true;
        }

        else if(scene.getStageData().getPlayerModel().contains("sphere.j3o"))
        {
            Sphere sphere = new Sphere(16,16,1.0f);
            geometry= new Geometry("box",sphere);                    
        }
        else {                            
            Node loadedNode = (Node)scene.getGame().getAssetManager().loadModel("Models/" + scene.getStageData().getPlayerModel());
            loadedNode.setName(scene.getStageData().getPlayerModel());            
            loadedNode.depthFirstTraversal(new SceneGraphVisitorAdapter(){
            @Override
            public void visit(Geometry g){
                geometry = g;
            } });            
        }
        
        geometry.setName("player");
        geometry.setQueueBucket(RenderQueue.Bucket.Transparent);
        
        mat_normal = scene.getStageData().getPlayerMaterial();
        mat_hit = scene.getStageData().getPlayerMaterialHit();    
        mat_revert = scene.getStageData().getPlayerMaterialRevert();    
        
        geometry.setMaterial(mat_normal);        
        geometry.setLocalTranslation(0.0f,1.0f,-15.0f);
        geometry.scale(scene.getStageData().getPlayerScale().x,
                        scene.getStageData().getPlayerScale().y,
                        scene.getStageData().getPlayerScale().z);
        
        CollisionShape collisionShape = new BoxCollisionShape(new Vector3f(1.0f,1.0f,1.0f));  
        player = new RigidBodyControl(collisionShape, 100.50f);            
        
        geometry.addControl(player);   
                
        stStatus = "PLAY";
                
        player.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_01);
        player.setCollideWithGroups(PhysicsCollisionObject.COLLISION_GROUP_02);
        player.addCollideWithGroup(PhysicsCollisionObject.COLLISION_GROUP_03);
                 
        tPathDie = new MotionPath();        
        tMotionControlDie = new MotionEvent(geometry, tPathDie);        
        tMotionControlDie.setInitialDuration(1.0f);
        tMotionControlDie.setSpeed(1.0f);          
        tPathDie.addListener(new MotionPathListener() {
        public void onWayPointReach(MotionEvent control, int wayPointIndex) {
                if (tPathDie.getNbWayPoints() == wayPointIndex + 1) {
                     tMotionControlDie.stop();                    
                }
            }
         });
              
        tPathCallback = new MotionPath();        
        tMotionControlCallback = new MotionEvent(geometry, tPathCallback);        
        tMotionControlCallback.setInitialDuration(1.0f);
        tMotionControlCallback.setSpeed(1.0f);          
        tPathCallback.addListener(new MotionPathListener() {
        public void onWayPointReach(MotionEvent control, int wayPointIndex) {
                if (tPathCallback.getNbWayPoints() == wayPointIndex + 1) {
                    playerInput.bExecuting_motion_path = false;    
                    tMotionControlCallback.stop();
                    playerInput.setPoxX((int)(scene.game.getStageData().getAvailableTracksAtMeasure(scene.getMeasure())-1));
                }
            }
         });
        
        scene.getRootNode().attachChild(geometry);   
        scene.getRootNode().attachChild(scene.getCameraNode());                  
        scene.getBulletAppState().getPhysicsSpace().add(player);        
        player.setKinematic(true);        
        playerInput = new PlayerControlKeysAndTouch(this);
    }   
    
    public void finalizePlayer() { playerInput.clearMappings(); }
        
    public void die()
    {        
        scene.getGame().getStateManager().getState(GameState.class).setGameText("Game Over", 4.0f, true);                                     
        geometry.setMaterial(mat_hit);
        stStatus = "DEAD";
        playerInput.bExecuting_motion_path = true;
        tPathDie.clearWayPoints();           
        
        Vector3f t1 = new Vector3f(geometry.getLocalTranslation());
        tPathDie.addWayPoint(t1);      
                
        t1.setX((scene.game.getStageData().getAvailableTracksAtMeasure(scene.getMeasure())-1) * 2.5f);        
        t1.setZ(2000);
        tPathDie.addWayPoint(t1);         
        
        tMotionControlDie.play();    
    }
    
    public void movePlayerToPosX(float pos)
    {
        if(stStatus.contains("DEAD")) return;
        
        hitplayer();
        
        playerInput.bExecuting_motion_path = true;
        tPathCallback.clearWayPoints();           
        
        Vector3f t1 = new Vector3f(geometry.getLocalTranslation());
        tPathCallback.addWayPoint(t1);      
                
        t1.setX(pos);
        tPathCallback.addWayPoint(t1);
        
        tMotionControlCallback.play();    
    }
    
    public void update(float tpf) {
        
        if(bDoHitSecurityTime)
        {
            scene.setAudioVolume(FastMath.interpolateLinear(hittimecounter/hittime,0.0f,1.0f));
             
            if(hittimecounter < hittime)
            {
                hittimecounter += tpf;                               
            }
            else
            {
                hittimecounter = 0.0f;
                bDoHitSecurityTime = false;
                stStatus = "PLAY";
                if(bControlSwapped) {
                    geometry.setMaterial(mat_revert);            
                } else {
                    geometry.setMaterial(mat_normal);
                }                
            }
        }                
                  
        if(iLives >= 0)
        {
            geometry.setLocalTranslation(playerInput.updatePlayer(tpf));
        }     
        
        if(bIsBoxSoRotate)
        {
            geometry.rotate(tpf*rotVelocity, 0.0f, 0.0f);
        }
    }    
    
    public void addPoints(int points) { iPoints += points; }        
    
    public void addEnergy(int Energy)
    {
        if(scene.getGame().getDebugEnabled()) return;
         
        if(stStatus.equals("PLAY"))
        {
            if(iEnergy + Energy > MAX_ENERGY)
            {
                addLives(1);
                iEnergy = 1;
                return;
            }
            iEnergy += Energy;
        }
        
         if(iEnergy < 1) {
            addLives(-1);
            if(stStatus.equals("DEAD")) return;            
            iEnergy = MAX_ENERGY;               
        } 
    }        
    
    public void addLives(int lives)
    {
        if(stStatus.equals("PLAY"))
        {
            iLives += lives;
        }
               
        
        if(iLives < 0) {
            stStatus = "DEAD";
            iLives = 0;
            iEnergy = 0;
            die();
        }
    }
    
    public void hitplayer()
    {
        if(stStatus.equals("PLAY"))
        {
            stStatus = "HIT";
            geometry.setMaterial(mat_hit);
            bDoHitSecurityTime = true;
        }
    }
    
    public Boolean getControlSwapped() { return bControlSwapped; }
    
    public void setControlSwapped(Boolean bSwap) { 
        bControlSwapped = bSwap;
        if(bSwap) {
            geometry.setMaterial(mat_revert);            
        } else {
            geometry.setMaterial(mat_normal);
        }
    }
        
    public int getLives() { return iLives; }            
     
    public int getEnergy() { return iEnergy; }
    
    public int getPoints() { return iPoints; }
    
    public String getStatus() { return stStatus; }
    
    public int getPosX() { return playerInput.getPosX();}    
    
    public int getPosZ() { return playerInput.getPosZ();}
    
    public String getStringPosition(){ return playerInput.getPlayerPosition(); }
            
    public String getPlayerInfoString() { return " Score  " + iPoints + "\n Health  " + iLives + "  :  " + iEnergy + (bControlSwapped ? "\n CONTROL SWAP" : ""); }
    
    public SceneAbstract getScene() { return scene; }
    
    public Geometry getPlayerGeometry(){ return geometry; }
}
       
