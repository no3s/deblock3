/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.Midi;

/**
 *
 * @author no3z
 */

import deBlock3.StageData;
import deBlock3.MidiMessageProcessorSingleton;
import deBlock3.SceneAbstract;
import javax.sound.midi.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
        
public class MidiInputHandler
{
    private static final Logger logger = Logger.getLogger(MidiInputHandler.class.getName());
    
    private static MidiInputHandler instance = null;
    private SceneAbstract scene = null;	        
    private StageData mStageData;
    private MidiMessageProcessorSingleton midiObjectCreator = MidiMessageProcessorSingleton.getInstance();
    private Transmitter trans = null;
  
    private boolean paused = false;
    
    public void start() { paused = false; }
    public void pause() { paused = true; }
    public boolean isPaused() { return paused; }
    private int seconds = 0;
    public String mtcTime;
    public String lastnote = "NONE";
    private int measure = 0;
    
    protected MidiInputHandler() {
      // Exists only to defeat instantiation.
    }
      
    public static MidiInputHandler getInstance() {
      if(instance == null) {
         instance = new MidiInputHandler();
         instance.initialize();
      }
      return instance;
   }
    
    
    private void initialize()
    {
        this.mStageData = midiObjectCreator.getStageData();		
        this.scene = midiObjectCreator.getScene();                     
            

        MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
        for (int i = 0; i < infos.length; i++) 
        {
            try 
            {                 
                MidiDevice device = MidiSystem.getMidiDevice(infos[i]);
                logger.log(Level.INFO, "{0}",new Object[]{infos[i]});

                List<Transmitter> transmitters = device.getTransmitters();

                for(int j = 0; j<transmitters.size();j++) {
                    transmitters.get(j).setReceiver(
                            new MidiInputReceiver(device.getDeviceInfo().toString(), mStageData, scene, midiObjectCreator)
                    );
                }

                trans = device.getTransmitter();
                trans.setReceiver(new MidiInputReceiver(device.getDeviceInfo().toString(), mStageData, scene, midiObjectCreator));
                device.open();
                logger.log(Level.INFO, "{0} opened succesfully!",new Object[]{device.getDeviceInfo()});
            } catch (MidiUnavailableException e) {}
        }
    }
        
    public void close() 
    {
        MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
        for (int i = 0; i < infos.length; i++) 
        {
            try 
            {
                MidiDevice device = MidiSystem.getMidiDevice(infos[i]);            
                List<Transmitter> transmitters = device.getTransmitters();               
                for(int j = 0; j<transmitters.size();j++) {            
                    transmitters.get(j).getReceiver().close();
                    transmitters.get(j).setReceiver(null);
                    transmitters.get(j).close();
                }                
                trans.close();                
                device.close();      
                logger.log(Level.FINE, "Closing {0} !",new Object[]{ infos[i]});
            } catch (MidiUnavailableException e) {}
        }        
        
    }
    
    public int getMeasure() {
        return measure;
    }
    
    protected void setClockMeasure(int m) {
        measure = m ;
    }
    
    protected void setSeconds(int m){
        seconds = m;
        measure = (int)((float)seconds / (60.0f / ((float)mStageData.getBPM()/ 4.0f)));
    }
    
    public MidiMessageProcessorSingleton getCreator() { return this.midiObjectCreator; }
        
    
//tried to write my own class. I thought the send method handles an MidiEvents sent to it
public class MidiInputReceiver implements Receiver {
    
    public class MTC
    {
          
               public int hours;
                public int minutes;
                public int seconds;
                public int frames;
                public int fractionalFrames;
                public int type;
                public int tick;
                public int full;
                public void print() {
                   midiHandler.mtcTime = "H: " + hours + 
                            " M: " + minutes + 
                            " S: " + seconds + 
                            " F: " + frames + 
                            " FF: " + fractionalFrames +
                            " TICK: " + tick + " type: " + type +" full: " + full;
                }
                        
                public MTC() {
                        clear();
                }
                
                public void clear() {
                        hours = minutes = seconds = frames = fractionalFrames = 0;
                }
    }
    
    public MTC mtc = new MTC();
    public static final int NOTE_ON = 0x90;
    public static final int NOTE_OFF = 0x80;
    public final String[] NOTE_NAMES = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
    	
    private  final String[]		SYSTEM_MESSAGE_TEXT =
	{
		"System Exclusive (should not be in ShortMessage!)",
		"MTC Quarter Frame: ",
		"Song Position: ",
		"Song Select: ",
		"Undefined",
		"Undefined",
		"Tune Request",
		"End of SysEx (should not be in ShortMessage!)",
		"Timing clock",
		"Undefined",
		"Start",
		"Continue",
		"Stop",
		"Undefined",
		"Active Sensing",
		"System Reset"
	};

	private  final String[]		QUARTER_FRAME_MESSAGE_TEXT =
	{
		"frame count LS: ",
		"frame count MS: ",
		"seconds count LS: ",
		"seconds count MS: ",
		"minutes count LS: ",
		"minutes count MS: ",
		"hours count LS: ",
		"hours count MS: "
	};

	private  final String[]		FRAME_TYPE_TEXT =
	{
		"24 frames/second",
		"25 frames/second",
		"30 frames/second (drop)",
		"30 frames/second (non-drop)",
	};

        private int get14bitValue(int nLowerPart, int nHigherPart)
	{
		return (nLowerPart & 0x7F) | ((nHigherPart & 0x7F) << 7);
	}
                
    private MidiMessageProcessorSingleton midiCreator;
    private MidiInputHandler midiHandler;
    
    public String name;
    public MidiInputReceiver(String name, StageData mStageData, SceneAbstract scene, MidiMessageProcessorSingleton creator) {
        this.name = name;
        this.midiCreator = creator;        
        this.midiHandler = MidiInputHandler.getInstance();
    }
        
    public void send(MidiMessage msg, long timeStamp) {   
        
        if(midiHandler.isPaused()) return;
        
        MidiMessage message = msg;
        
                if (message instanceof ShortMessage) {                    
                    ShortMessage sm = (ShortMessage) message;
                    //System.out.println(sm.getCommand());
                    if (sm.getCommand() == NOTE_ON) {
                        int key = sm.getData1() + 12;
                        int octave = (key / 12)-1;
                        int note = key % 12;
                        String noteName = NOTE_NAMES[note];
                        int velocity = sm.getData2();
                        midiHandler.lastnote = "Ch: " + sm.getChannel() + " " + noteName + octave + " k=" + key + " v: " + velocity;
                        //System.out.println("Channel: " + sm.getChannel() + " Note on, " + noteName + octave + " key=" + key + " velocity: " + velocity);
                        if(velocity != 0) {
                            midiCreator.create(timeStamp, sm.getChannel(), key, velocity);
                        }
                    }
                    else if (sm.getCommand() == 0xF0) {
                        String strMessage = ""; //SYSTEM_MESSAGE_TEXT[sm.getChannel()];
                        switch (sm.getChannel())
                        {
                        case 0x1:
                                strMessage += SYSTEM_MESSAGE_TEXT[sm.getChannel()];
                                int	nQType = (sm.getData1() & 0x70) >> 4;
                                int	nQData = sm.getData1() & 0x0F;
                                if (nQType == 7)
                                {
                                        nQData = nQData & 0x1;
                                }
                                strMessage += QUARTER_FRAME_MESSAGE_TEXT[nQType] + nQData;
                                switch(nQType)                                    
                                {
                                    
//#define SE(ARG) tc.tick=ARG; full_tc|=1<<(ARG);
//#define SL(ARG) ARG = ( ARG &(~0xf)) | (data&0xf);
//#define SH(ARG) ARG = ( ARG &(~0xf0)) | ((data&0xf)<<4);
                                        
                                    case 0:  mtc.tick = 1; mtc.full|=1<<(1); mtc.frames = (mtc.frames &(~0xf)) | (nQData&0xf); break;
                                    case 1:  mtc.tick = 2; mtc.full|=1<<(2); mtc.frames = ( mtc.frames &(~0xf0)) | ((nQData&0xf)<<4);; break;
                                    case 2:  mtc.tick = 3; mtc.full|=1<<(3); mtc.seconds = (mtc.seconds &(~0xf)) | (nQData&0xf); ; break;
                                    case 3:  mtc.tick = 4; mtc.full|=1<<(4); mtc.seconds = ( mtc.seconds &(~0xf0)) | ((nQData&0xf)<<4);; break;
                                    case 4:  mtc.tick = 5; mtc.full|=1<<(5); mtc.minutes = (mtc.minutes &(~0xf)) | (nQData&0xf); ; break;
                                    case 5:  mtc.tick = 6; mtc.full|=1<<(6); mtc.minutes = ( mtc.minutes &(~0xf0)) | ((nQData&0xf)<<4);; break;
                                    case 6:  mtc.tick = 7; mtc.full|=1<<(7); mtc.hours = (mtc.hours &(~0xf)) | (nQData&0xf); ; break;
                                    case 7: {
                                        mtc.tick = 0; mtc.full|=1<<(0);
                                        mtc.hours = (mtc.hours&(~0xf0)) | ((nQData&1)<<4);  
                                        mtc.type = (nQData>>1)&3; 
                                        
                                        if (mtc.full!=0xff) { logger.log(Level.WARNING, "Somehow mtc not full!"); break; } 
                                        midiHandler.setSeconds((mtc.minutes * 60) + mtc.seconds);
                                        mtc.print();
                                        mtc.full = 0; 
                                        
                                    }
                                    default: break;
                                }
                                                                
                                if (nQType == 7)
                                {
                                        int	nFrameType = (sm.getData1() & 0x06) >> 1;
                                        strMessage += ", frame type: " + FRAME_TYPE_TEXT[nFrameType];
                                }
                                break;

                        case 0x2:
//                                strMessage += "song pos ";
//                                strMessage += get14bitValue(sm.getData1(), sm.getData2());
//                                strMessage += " " + sm.getData1() + " " + sm.getData2();
                                
                                int meas = get14bitValue(sm.getData1(), sm.getData2());
                                midiHandler.setClockMeasure(meas);
                                break;

                        case 0x3:
                                strMessage += sm.getData1();
                                break;
                        }
//                      if(!strMessage.isEmpty()) { System.out.println(strMessage); }
                    }
                } else {
                    //System.out.println("Other message: " + message.getClass());
                }

    }
    public void close() {}
    }
}