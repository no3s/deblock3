/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.Midi;

/**
 *
 * @author no3z
 */

import deBlock3.AppStates.GameState;
import com.jme3.audio.AudioNode;
import com.leff.midi.MidiFile;
import com.leff.midi.MidiTrack;
import com.leff.midi.event.MidiEvent;
import com.leff.midi.event.NoteOff;
import com.leff.midi.event.NoteOn;
import com.leff.midi.event.meta.Tempo;
import com.leff.midi.util.MetronomeTick;
import com.leff.midi.util.MidiEventListener;
import com.leff.midi.util.MidiProcessor;
import com.leff.midi.util.MidiUtil;
import deBlock3.StageData;
import deBlock3.MidiMessageProcessorSingleton;
import deBlock3.SceneAbstract;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MidiPlayerHandler implements MidiEventListener {

        private static final Logger logger = Logger.getLogger(MidiPlayerHandler.class.getName());
    
        private SceneAbstract scene = null;
        private MidiProcessor processor = null;
        private AudioNode audio = null;
        private MidiMessageProcessorSingleton midiObjectCreator = MidiMessageProcessorSingleton.getInstance();                
        private MidiFile midi = null;	        
        private StageData mStageData;
        private Boolean hasFinished = false;        
        
        public Boolean getStageClear() { return hasFinished; }
        
	public MidiPlayerHandler() {
                this.mStageData = midiObjectCreator.getStageData();		
                this.scene = midiObjectCreator.getScene();     
	}
	
	@Override
	public void onStart(boolean fromBeginning) {		
		if(fromBeginning) { logger.log(Level.FINE,"{0} Started!", new Object[]{mStageData.getLabel()}); }
		else  { logger.log(Level.FINE,"{0} Resumed!", new Object[]{mStageData.getLabel()}); }
	}

	@Override
	public void onEvent(MidiEvent event, long ms) {	 midiObjectCreator.create(event); }

	@Override
	public void onStop(boolean finished) {
            hasFinished = finished;
            if(hasFinished && scene.getPlayer().getStatus().equals("PLAY")) {
                    logger.log(Level.FINE,"{0} Finished!", new Object[]{mStageData.getLabel()});   
                    mStageData.setStageCleared(true);
                    scene.getGame().saveHighScore();
                    scene.getGame().enqueue(executeFinishText);

            }
            else  { logger.log(Level.FINE,"{0} Paused!", new Object[]{mStageData.getLabel()}); }                             
	}
	
                
        public final Callable<Void> executeFinishText = new Callable<Void>() {
               @Override
                public Void call() throws Exception {
                      scene.getGame().getStateManager().getState(GameState.class).setGameText("Stage clear!", 6.0f, true);  
                      return null;
                }
        };
    
        public void stop() {
            if(processor != null){                
                processor.stop(); 
                processor.reset();
                processor = null;
            }
            if(audio != null)
            {
                audio.stop();
                audio.removeFromParent();
                audio = null;
            }      
             { logger.log(Level.FINE,"{0} Audio Processor stopped!", new Object[]{mStageData.getLabel()}); }
        }                        
        
        public void pause(boolean pause)
        {
            if(pause)
            {
                if(audio != null) { audio.pause(); }
                if(processor != null) { processor.stop(); }            
            }
            else
            {
                if(audio != null) { audio.play();}
                if(processor != null) { processor.start();}            
            }
        }
        
	public void initialize() {
            try {                        
                this.midi = new MidiFile(mStageData.getMidiFile());                                            
                this.audio = new AudioNode(scene.getGame().getAssetManager(), "Sounds/"+mStageData.getAudioFile(), true);
                this.midiObjectCreator  = MidiMessageProcessorSingleton.getInstance();
                audio.setPositional(false);                
                mStageData.setNumTracks(midi.getTrackCount());
                logger.log(Level.FINE,"{0} Succesfully openned midi file! Tracks {1}", new Object[]{mStageData.getLabel(),midi.getTrackCount()});                 
            } catch(IOException e) {
                    System.err.println(e);
                    return;
            }

            // 2. Create a MidiProcessor
            processor = new MidiProcessor(midi);

            // 3. Register listeners for the events you're interested in
            MidiPlayerHandler ep = new MidiPlayerHandler();
            processor.registerEventListener(ep, Tempo.class);
            processor.registerEventListener(ep, NoteOn.class);
            processor.registerEventListener(ep, NoteOff.class);
	}
        
        public void initializeMinMaxNotes() {
            for(int i=0; i<=midi.getTrackCount()-1; ++i)
            {
                MidiTrack track = midi.getTracks().get(i);
                if(track == null) continue;
                int channel = track.calculateMinMaxNotes(i);
                if(channel != -1) { mStageData.setMinMaxNoteOnTrack(channel, track.getMinMaxNoteValue()); }
            }
        }
        
        public void play() { processor.start(); audio.play(); }
        
        public void setAudioVolume(float volume) { if(audio != null) { audio.setVolume(volume); } }
        
        public MetronomeTick getMetronome() { return processor.getMetronome(); }
        
        public int getNumTracks() { if(midi != null){ return midi.getTrackCount(); } return 0; }        
        
        public long getMidiFileDurationInMs() { return MidiUtil.ticksToMs(midi.getLengthInTicks(),Tempo.DEFAULT_MPQN,midi.getResolution()); }
        
        public long getFrequencyDurationInMs() { return MidiUtil.ticksToMs(processor.getMetronome().getFrequency(),Tempo.DEFAULT_MPQN,midi.getResolution()); }        
        
        public float getBPM() { return 60000000.0f / processor.getMPQN(); }                                        
}