/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.AppStates;

/**
 *
 * @author no3z
 */
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.RenderState;
import com.jme3.math.FastMath;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial.CullHint;
import deBlock3.Main;
import deBlock3.Player;
import deBlock3.SceneAbstract;
import deBlock3.TestScene;

public class GameState extends AbstractAppState {

    protected Node guiNode = new Node("Gui Node");

    protected BitmapText statsText;
    protected BitmapText playerText;
    protected BitmapText gameText;
    
    protected BitmapFont guiFont;    
    protected BitmapFont gameFont;  
   
    private Main game = null;

    protected SceneAbstract sceneTest = null;
           
    protected Player player = null;
    
    private AppActionListener actionListener = new AppActionListener();
   
    private float iGameTextTime = 0;
    private float iGameTextCounter = 0;
    private boolean endLevel = false;
    
    private static float gameTextSize = 48.0f;
        
    
    public GameState(Main game) {
       this.game = game;       
        // enable depth test and back-face culling for performance

        guiNode.setQueueBucket(Bucket.Gui);
        guiNode.setCullHint(CullHint.Never);
        loadText();     
    }
   
    
    private class AppActionListener implements ActionListener {
        public void onAction(String name, boolean value, float tpf) {
            if (!value) {
                return;
            }

            if (name.equals("GOTO_MENU")){
                game.loadMenu();
            }
        }
    }
   
    private void loadText(){
        float sizemul = (float)game.getContext().getSettings().getWidth() / 540.0f;
        guiFont = game.getAssetManager().loadFont("Interface/Fonts/Default.fnt");
        gameFont = game.getAssetManager().loadFont("/Interface/Fonts/beats.fnt");
                
        statsText = new BitmapText(guiFont, false);
        statsText.setSize(16.0f);
        statsText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),16));
        statsText.setLocalTranslation(12, game.getContext().getSettings().getHeight()/10.0f, 0);       
        statsText.setAlignment(BitmapFont.Align.Left);                          
        guiNode.attachChild(statsText);
        statsText.setText(" ");
        
        playerText = new BitmapText(gameFont, false);
        playerText.setSize(18.0f*sizemul);
        playerText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),18*3));
        if(game.getTargetPlatform() == Main.eTargetPlatform.OUYA)  {
            playerText.setLocalTranslation(90, game.getContext().getSettings().getHeight()-64, 0);     
        } else {
            playerText.setLocalTranslation(8, game.getContext().getSettings().getHeight()-8, 0);     
        }
        playerText.setAlignment(BitmapFont.Align.Left);   
        guiNode.attachChild(playerText);
        playerText.setText(" ");
        
        gameText = new BitmapText(gameFont, false);
        gameText.setSize(gameTextSize*sizemul);
        gameText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),64));
        gameText.setLocalTranslation(0,game.getContext().getSettings().getHeight()/1.4f, 0);     
        gameText.setAlignment(BitmapFont.Align.Center);                
        gameText.setText(" ");
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);                          
        app.getRenderer().applyRenderState(RenderState.DEFAULT);                   
    }
    
    public String getStats() { return "Tracks: " + sceneTest.getNumTracks() + " " + sceneTest.getMetronomeString(); }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        
        sceneTest.update(tpf); 
        
        sceneTest.getRootNode().updateLogicalState(tpf);
        sceneTest.getRootNode().updateGeometricState();        
//        
        if(game.getDebugEnabled())
        {
            statsText.setText("FPS: " + String.format("%04d", (int)game.getTimer().getFrameRate()) + " T: " + sceneTest.getNumTracks()+ " " + 
                            " Pos: " + sceneTest.player.getStringPosition() + " BPM: " + String.format("%.2f",sceneTest.getBPM()) + " " + sceneTest.getMetronomeString());                          
        }
        
        playerText.setText(sceneTest.getPlayer().getPlayerInfoString());
                
        checkGameText(tpf);
                
        
        guiNode.updateLogicalState(tpf);                     
        guiNode.updateGeometricState();  
    }
   
    @Override
    public void stateAttached(AppStateManager stateManager) {
              
        this.sceneTest = new TestScene(game, "level1");
        this.player = new Player(sceneTest); 
        
        sceneTest.initialize(player);
        player.initialize();
        
        game.getInputManager().addMapping("GOTO_MENU", new KeyTrigger(KeyInput.KEY_ESCAPE));                                
        game.getInputManager().addListener(actionListener, "GOTO_MENU");
       
        game.getViewPort().attachScene(sceneTest.getRootNode());
        game.getGUIViewPort().attachScene(guiNode);
    }

    @Override
    public void stateDetached(AppStateManager stateManager) { 
       game.getInputManager().deleteMapping("GOTO_MENU");        
       game.getInputManager().removeListener(actionListener);
       game.getInputManager().removeListener(player.playerInput.AsActionListener());    
       game.getViewPort().detachScene(sceneTest.getRootNode());
       game.getGUIViewPort().detachScene(guiNode);             
       sceneTest.stop();   
       
       sceneTest = null;
       player = null;
       
       System.gc();
    }

    @Override
    public void setEnabled(boolean enabled) { 
        super.setEnabled(enabled);
        if(enabled){
            sceneTest.pauseAudio(false);
        }
        else
        {
            sceneTest.pauseAudio(true);
        }
        
    }
    
    public void setGameText(String text, float time, boolean stopLevel)
    {
        if(guiNode.hasChild(gameText)) 
        {
            guiNode.detachChild(gameText);              
        }
                
        float sizemul = (float)game.getContext().getSettings().getWidth() / 640.0f;
        
        gameText.setText(text);
        gameText.setSize(gameTextSize * sizemul);
        iGameTextTime = time;
        guiNode.attachChild(gameText);
        iGameTextCounter = 0.0f;
        endLevel = stopLevel;        
    }

    public void setGameText(String text, float time, float size)
    {
        if(endLevel) return;
        
        if(guiNode.hasChild(gameText)) 
        {
            guiNode.detachChild(gameText);              
        }
        
        float sizemul = (float)game.getContext().getSettings().getWidth() / 640.0f;
        
        gameText.setText(text);
        gameText.setSize(size * sizemul);
        iGameTextTime = time;
        guiNode.attachChild(gameText);
        iGameTextCounter = 0.0f;
        endLevel = false;        
    }
        
    private void checkGameText(float tpf)
    {
        if(iGameTextCounter > iGameTextTime)
        {
            if(guiNode.hasChild(gameText)) 
            {
                guiNode.detachChild(gameText);  
            }
            if(endLevel){
                iGameTextTime = 0.0f;
                iGameTextCounter = 0.0f;
                gameText.setText("");
                sceneTest.finished();                
            }            
        }
        
        if(!gameText.getText().isEmpty())
        {
            iGameTextCounter += tpf;
            if(endLevel)
            {
                sceneTest.setAudioVolume(FastMath.interpolateLinear(iGameTextCounter/iGameTextTime,1.0f,0.0f));             
            }
        }
    }
    
    @Override
    public void render(RenderManager rm) {
        
    }
    
    public SceneAbstract getScene(){
        return sceneTest;
    }
            
}