/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.AppStates;

/**
 *
 * @author no3z
 */
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.TouchInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.TouchListener;
import com.jme3.input.controls.TouchTrigger;
import com.jme3.input.event.TouchEvent;
import static com.jme3.input.event.TouchEvent.Type.DOWN;
import static com.jme3.input.event.TouchEvent.Type.KEY_UP;
import com.jme3.material.RenderState;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial.CullHint;
import deBlock3.Main;

public class StartState extends AbstractAppState {

    private Main game = null;
        
    private Node rootNode = new Node("Root Node");
    private Node guiNode = new Node("Gui Node");
    private BitmapText titleText;
    private BitmapText menuText;
    private BitmapFont menuFont;
      
    private DrawFloorForStaticState floorDrawer = null;
   
    private StartState.AppActionListener actionListener = new StartState.AppActionListener();
       
    public StartState(Main game) {
       this.game = game;
        
       guiNode.setQueueBucket(Bucket.Gui);
       guiNode.setCullHint(CullHint.Never);  
       rootNode.setCullHint(CullHint.Dynamic);
                
       menuFont = game.getAssetManager().loadFont("/Interface/Fonts/beats.fnt");
       
       float sizemul = (float)game.getContext().getSettings().getWidth() / 640.0f;
       
       titleText = new BitmapText(menuFont, false);
       titleText.setSize(56.0f * sizemul);      
       titleText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),48));                     
       titleText.setLocalTranslation(0, game.getContext().getSettings().getHeight()/1.6f, 0);  
       titleText.setText("deBlock3");       
       titleText.setAlignment(BitmapFont.Align.Center);
                     
       menuText = new BitmapText(menuFont, false);
       menuText.setSize(menuFont.getCharSet().getRenderedSize() * sizemul);
       menuText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),48));
       menuText.setLocalTranslation(0, game.getContext().getSettings().getHeight()/1.2f, 0);       
       menuText.setAlignment(BitmapFont.Align.Center);
       menuText.setText("no3z productions\npreviews\n");
       
       guiNode.attachChild(titleText);
       guiNode.attachChild(menuText);
       
       GuiNodeButtonDrawer.getInstance().drawOKButton(game, guiNode);   
       
       floorDrawer = new DrawFloorForStaticState(game, rootNode);
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);       
        app.getRenderer().applyRenderState(RenderState.DEFAULT);       
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);                      

        floorDrawer.update(tpf);
        
        rootNode.updateLogicalState(tpf);
        rootNode.updateGeometricState();        
        
        // simple update and root node
        guiNode.updateLogicalState(tpf);
        guiNode.updateGeometricState();
    }
   
    @Override
    public void stateAttached(AppStateManager stateManager) 
    {
        if (game.getInputManager() != null ){
            game.getInputManager().addMapping("EXIT_GAME", new KeyTrigger(KeyInput.KEY_ESCAPE));
            game.getInputManager().addMapping("GOTO_MENU", new KeyTrigger(KeyInput.KEY_RETURN));
            game.getInputManager().addMapping("Touch", new TouchTrigger(0));
            game.getInputManager().addMapping("TouchEscape", new TouchTrigger(TouchInput.KEYCODE_BACK));
            
        }
        
        game.getInputManager().addListener(actionListener, "EXIT_GAME");
        game.getInputManager().addListener(actionListener, "GOTO_MENU");
        game.getInputManager().addListener(touchListener, new String[]{"Touch"});
        game.getInputManager().addListener(touchListener, new String[]{"TouchEscape"});
                            
        floorDrawer.attach();
        
        game.getViewPort().attachScene(rootNode);
        game.getGuiViewPort().attachScene(guiNode);                  
    }

    @Override
    public void stateDetached(AppStateManager stateManager) 
    {          
        floorDrawer.detach();
        
        game.getViewPort().detachScene(rootNode);
        game.getGuiViewPort().detachScene(guiNode);
        
        game.getInputManager().deleteMapping("GOTO_MENU");
        game.getInputManager().deleteMapping("EXIT_GAME");
        game.getInputManager().deleteMapping("Touch");
        game.getInputManager().deleteMapping("TouchEscape");
        game.getInputManager().removeListener(actionListener);
        game.getInputManager().removeListener(touchListener);
    }

    private class AppActionListener implements ActionListener {
        public void onAction(String name, boolean value, float tpf) {
            if (!value) {
                return;
            }            
            if (name.equals("EXIT_GAME")){
                game.playErrorSound();
                game.destroyGame();
            }
            else if (name.equals("GOTO_MENU")){                
                game.playOKSound();
                game.loadMenu();                
            }
        } 
    }
    
    private TouchListener touchListener = new TouchListener() {
       public void onTouch(String name, TouchEvent evt, float tpf) {
       float x;
       float y;
       switch(evt.getType())
        {            
            case DOWN:
                x = evt.getX();
                y = evt.getY();            
                
                if (x>0&&x<game.getContext().getSettings().getWidth()&&                   
                    y>0&&y< game.getContext().getSettings().getHeight())
                { 
                    game.playOKSound();
                    game.loadMenu();          
                }
            break;
            case KEY_UP:
                if (name.equals("TouchEscape")) 
                {
                     game.playErrorSound();
                     game.destroyGame();
                }
            break;
            default:
            break;
        }
       }
   };

}