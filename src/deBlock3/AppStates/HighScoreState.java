/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.AppStates;

/**
 *
 * @author no3z
 */
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.TouchInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.TouchListener;
import com.jme3.input.controls.TouchTrigger;
import com.jme3.input.event.TouchEvent;
import static com.jme3.input.event.TouchEvent.Type.DOWN;
import static com.jme3.input.event.TouchEvent.Type.KEY_UP;
import com.jme3.material.RenderState;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial.CullHint;
import deBlock3.StageData;
import deBlock3.Main;

public class HighScoreState extends AbstractAppState {

    private Main game = null;
        
    private Node rootNode = new Node("Root Node");
    private Node guiNode = new Node("Gui Node");
    private BitmapText titleText;
    private BitmapText menuText;
    private BitmapText scoreText;
    private BitmapFont menuFont;   
       
    private DrawFloorForStaticState floorDrawer = null;
           
    private int currentLetterPosition = 0;
    private int currentLetter = 65;
    private StringBuilder currentString = new StringBuilder("A  ");
    private int score = 0;
    private int MAX_CHARS = 3;
    
    public void setHighScore(int iScore) { score = iScore;}    
    
    private HighScoreState.AppActionListener actionListener = new HighScoreState.AppActionListener();   
    
    public HighScoreState(Main game) {
       this.game = game;
        
       guiNode.setQueueBucket(Bucket.Gui);
       guiNode.setCullHint(CullHint.Never);  
       rootNode.setCullHint(CullHint.Dynamic);
                
       menuFont = game.getAssetManager().loadFont("/Interface/Fonts/beats.fnt");
       
       float sizemul = (float)game.getContext().getSettings().getWidth() / 640.0f;
        
       titleText = new BitmapText(menuFont, false);
       titleText.setSize(56.0f*sizemul);      
       titleText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),48));                     
       titleText.setLocalTranslation(0, game.getContext().getSettings().getHeight()/1.15f, 0);  
       titleText.setText("Congratulations!");       
       titleText.setAlignment(BitmapFont.Align.Center);
                             
       menuText = new BitmapText(menuFont, false);
       menuText.setSize(menuFont.getCharSet().getRenderedSize()*sizemul);
       menuText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),48));
       menuText.setLocalTranslation(0, game.getContext().getSettings().getHeight()/1.5f, 0);       
       menuText.setAlignment(BitmapFont.Align.Center);
       menuText.setText("High Score achieved!\nEnter your initials: ");
       
       scoreText = new BitmapText(menuFont, false);
       scoreText.setSize(menuFont.getCharSet().getRenderedSize()*sizemul);
       scoreText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),48));
       scoreText.setLocalTranslation(0, game.getContext().getSettings().getHeight()/2.2f, 0);       
       scoreText.setAlignment(BitmapFont.Align.Center);
       scoreText.setText(" ");
       scoreText.setText(currentString);
       
       GuiNodeButtonDrawer.getInstance().drawMovementOKButtons(game, guiNode);
       
                  
       floorDrawer = new DrawFloorForStaticState(game, rootNode);
        
       guiNode.attachChild(titleText);
       guiNode.attachChild(menuText);
       guiNode.attachChild(scoreText);
       
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        app.getRenderer().applyRenderState(RenderState.DEFAULT); 
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);                      
        
        floorDrawer.update(tpf);
        
        rootNode.updateLogicalState(tpf);
        rootNode.updateGeometricState();                       
        scoreText.setText(score + "______ " + currentString);                       
        guiNode.updateLogicalState(tpf);
        guiNode.updateGeometricState();
    }
   
    @Override
    public void stateAttached(AppStateManager stateManager) {
                        
        // Init input
        if (game.getInputManager() != null ){
            game.getInputManager().addMapping("NEXT", new KeyTrigger(KeyInput.KEY_RIGHT));
            game.getInputManager().addMapping("PREV", new KeyTrigger(KeyInput.KEY_LEFT));
            game.getInputManager().addMapping("OK", new KeyTrigger(KeyInput.KEY_RETURN));
            game.getInputManager().addMapping("DEL", new KeyTrigger(KeyInput.KEY_BACK));
            game.getInputManager().addMapping("DONE", new KeyTrigger(KeyInput.KEY_ESCAPE));
            game.getInputManager().addMapping("Touch", new TouchTrigger(0));
            game.getInputManager().addMapping("TouchEscape", new TouchTrigger(TouchInput.KEYCODE_BACK));
            
        }        
        game.getInputManager().addListener(actionListener, "NEXT");
        game.getInputManager().addListener(actionListener, "PREV");
        game.getInputManager().addListener(actionListener, "OK");
        game.getInputManager().addListener(actionListener, "DEL");
        game.getInputManager().addListener(actionListener, "DONE");
        game.getInputManager().addListener(touchListener, new String[]{"Touch"});
        game.getInputManager().addListener(touchListener, new String[]{"TouchEscape"});                            

        floorDrawer.attach();
        
        game.getViewPort().attachScene(rootNode);
        game.getGuiViewPort().attachScene(guiNode);                
    }

    @Override
    public void stateDetached(AppStateManager stateManager) {                              
        
        floorDrawer.detach();
        
        game.getViewPort().detachScene(rootNode);
        game.getGuiViewPort().detachScene(guiNode);        
        game.getInputManager().deleteMapping("NEXT");
        game.getInputManager().deleteMapping("PREV");
        game.getInputManager().deleteMapping("OK");
        game.getInputManager().deleteMapping("DEL");        
        game.getInputManager().deleteMapping("DONE");  
        game.getInputManager().deleteMapping("Touch");
        game.getInputManager().deleteMapping("TouchEscape");
        game.getInputManager().removeListener(actionListener);
        game.getInputManager().removeListener(touchListener);

    }

    private class AppActionListener implements ActionListener {
        public void onAction(String name, boolean value, float tpf) {
            if (!value) {
                return;
            }            
            if (name.equals("NEXT")){ game.playMoveSound(); nextLetter(); }
            else if (name.equals("PREV")){ game.playMoveSound(); prevLetter(); }
            else if (name.equals("OK")){ game.playOKSound(); OKLetter(); }
            else if (name.equals("DEL")){ game.playErrorSound(); delLetter(); }
            else if (name.equals("DONE")){ game.playOKSound(); OKInput(); }
        } 
    }
    
     private TouchListener touchListener = new TouchListener() {

       public void onTouch(String name, TouchEvent evt, float tpf) {
       float x;
       float y;
       switch(evt.getType())
        {            
            case DOWN:
                x = evt.getX();
                y = evt.getY();            
                
                if (x>0&&x<game.getContext().getSettings().getWidth()*0.5&&
                    y>0&&y< game.getContext().getSettings().getHeight()*0.5)
                {
                    game.playMoveSound();
                     prevLetter();
                }

                if (x>game.getContext().getSettings().getWidth()*0.5&&x<game.getContext().getSettings().getWidth()&&
                    y>0&&y< game.getContext().getSettings().getHeight()*0.5)
                { 
                    game.playMoveSound();
                    nextLetter();
                }
                if (x>0&&x<game.getContext().getSettings().getWidth()&&                   
                    y>game.getContext().getSettings().getHeight()*0.5&&y< game.getContext().getSettings().getHeight())
                { 
                    game.playOKSound();
                    OKLetter();
                }
            break;
                
            case KEY_UP:
                if (name.equals("TouchEscape")) 
                {
                    if(currentLetterPosition != 0){
                     game.playErrorSound();
                     delLetter();
                    }
                    else {
                        currentString.insert(0, "NO3");
                        game.playOKSound();
                        OKInput();
                    }
                        
                }
            break;
            default:
            break;
        }
       }
   };
    
    public void nextLetter() {        
        currentLetter += 1;
        
        if(currentLetter > 57 && currentLetter < 63) { currentLetter = 63; }
        //else if(currentLetter > 90 && currentLetter < 97) { currentLetter = 97; }
        else if(currentLetter > 90) { currentLetter = 48; }
        
        char c = (char)currentLetter;
        currentString.setCharAt(currentLetterPosition,c);
    }
    
    public void prevLetter() {
        currentLetter -= 1;
        if(currentLetter == 47) { currentLetter = 90; }
        else if(currentLetter == 62) { currentLetter = 57; }
        else if(currentLetter == 96 ) { currentLetter = 65; }

        
        char c = (char)currentLetter;
        currentString.setCharAt(currentLetterPosition,c);
    }
    
    public void OKLetter() {
        if(currentLetterPosition < MAX_CHARS -1) {
            currentLetterPosition += 1;
            char c = (char)currentLetter;
            currentString.setCharAt(currentLetterPosition,c);
        }
        else
        {
            OKInput();
        }
    }
    
    public void delLetter() {
        String t = " ";
        char c = t.charAt(0);
        currentString.setCharAt(currentLetterPosition,c);
        if(currentLetterPosition > 0) {
            currentLetterPosition -= 1;
            currentLetter = currentString.charAt(currentLetterPosition);
        }
    }

    public void OKInput() {
        java.util.Date date = new java.util.Date(); 
        java.text.SimpleDateFormat sdf=new java.text.SimpleDateFormat("dd-MM-yyyy");
        String fecha = sdf.format(date);
        game.getStageData().setHighScoreMap(StageData.MAX_HIGHSCORES-1, score, currentString.toString(), fecha);
        game.saveHighScore();
        game.loadMenu();
    }       
}