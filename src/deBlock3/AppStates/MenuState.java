/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.AppStates;

/**
 *
 * @author no3z
 */
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.TouchInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.TouchListener;
import com.jme3.input.controls.TouchTrigger;
import com.jme3.input.event.TouchEvent;
import static com.jme3.input.event.TouchEvent.Type.DOWN;
import static com.jme3.input.event.TouchEvent.Type.KEY_UP;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial.CullHint;
import deBlock3.Main;

public class MenuState extends AbstractAppState {

    private Main game = null;
    private Node rootNode = new Node("Root Node");
    private Node guiNode = new Node("Gui Node");
    private DrawFloorForStaticState floorDrawer = null;
    
    private BitmapText titleText;
    private BitmapText menuText;
    private BitmapText selectedText;
    private BitmapText instructionText;
    private BitmapText highScoreText;
    private BitmapFont menuFont;     
    
    private AppActionListener actionListener = new AppActionListener();    
    
    public MenuState(Main game) {
        this.game = game;

        guiNode.setQueueBucket(Bucket.Gui);
        guiNode.setCullHint(CullHint.Never);  
        rootNode.setCullHint(CullHint.Dynamic);
        menuFont = game.getAssetManager().loadFont("/Interface/Fonts/beats.fnt");

        float sizemul = (float)game.getContext().getSettings().getWidth() / 640.0f;

        titleText = new BitmapText(menuFont, false);
        titleText.setSize(48.0f * sizemul);
        selectedText = new BitmapText(menuFont, false);
        selectedText.setSize(48.0f * sizemul);
        menuText = new BitmapText(menuFont, false);
        menuText.setSize(menuFont.getCharSet().getRenderedSize() * sizemul);
        highScoreText = new BitmapText(menuFont, false);
        highScoreText.setSize(20.0f * sizemul);
        instructionText = new BitmapText(menuFont, false);
        instructionText.setSize(24.0f* sizemul);

        titleText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),48* sizemul));
        if(game.getTargetPlatform() == Main.eTargetPlatform.OUYA) {
         titleText.setLocalTranslation(95,titleText.getLineHeight()+ game.getContext().getSettings().getHeight()/16, 0);
        } else {           
          titleText.setLocalTranslation(16, (int)(game.getContext().getSettings().getHeight()), 0);
        }

        titleText.setText("deBlock3");       
        titleText.setAlignment(BitmapFont.Align.Left);

        menuText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),48* sizemul));
        menuText.setLocalTranslation(0, game.getContext().getSettings().getHeight()/1.2f, 0);       
        menuText.setAlignment(BitmapFont.Align.Center);

        selectedText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),48* sizemul));
        selectedText.setLocalTranslation(0, game.getContext().getSettings().getHeight()/1.5f, 0);       
        selectedText.setAlignment(BitmapFont.Align.Center);                   


        highScoreText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),32* sizemul));
        highScoreText.setLocalTranslation(0, game.getContext().getSettings().getHeight()/2.0f, 0);       
        highScoreText.setAlignment(BitmapFont.Align.Center);  
        highScoreText.setText(" ");
        highScoreText.setColor(new ColorRGBA(0.99f,0.9f,0.99f,0.95f));
        instructionText.setBox(new com.jme3.font.Rectangle(0,0,game.getContext().getSettings().getWidth(),24* sizemul));
        instructionText.setLocalTranslation(0, instructionText.getLineHeight() + game.getContext().getSettings().getHeight()/15, 0);       
        instructionText.setAlignment(BitmapFont.Align.Center);                   
        instructionText.setText("");

        GuiNodeButtonDrawer.getInstance().drawMovementOKButtons(game, guiNode);
           
        floorDrawer = new DrawFloorForStaticState(game, rootNode);
       
        //guiNode.attachChild(titleText);
        guiNode.attachChild(menuText);
        guiNode.attachChild(selectedText);
        guiNode.attachChild(highScoreText);
        guiNode.attachChild(instructionText);        
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        app.getRenderer().applyRenderState(RenderState.DEFAULT);        
    }

    private ColorRGBA greenColor = new ColorRGBA(0.1f,08f,0.1f,0.65f); 
    private ColorRGBA redColor = new ColorRGBA(0.8f,0.1f,0.1f,0.85f); 
                    
    @Override
    public void update(float tpf) {                
        super.update(tpf);                      
      
        floorDrawer.update(tpf);
      
        rootNode.updateLogicalState(tpf);
        rootNode.updateGeometricState();
        menuText.setColor(ColorRGBA.White);
        menuText.setText("Select Stage:");
        if(game.getIsPrevStageCleared()) {
            selectedText.setColor(ColorRGBA.LightGray);            
            instructionText.setColor(greenColor);
            instructionText.setText("Ready!");
        } else {
            selectedText.setColor(ColorRGBA.DarkGray);   
            instructionText.setColor(redColor);
            instructionText.setText("Blocked!");
        }
        
        selectedText.setText((game.getCurrentStage()+1) + " / " + game.setCurrentStage());
        if(game.getStageHighScore().isEmpty()){
            highScoreText.setText(" ");
            } else {
            
            highScoreText.setText(game.getStageHighScore());
        }                            
                
        guiNode.updateLogicalState(tpf);
        guiNode.updateGeometricState();
    }
   
   
    @Override
    public void stateAttached(AppStateManager stateManager) {
                        
        // Init input
        if (game.getInputManager() != null ){
            game.getInputManager().addMapping("EXIT_GAME", new KeyTrigger(KeyInput.KEY_ESCAPE));
            game.getInputManager().addMapping("GOTO_GAME", new KeyTrigger(KeyInput.KEY_RETURN));
            game.getInputManager().addMapping("MENU_LEVELDOWN", new KeyTrigger(KeyInput.KEY_A));
            game.getInputManager().addMapping("MENU_LEVELUP", new KeyTrigger(KeyInput.KEY_D));
            game.getInputManager().addMapping("MENU_LEVELDOWN", new KeyTrigger(KeyInput.KEY_LEFT));
            game.getInputManager().addMapping("MENU_LEVELUP", new KeyTrigger(KeyInput.KEY_RIGHT));
            game.getInputManager().addMapping("Touch", new TouchTrigger(0));
            game.getInputManager().addMapping("TouchEscape", new TouchTrigger(TouchInput.KEYCODE_BACK));
            
        }
        
        game.getInputManager().addListener(actionListener, "EXIT_GAME");
        game.getInputManager().addListener(actionListener, "GOTO_GAME");
        game.getInputManager().addListener(actionListener, "MENU_LEVELDOWN");
        game.getInputManager().addListener(actionListener, "MENU_LEVELUP");
        game.getInputManager().addListener(touchListener, new String[]{"Touch"});
        game.getInputManager().addListener(touchListener, new String[]{"TouchEscape"});
        
        floorDrawer.attach();
        
        game.getViewPort().attachScene(rootNode);
        game.getGuiViewPort().attachScene(guiNode);
    }

    @Override
    public void stateDetached(AppStateManager stateManager) {
        
        floorDrawer.detach();
                
        game.getViewPort().detachScene(rootNode);
        game.getGuiViewPort().detachScene(guiNode);
        
        game.getInputManager().deleteMapping("GOTO_GAME");
        game.getInputManager().deleteMapping("MENU_LEVELDOWN");
        game.getInputManager().deleteMapping("MENU_LEVELUP");
        game.getInputManager().deleteMapping("EXIT_GAME");
        game.getInputManager().deleteMapping("Touch");
        game.getInputManager().deleteMapping("TouchEscape");
        game.getInputManager().removeListener(actionListener);
        game.getInputManager().removeListener(touchListener);
               

    }

    private class AppActionListener implements ActionListener {
        public void onAction(String name, boolean value, float tpf) {
            if (!value) {
                return;
            }            
            if (name.equals("EXIT_GAME")){
                game.playErrorSound();
                game.loadStart();
            } else if (name.equals("MENU_LEVELDOWN")){                
                int cl = game.getCurrentStage();
                game.setStageData(game.getCurrentStage()-1);  
                if( game.getCurrentStage() == cl) { game.playErrorSound();} 
                else { game.playMoveSound(); }                
            }else if (name.equals("MENU_LEVELUP")){                
                int cl = game.getCurrentStage();
                game.setStageData(game.getCurrentStage()+1);                
                if( game.getCurrentStage() == cl) { game.playErrorSound();} 
                else { game.playMoveSound(); }                
            }else if (name.equals("GOTO_GAME")){    
                if(game.getIsPrevStageCleared() || game.getDebugEnabled()) {
                    game.playOKSound(); 
                    game.loadGame();             
                } else {
                    game.playErrorSound();
                }
            }
        } 
    }
          
    private TouchListener touchListener = new TouchListener() {

       public void onTouch(String name, TouchEvent evt, float tpf) {
       float x;
       float y;
       switch(evt.getType())
        {            
            case DOWN:
                x = evt.getX();
                y = evt.getY();            
                
                if (x>0&&x<game.getContext().getSettings().getWidth()*0.5&& y>0&&y< game.getContext().getSettings().getHeight()*0.5){
                       int cl = game.getCurrentStage();
                    game.setStageData(game.getCurrentStage()-1);   
                    if( game.getCurrentStage() == cl) { game.playErrorSound();} 
                    else { game.playMoveSound(); }                
                }
                if (x>game.getContext().getSettings().getWidth()*0.5&&x<game.getContext().getSettings().getWidth()&&y>0&&y< game.getContext().getSettings().getHeight()*0.5){ 
                    int cl = game.getCurrentStage();
                    game.setStageData(game.getCurrentStage()+1);   
                    if( game.getCurrentStage() == cl) { game.playErrorSound();} 
                    else { game.playMoveSound(); }                
                }
                if (x>0&&x<game.getContext().getSettings().getWidth()&&y>game.getContext().getSettings().getHeight()*0.5&&y< game.getContext().getSettings().getHeight()){ 
                    if(game.getIsPrevStageCleared()) {
                        game.playOKSound(); 
                        game.loadGame();             
                    } else {
                        game.playErrorSound();
                    }       
                }
            break;                
            case KEY_UP:
                if (name.equals("TouchEscape")) {
                    game.playErrorSound();
                    game.loadStart();
                }
            break;
                
            default:
            break;
        }
       }
   };

    
    public void render(RenderManager rm) {
    }
}