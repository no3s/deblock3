/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.AppStates;

import com.jme3.asset.TextureKey;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;
import deBlock3.StageData;
import deBlock3.Main;
import deBlock3.Midi.MidiPlayerHandler;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author no3z
 */
public class DrawFloorForStaticState {
        
    private static final Logger logger = Logger.getLogger(DrawFloorForStaticState.class.getName());
    
    private Main game;
    private Node rootNode;
    private CameraNode cameraNode;    
    private Geometry floor_geo;
    private float sinAccumulator = 0.0f;
    private float randomStartX =  (float)new Random().nextInt(50);
    private float randomStartZ =  (float)new Random().nextInt(50);
    private String RandomTexturesArray[] = 
    {
        "Textures/funkgas/envmap.png"        
        //,"Textures/starsenv.png"
        //,"Textures/nightskyenv.png"        
    };
   
    private String getRandomTexture() { return (RandomTexturesArray[new Random().nextInt(RandomTexturesArray.length)]); }
      
    public DrawFloorForStaticState(Main game, Node rootNode) 
    {
        this.game = game;
        this.rootNode = rootNode;
        cameraNode = new CameraNode("camera", game.getCamera());  
        cameraNode.lookAt(new Vector3f(0,0,100),Vector3f.UNIT_Y); 
        cameraNode.getCamera().setFrustumPerspective(20.0f,1.777f,0.01f,1000.0f);       
        Box floor = new Box(Vector3f.ZERO, 1000f, 0.1f, 1000f);      
        Material floor_mat = StageData.createModelsMaterials(game.getAssetManager(),game.getRandomTexture());          
        floor_geo = new Geometry("Floor", floor);   
        floor_geo.setMaterial(floor_mat);   
            
        TextureKey key = new TextureKey(getRandomTexture(), false);
        key.setGenerateMips(true);
        Texture tex = game.getAssetManager().loadTexture(key);       
        float t1 = 1.0f + (float)new Random().nextInt(5);
        float t2 = 1.0f + (float)new Random().nextInt(20);        
        rootNode.attachChild(SkyFactory.createSky(game.getAssetManager(), tex, new Vector3f(t1,t2,t2), true));
        
        sinAccumulator = (float)Math.PI * (float)new Random().nextInt(1);
    }

    public void update(float tpf)
    {                
        sinAccumulator += tpf*0.05;        
        if(sinAccumulator >= Math.PI * 2) { sinAccumulator -= Math.PI * 2; }          
        float circlex = randomStartX * (float)Math.cos(sinAccumulator);
        float circlez = randomStartZ * (float)Math.sin(sinAccumulator);                
        cameraNode.lookAt(new Vector3f(-circlez,0,-circlex+100),Vector3f.UNIT_Y);        
        cameraNode.setLocalTranslation(new Vector3f(circlex, 2.0f, circlez));        
    }
    
    public void attach()
    {
        rootNode.attachChild(floor_geo);
        rootNode.attachChild(cameraNode);    
    }
    
    public void detach()
    {
        rootNode.detachChild(floor_geo);
        rootNode.detachChild(cameraNode);
    }
}
