/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deBlock3.AppStates;

import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;
import deBlock3.Main;


/**
 *
 * @author no3z
 */
public class GuiNodeButtonDrawer {
            
    private static GuiNodeButtonDrawer instance = null;
        
           protected GuiNodeButtonDrawer() {
      // Exists only to defeat instantiation.
    }
      
    public static GuiNodeButtonDrawer getInstance() {
      if(instance == null) {
         instance = new GuiNodeButtonDrawer();
      }
      return instance;
   }          
    
    void drawOKButton(Main game, Node guiNode){
        if(game.getTargetPlatform() == Main.eTargetPlatform.OUYA)
        {
            Picture picOK = new Picture("OUYA_O");
            picOK.setImage(game.getAssetManager(), "Images/OUYA_O.png", true);
            picOK.setWidth(65);
            picOK.setHeight(80);
            int posx = (int)(game.getContext().getSettings().getWidth() - 160 );
            int posy = (int)(game.getContext().getSettings().getHeight()/15);
            picOK.setPosition( posx, posy);         
            guiNode.attachChild(picOK);
        }
        else if(game.getTargetPlatform() == Main.eTargetPlatform.ANDROID) 
        {
            Picture picOK = new Picture("OK");            
            picOK.setImage(game.getAssetManager(), "Images/OK.png", true);   
            int ar = 2;
            int sw = game.getContext().getSettings().getWidth() ;
            int sh = game.getContext().getSettings().getHeight();
            float fw = (float)sw/(float)ar;
            float fh = (float)sh/(float)ar;
            int pw = (int)((fw*128.0f)/(float)sw);
            int ph = (int)((fh*128.0f)/(float)sh);
            picOK.setWidth(pw);
            picOK.setHeight(ph);
            picOK.getMaterial().setColor("Color", new ColorRGBA(1f, 1f, 1f, 0.95f));
            int OKposX =(int)(game.getContext().getSettings().getWidth() - ((pw)+ (pw/2)) );
            int OKposY = (int)(game.getContext().getSettings().getHeight()- (pw*1.5));
            picOK.setPosition( OKposX, OKposY);         
            guiNode.attachChild(picOK);
        } 
    }
    
    void drawMovementOKButtons(Main game, Node guiNode){
            
        if(game.getTargetPlatform() != Main.eTargetPlatform.OUYA)
        {         
            Picture picRight = new Picture("RIGHT");
            picRight.setImage(game.getAssetManager(), "Images/right.png", true);            
            float ar = 1.1069f;
            int sw = game.getContext().getSettings().getWidth() ;
            int sh = game.getContext().getSettings().getHeight();
            float fw = (float)sw/ar;
            float fh = (float)sh/ar;
            int ph = (int)((fh*64.0f)/(float)sh);
            int pw = (int)((fw*64.0f)/(float)sw);
            picRight.setWidth(pw);
            picRight.setHeight(ph);
            picRight.getMaterial().setColor("Color", new ColorRGBA(1f, 1f, 1f, 0.75f));
            int posx = (int)(game.getContext().getSettings().getWidth() - ((pw)+ (pw/2)) );
            int posy = (int)(ph/2);
            picRight.setPosition( posx, posy);         
            guiNode.attachChild(picRight);
            
            Picture picLeft = new Picture("LEFT");
            picLeft.setImage(game.getAssetManager(), "Images/left.png", true);            
            picLeft.setWidth(pw);
            picLeft.setHeight(ph);
            picLeft.getMaterial().setColor("Color", new ColorRGBA(1f, 1f, 1f, 0.75f));
            posx = (int)((pw)- (pw/2));
            posy = (int)(ph/2);
            picLeft.setPosition( posx, posy);         
            guiNode.attachChild(picLeft);
        }
        
        if(game.getTargetPlatform() == Main.eTargetPlatform.OUYA)
        {
             Picture picOuyaControls = new Picture("OUYA_CONTROLS");
             picOuyaControls.setImage(game.getAssetManager(), "Images/OUYA_CONTROLS.png", true);
             picOuyaControls.setWidth(230);
             picOuyaControls.setHeight(182);
             int posx = (int)(game.getContext().getSettings().getWidth() - 320 );
             int posy = (int)(game.getContext().getSettings().getHeight()/15);
             picOuyaControls.setPosition( posx, posy);                     
             guiNode.attachChild(picOuyaControls);
        }                
        else if(game.getTargetPlatform() == Main.eTargetPlatform.ANDROID) 
        {
            Picture picOK = new Picture("OK");            
            picOK.setImage(game.getAssetManager(), "Images/OK.png", true);   
            float ar = 1.36f;
            int sw = game.getContext().getSettings().getWidth() ;
            int sh = game.getContext().getSettings().getHeight();
            float fw = (float)sw/ar;
            float fh = (float)sh/ar;
            int pw = (int)((fw*128.0f)/(float)sw);
            int ph = (int)((fh*128.0f)/(float)sh);
            picOK.setWidth(pw);
            picOK.setHeight(ph);
            picOK.getMaterial().setColor("Color", new ColorRGBA(1f, 1f, 1f, 0.95f));
            int OKposX =(int)(game.getContext().getSettings().getWidth() - ((pw)+ (pw/2)) );
            int OKposY = (int)(game.getContext().getSettings().getHeight()- (pw*1.5));
            picOK.setPosition( OKposX, OKposY);                
            guiNode.attachChild(picOK);
        }                
    }        
}
