package deBlock3;

import com.jme3.asset.AssetManager;
import com.jme3.asset.TextureKey;
import com.jme3.audio.AudioNode;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.material.RenderState.FaceCullMode;
//import com.jme3.material.plugins.NeoTextureMaterialKey;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;
import com.jme3.texture.Texture;
import deBlock3.Controls.GeometryVisitor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Properties;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;


public class StageData  { 
    
    private static final Logger logger = Logger.getLogger(StageData.class.getName());
    
    public static class ControlTypesForAxis
    {
        public ControlTypesForAxis() { xtype = "X"; xtype = "Y"; ztype = "Z"; }
        public String xtype;
        public String ytype;
        public String ztype;
    }
            
    public static class MutableProperties 
    {
        public String       options;
        public Boolean      skip = false;
        public String       type;
        public AudioNode    sound;
        public String       texture;
        public Material     material;
        public String       control;
        public String       controlString;
        public String       objectModel;
        public float        objectDuration;
        public float        angularFactor;
        public float        soundStartTime;
        public float        soundVolume;
        public int          measure;   
        public String       soundfile;
        public float        mass;
        public Geometry     objectGeometry;
        public Vector3f     position = new Vector3f();
        public Vector3f     direction = new Vector3f();
        public Vector3f     velocity = new Vector3f();                
        public Vector3f     scale = new Vector3f();
        
        public ControlTypesForAxis controlTypes = new ControlTypesForAxis();                
        public ControlTypesForAxis getControlTypes() { return controlTypes; }
                
        public String getControlTypePosDirFormed() {
            return "(" + controlTypes.xtype + "=" + position.x + "'" 
                       + controlTypes.ytype  + "=" + position.y + "'" 
                       + controlTypes.ztype + "=" + position.z + ";" 
                       + direction.x + "'" + direction.y + "'" + direction.z + ";" 
                       + velocity.x + "'" + velocity.y + "'" + velocity.z + ")";             
        }        
        
        public String getScaleAsString() { return  "(" + scale.x + "'" + scale.y + "'" + scale.z + ")";  }
    }
    
    public static class TrackProperties {    
        public Vector2f trackMinMaxNotes = new Vector2f(0,127);                
        Boolean         pingpongOffset = false;
        float           trackOffsetPos = -5.0f;            
        
        public MutableProperties getProperties(int note, int measure)
        {
            if(noteInTrackProperties.get(note)!=null) {
                int key = noteInTrackProperties.get(note).floorKey(measure);
                MutableProperties mp = noteInTrackProperties.get(note).get(key);
                return mp;
            }
            if( trackProperties.floorKey(measure) == null ) { return null; }
            int key = trackProperties.floorKey(measure);           
            MutableProperties mp =  trackProperties.get(key);
            return mp;
        }                        
        public TreeMap<Integer,MutableProperties> getTrackMutableMap() { return trackProperties; }
        public TreeMap<Integer,NavigableMap<Integer,MutableProperties>> getNotesMutableProperties() { return noteInTrackProperties; }        
        public TreeMap<Integer,MutableProperties> trackProperties = new TreeMap<Integer, MutableProperties>();
        public TreeMap<Integer,NavigableMap<Integer,MutableProperties>> noteInTrackProperties = new  TreeMap<Integer,NavigableMap<Integer,MutableProperties>>();
    }
    
    public static class StageCameraPosition {
        public Vector3f cameraPosition;
        public Vector3f cameraRotation;
        public Vector3f cameraFrustrum;
    }

    public static class Texts {
        public Texts(String _text, float _time, int _size){ text = _text; time = _time; size = _size; };                
        public String  text;
        public float   time;
        public int     size;
    }
        
    public class HighScore implements Comparable<HighScore>  {
        String playerName;
        int    score;
        String date;
        @Override
        public int compareTo(HighScore other) {      
            if(this.score > other.score ) return -1;
            if(this.score < other.score ) return 1;
            return 0;
        } 
    }
    
    public static int   MAX_HIGHSCORES = 3;
    private HighScore[] highScoreArray = new HighScore[MAX_HIGHSCORES];
    private Boolean     bStageCleared = false;
    private String      stage_floor_texture;
    private String      stageFloorModelString;
    private Float       stage_gapbetween_tracks;
    private Vector3f    stageFloorModelScaleVector = new Vector3f();
    private Vector3f    stageFloorModelTranslationVector = new Vector3f();
    private String      stage_envsphere_texture;
    private Material    playerMaterial;
    private Material    playerMaterialHit;
    private Material    playerMaterialRevert;
    private String      playerModel;
    private Vector3f    player_scale = new Vector3f(1.0f,1.0f,1.0f);
    public  float       bpm;
    
    private Main            mGame;
    private InputStream     isMidiFile;
    private Integer         tracks = 0;
    private Properties      properties = new Properties();
    private SceneAbstract   scene = null;              
    private String          filename;   
    
    private NavigableMap<Integer,StageCameraPosition> cameraMap = new TreeMap<Integer, StageCameraPosition>();
    private TreeMap<Integer, TrackProperties> trackTree = new TreeMap<Integer, TrackProperties>();    
    private TreeMap<Integer, Texts> trackTexts = new TreeMap<Integer, Texts>();   
    private TreeMap<Integer, Integer> tracksAvailableAtMeasure = new TreeMap<Integer, Integer>();  
        
    public TreeMap<Integer, TrackProperties> getTrackPropertiesMap() { return trackTree; }
    public NavigableMap<Integer,StageCameraPosition> getCameraMap() { return cameraMap; }
    public TreeMap<Integer, Texts> getTextsMap() { return trackTexts; }
    public TreeMap<Integer, Integer> getTracksAvailableMap() { return tracksAvailableAtMeasure; }
    public Map<String, Material> materialsMap = new TreeMap<String, Material>();
    public Map<String, AudioNode> soundsMap = new TreeMap<String, AudioNode>();
    public Map<String, Geometry> geometryMap = new TreeMap<String, Geometry>();
    
    public StageData(InputStream inputStream, String file, Main game) {
        
        this.mGame = game;
        this.filename = file;
        
        try {            
            properties.load(inputStream);
            inputStream.close();                                            
                    
            for(int j = 0; j < StageData.MAX_HIGHSCORES; j++) {
                HighScore hs = new HighScore();                
                hs.playerName = "";
                hs.score = 0;
                java.util.Date date = new java.util.Date(); 
                java.text.SimpleDateFormat sdf=new java.text.SimpleDateFormat("dd/MM/yyyy");
                String fecha = sdf.format(date);
                hs.date = fecha;
                highScoreArray[j] = hs;
            }
        
            Enumeration enuKeys = properties.keys();
            while (enuKeys.hasMoreElements()) {
                    String key = (String) enuKeys.nextElement();
                    String value = properties.getProperty(key);
                    logger.log(Level.FINE,"{0} : {1}",new Object[]{key,value});
                }
                                    
            if(!geometryMap.containsKey("box.j3o")) {
                Box box = new Box(1.0f,1.0f,1.0f);
                Geometry geo = new Geometry("box",box);                    
                geometryMap.put("box.j3o", geo);
            }

            if(!geometryMap.containsKey("sphere.j3o")) {
                Sphere sphere = new Sphere(16,16,1.0f);
                Geometry geo = new Geometry("box",sphere);                    
                geometryMap.put("sphere.j3o", geo);
            }
                        
            if(!hasMidiFile()){return;}
            
            InputStream sMidiFile = (InputStream)game.getAssetManager().loadAsset("Sounds/"+properties.getProperty("midifile"));        
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024]; int len;
            
            try{
                while ((len = sMidiFile.read(buffer)) > -1 ) 
                {
                    baos.write(buffer, 0, len);
                } 
                baos.flush();
            } catch (IOException e) { 
                    logger.log(Level.WARNING,"{0}",new Object[]{e}); 
            }
                        
            isMidiFile = new ByteArrayInputStream(baos.toByteArray()); 
            
        } catch (FileNotFoundException e) {                                
                logger.log(Level.SEVERE,"{0}",new Object[]{e});
        } catch (IOException e) {
                logger.log(Level.SEVERE,"{0}",new Object[]{e});
        }                
    }    
    
    public void initialize(Node rootNode, SceneAbstract scene)
    {
        this.scene = scene;
        parseCameraPoints(properties.getProperty("camera", "{0,0.0,0.0,0.0,0.0,0.0,0.0}"));                                   
        parseAvailableTracksAtMeasure(properties.getProperty("tracks","NONE"));
                
        if(tracks == 0 ) {
            int key = tracksAvailableAtMeasure.floorKey(0);                    
            tracks = tracksAvailableAtMeasure.get(key);
        } else { tracks -= 1; }
                
        stage_floor_texture = properties.getProperty("stage_texture", "NONE");        
        stageFloorModelString = properties.getProperty("stage_model", "floor.j3o");        
        String lscale  = properties.getProperty("stage_model_scale","NONE");        
        stage_envsphere_texture = properties.getProperty("stage_sphere_texture","NONE");        
        stage_gapbetween_tracks = Float.parseFloat(properties.getProperty("stage_movement_size","5.0f"));                
        if(lscale.contains("NONE")) { stageFloorModelScaleVector.set(1.0f,1.0f,1.0f); }
        else {
                String delim = "[,]";
                String[] tokens = lscale.split(delim);        
                if(tokens.length == 3) {                    
                    stageFloorModelScaleVector.x = Float.parseFloat(tokens[0].trim());
                    stageFloorModelScaleVector.y = Float.parseFloat(tokens[1].trim());
                    stageFloorModelScaleVector.z = Float.parseFloat(tokens[2].trim());    
                }
        }                                
        String ltrans  = properties.getProperty("stage_model_translation","NONE");        
        if(ltrans.contains("NONE")) { stageFloorModelTranslationVector.set(0.0f,0.0f,0.0f); }
        else {
                String delim = "[,]";
                String[] tokens = ltrans.split(delim);        
                if(tokens.length == 3) {                    
                    stageFloorModelTranslationVector.x = Float.parseFloat(tokens[0].trim());
                    stageFloorModelTranslationVector.y = Float.parseFloat(tokens[1].trim());
                    stageFloorModelTranslationVector.z = Float.parseFloat(tokens[2].trim());    
                }
        }                        
        playerModel = properties.getProperty("player_model","box.j3o");        
        String pscale  = properties.getProperty("player_scale","NONE");        
        if(pscale.contains("NONE")) { player_scale.set(1.0f,1.0f,1.0f); }
        else {
                String delim = "[,]";
                String[] tokens = pscale.split(delim);        
                if(tokens.length == 3) {                    
                    player_scale.x = Float.parseFloat(tokens[0].trim());
                    player_scale.y = Float.parseFloat(tokens[1].trim());
                    player_scale.z = Float.parseFloat(tokens[2].trim());    
                }
        }        
        String pTexture = properties.getProperty("player_tex","NONE");               
        String texts = properties.getProperty("texts","NONE");        
        bpm = Float.parseFloat(properties.getProperty("bpm","120"));
        
        parseTexts(texts);
                       
        playerMaterial = createMaterialFactory(pTexture);        
        String pTextureHit = properties.getProperty("player_texhit","NONE");               
        playerMaterialHit = createMaterialFactory(pTextureHit);                
        
        String pTextureRevert = properties.getProperty("player_tex_revert","NONE");               
        playerMaterialRevert = createMaterialFactory(pTextureRevert);                
        
        for(int i = 0; i <= tracks+1; ++i) {
            String temp = "tr" + (i);                       
            String temp2 = properties.getProperty(temp,"none");                        
            if(temp2.isEmpty()) temp2 = "none";
            logger.log(Level.FINE,"{0} : {1}",new Object[]{temp,temp2});                        
            TrackProperties track = new TrackProperties();
            parseMutableProperties(track.trackProperties,temp2);            
            if(track.trackProperties.size() > 0) {
                trackTree.put(i, track);
            }            
            for(int j = 0; j < 128; j++) {
                String tempNote = "tr" + (i) + "_" + j;
                String tempNoteFound = properties.getProperty(tempNote,"NONE");
                if(!tempNoteFound.equals("NONE")) {
                    logger.log(Level.FINE,"{0} : {1}",new Object[]{tempNote,tempNoteFound});
                    NavigableMap<Integer,MutableProperties> nm = new TreeMap<Integer,MutableProperties>();
                    parseMutableProperties(nm,tempNoteFound);                    
                    track.noteInTrackProperties.put(j,nm);
                }
            }
            String concat = temp.concat("_ranges");
            String rangesFound = properties.getProperty(concat, "NONE");
            if(!rangesFound.equals("NONE")) {
                logger.log(Level.FINE,"{0} : {1}",new Object[]{concat,rangesFound});
                String delim = "[,]";
                String[] tokens = rangesFound.split(delim);                       
                track.trackMinMaxNotes = new Vector2f(Float.parseFloat(tokens[0]),Float.parseFloat(tokens[1]));
            }
        }            
    }
        
    private void parseAvailableTracksAtMeasure(String temp) {     
        if(temp.contains("NONE")) { tracksAvailableAtMeasure.put(0,1); }        
        String delim = "[:]";
        String[] tokens = temp.split(delim);        
        for (int i = 0; i < tokens.length; i++) {           
            //Avoid brackets
            tokens[i] = tokens[i].substring(1, tokens[i].length()-1);            
            String dl = "[,]";
            String[] tokens2 = tokens[i].split(dl);
            if(tokens2.length != 2) { logger.log(Level.WARNING,"ParseAvailableTracksAtMeasure not enough tokens"); continue; }                                    
            int pos = Integer.parseInt(tokens2[0].trim());
            int num = Integer.parseInt(tokens2[1].trim());
            tracksAvailableAtMeasure.put(pos, num);
        }
    }
    
    private void parseTexts(String temp) {     
        if(temp.contains("NONE")) return;        
        String delim = "[:]";
        String[] tokens = temp.split(delim);        
        for (int i = 0; i < tokens.length; i++) {                       
            tokens[i] = tokens[i].substring(1, tokens[i].length()-1);            
            String dl = "[,]";
            String[] tokens2 = tokens[i].split(dl);
            if(tokens2.length != 4) { logger.log(Level.WARNING,"ParseTexts not enough tokens"); continue; }            
            Texts t = new Texts(
                    tokens2[1].trim(),
                    Float.parseFloat(tokens2[2].trim()),
                    Integer.parseInt(tokens2[3].trim()) );             
            int pos = Integer.parseInt(tokens2[0].trim());
            trackTexts.put(pos, t);
        }
    }
            
    public void parseMutableProperties(NavigableMap<Integer,MutableProperties> nMap, String temp) {                                
        String delim = "[:]";
        String[] tokens = temp.split(delim);                
        for (int i = 0; i < tokens.length; i++) {                       
            tokens[i] = tokens[i].substring(1, tokens[i].length()-1);            
            String dl = "[,]";
            String[] tokens2 = tokens[i].split(dl);
            if(tokens2.length == 2) {            
                MutableProperties prop;
                prop = new MutableProperties();            
                prop.measure = Integer.parseInt(tokens2[0].trim());     
                String action = tokens2[1].trim().toLowerCase();                          
                if(action.equals("skip")) {                    
                    prop.skip = true; prop.type = "skip"; prop.control = "skip";
                    prop.controlTypes.xtype = "skip"; prop.controlTypes.ytype = "skip"; prop.controlTypes.ztype = "skip";
                    prop.position = new Vector3f(0.0f,0.0f,0.0f); prop.direction = new Vector3f(0.0f,0.0f,0.0f);
                    prop.velocity = new Vector3f(0.0f,0.0f,0.0f); prop.scale  = new Vector3f(0.0f,0.0f,0.0f);
                    prop.objectDuration = 0.0f; prop.mass = 0.0f; prop.soundfile = "skip";
                    prop.soundStartTime = 0.0f; prop.soundVolume = 0.0f; prop.objectModel = "skip"; prop.texture = "skip";                    
                    nMap.put(prop.measure, prop);
                    continue;
                }                                               
            }            
            if(tokens2.length < 11) { logger.log(Level.WARNING,"ParseTrackProperties not enough tokens"); continue; }
            MutableProperties prop;
            prop = new MutableProperties();
            prop.skip = false;
            prop.measure = Integer.parseInt(tokens2[0].trim());                
            prop.type = tokens2[1].trim();                        
            prop.controlString = tokens2[2].trim();
            
            parseObjPosDir(prop.controlString, prop);                                    
            
            String scale = tokens2[3].trim();
            parseScale(scale, prop);
            
            prop.objectDuration = Float.parseFloat(tokens2[4].trim());            
            prop.mass = Float.parseFloat(tokens2[5].trim());                                            
            prop.soundfile = tokens2[6].trim();                        
            prop.soundStartTime = Float.parseFloat(tokens2[7].trim());                
            prop.soundVolume = Float.parseFloat(tokens2[8].trim());                
            prop.objectModel = tokens2[9].trim();                     
            if(geometryMap.containsKey(prop.objectModel)) {
               prop.objectGeometry =  geometryMap.get(prop.objectModel);
            } else {
                Node loadedNode = (Node)scene.getGame().getAssetManager().loadModel("Models/" + prop.objectModel);
                mGame.getRenderManager().preloadScene(loadedNode);
                loadedNode.setName(prop.objectModel);           
                GeometryVisitor geoV = new GeometryVisitor();
                loadedNode.depthFirstTraversal(geoV);            
                prop.objectGeometry = geoV.getGeometry();
                geometryMap.put(prop.objectModel, prop.objectGeometry);                
            }
            prop.texture = tokens2[10].trim();                                                                          
            
            if(prop.soundfile.equals("none") || prop.soundfile.equals("NONE")) { prop.sound = null; } 
            else { prop.sound = createSoundFactory(prop.soundfile, prop.soundStartTime, prop.soundVolume); }                                    
            prop.material = createMaterialFactory(prop.texture);            
            if(tokens2.length == 12) { prop.options = tokens2[11].trim().toUpperCase(); } 
            else { prop.options = new String(); }                        
            nMap.put(prop.measure, prop);
        }
    }    
    
    public AudioNode createSoundFactory(String input, float start, float end) {                        
        if(soundsMap.containsKey(input))    { return soundsMap.get(input); }                        
                                                    
        AudioNode sound = new AudioNode(mGame.getAssetManager(), "Sounds/"+input, false);
        sound.setLooping(false);
        sound.setVolume(0.0f);
        sound.setPositional(false);
        getScene().getRootNode().attachChild(sound);
        sound.play();  
        soundsMap.put(input, sound);                        
        return sound;
    }
    
    public Geometry createObjectGeometry(String input) {            
        if(geometryMap.containsKey(input)) { return  geometryMap.get(input); }
        else {
            Node loadedNode = (Node)scene.getGame().getAssetManager().loadModel("Models/" + input);
            mGame.getRenderManager().preloadScene(loadedNode);
            loadedNode.setName(input);           
            GeometryVisitor geoV = new GeometryVisitor();
            loadedNode.depthFirstTraversal(geoV);                             
            geometryMap.put(input, geoV.getGeometry());      
            return geoV.getGeometry();
        }
    }
            
    public Material createMaterialFactory(String input) {        
         if( ( !input.contains("NONE") || !input.contains("none") ) && materialsMap.containsKey(input)) {
             return materialsMap.get(input);
         }
         
        String texture; Material material;
        if(input.contains("Anim")) {          
            String temp = input;
            temp = temp.substring(5,temp.length()-1);                    
            String delim = "[;]";
            String[] tokens = temp.split(delim);            
            texture = tokens[0].trim();
            int u = Integer.parseInt(tokens[1].trim());
            int v = Integer.parseInt(tokens[2].trim());
            int speed = Integer.parseInt(tokens[3].trim());                        
            material = new Material(scene.getGame().getAssetManager(), "MatDefs/AnimatedSprite.j3md");                
            TextureKey key = new TextureKey("Textures/" +  texture);
            key.setGenerateMips(true);
            Texture tex = scene.getGame().getAssetManager().loadTexture(key);
            tex.setWrap(Texture.WrapMode.Repeat);
            material.setTexture("AniTexMap", tex);
            material.setInt("numTilesU", u);
            material.setInt("numTilesV", v);
            material.setInt("Speed", speed);
             material.setFloat("distance", 1.0f);
            material.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
            material.getAdditionalRenderState().setAlphaTest(true);
            material.getAdditionalRenderState().setAlphaFallOff(0.05f); 
            material.getAdditionalRenderState().setWireframe(false);
            material.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);            
        } else if(input.contains("Sprite")) {
            String temp = input;
            temp = temp.substring(7,temp.length()-1);                        
            String delim = "[;]";
            String[] tokens = temp.split(delim);            
            texture = tokens[0].trim();
            int u = Integer.parseInt(tokens[1].trim());
            int v = Integer.parseInt(tokens[2].trim());
            int tile = Integer.parseInt(tokens[3].trim());                        
            material = new Material(scene.getGame().getAssetManager(), "MatDefs/NumberedSprite.j3md");                
            TextureKey key = new TextureKey("Textures/" +  texture);
            key.setGenerateMips(true);
            Texture tex = scene.getGame().getAssetManager().loadTexture(key);
            tex.setWrap(Texture.WrapMode.Repeat);
            material.setTexture("AniTexMap", tex);
            material.setInt("numTilesU", u);
            material.setInt("numTilesV", v);
            material.setInt("tile", tile);
            material.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
            material.getAdditionalRenderState().setAlphaTest(true);
            material.getAdditionalRenderState().setAlphaFallOff(0.05f); 
            material.getAdditionalRenderState().setWireframe(false);     
            material.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
        } else {                          
            if(!input.equals("none") && !input.equals("NONE") && !input.contains(".tgr")) {       
                material = new Material(scene.getGame().getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");                
                TextureKey key = new TextureKey("Textures/" + input, false);
                key.setGenerateMips(true);
                Texture tex = scene.getGame().getAssetManager().loadTexture(key);
                tex.setWrap(Texture.WrapMode.Repeat);            
                material.setTexture("ColorMap", tex);
                material.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
                material.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
            } else{
                material = new Material(scene.getGame().getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md"); 
                material.setColor("Color", ColorRGBA.randomColor());
            }   
        }         
        material.preload(mGame.getRenderManager());        
        materialsMap.put(input, material);           
        return material;
    }
    
    public static void parseScale(String temp, MutableProperties prop ) {
        temp = temp.substring(temp.indexOf("(", 0)+1,temp.length()-1);
        String delim2 = "[']";
        String[] tokens2 = temp.split(delim2);        
        prop.scale.x = Float.parseFloat(tokens2[0].trim());
        prop.scale.y = Float.parseFloat(tokens2[1].trim());
        prop.scale.z = Float.parseFloat(tokens2[2].trim());           
    }
    
    public static void parseObjPosDir(String temp, MutableProperties prop) {
        prop.control = temp.substring(0,temp.indexOf("(", 0));
        prop.controlString = temp.substring(temp.indexOf("(", 0),temp.length());        
        temp = temp.substring(temp.indexOf("(", 0)+1,temp.length()-1);                
        String delim = "[;]";
        String[] tokens = temp.split(delim);         
        String delim2 = "[']";
        String[] tokens2 = tokens[0].split(delim2);        
        String delim3 = "[=]";
        String[] pair = tokens2[0].trim().split(delim3);        
        prop.controlTypes.xtype = pair[0].trim();
        prop.position.x = Float.parseFloat(pair[1].trim());        
        pair = tokens2[1].trim().split(delim3);     
        prop.controlTypes.ytype = pair[0].trim();
        prop.position.y = Float.parseFloat(pair[1].trim());        
        pair = tokens2[2].trim().split(delim3);     
        prop.controlTypes.ztype = pair[0].trim();
        prop.position.z = Float.parseFloat(pair[1].trim());        
        String[] tokens3 = tokens[1].split(delim2);
        prop.direction.x = Float.parseFloat(tokens3[0].trim());
        prop.direction.y = Float.parseFloat(tokens3[1].trim());
        prop.direction.z = Float.parseFloat(tokens3[2].trim());                        
        String[] tokens4 = tokens[2].split(delim2);
        prop.velocity.x = Float.parseFloat(tokens4[0].trim());
        prop.velocity.y = Float.parseFloat(tokens4[1].trim());
        prop.velocity.z = Float.parseFloat(tokens4[2].trim());    
    }
    
    private void parseCameraPoints(String temp) {
        String delim = "[:]";
        String[] tokens = temp.split(delim);        
        for (int i = 0; i < tokens.length; i++)
        {   
            tokens[i] = tokens[i].substring(1, tokens[i].length()-1);
            
            String dl = "[,]";
            String[] tokens2 = tokens[i].split(dl);
            
            logger.log(Level.FINE,"{0} Lenght: {1}",new Object[]{tokens[i],tokens2.length});
            
            if(tokens2.length != 10) { logger.log(Level.WARNING,"ParseCameraPoints not enough tokens"); return; }

            StageCameraPosition cameraPos = new StageCameraPosition();
            
            int pos = Integer.parseInt(tokens2[0]);
            
            cameraPos.cameraPosition = new Vector3f(Float.parseFloat(tokens2[1].trim()),
                       Float.parseFloat(tokens2[2].trim()),Float.parseFloat(tokens2[3].trim()));
            
            cameraPos.cameraRotation = new Vector3f(Float.parseFloat(tokens2[4].trim()),
                       Float.parseFloat(tokens2[5].trim()),Float.parseFloat(tokens2[6].trim()));
           
            cameraPos.cameraFrustrum = new Vector3f(Float.parseFloat(tokens2[7].trim()),
                       Float.parseFloat(tokens2[8].trim()),Float.parseFloat(tokens2[9].trim()));
            
            cameraMap.put(pos, cameraPos);
        }
    }
            
    private Material createMaterial(AssetManager assMgr, String texture,  ColorRGBA color) {    
        Material material;        
        if(!texture.equals("none") && !texture.equals("NONE") && !texture.contains(".tgr"))
        {       
            material = new Material(assMgr, "Common/MatDefs/Misc/Unshaded.j3md");                
            TextureKey key = new TextureKey("Textures/" + texture, false);
            key.setGenerateMips(true);
            Texture tex = assMgr.loadTexture(key);
            tex.setWrap(Texture.WrapMode.Repeat);            
            material.setTexture("ColorMap", tex);
            material.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
            material.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
        } else{
            material = new Material(assMgr, "Common/MatDefs/Misc/Unshaded.j3md"); 
            material.setColor("Color", color);
        }                           
        return material;
    }         

    public static Geometry createModelsGeometries(AssetManager assMgr, String input) {            
        if(input.contains("floor.j3o")) {
            Box floor = new Box(Vector3f.ZERO, 1000f, 0.1f, 1000f);
            Geometry geo = new Geometry("box",floor);                    
            return geo;
        }        
        Node loadedNode = (Node)assMgr.loadModel("Models/" + input);      
        loadedNode.setName(input);           
        GeometryVisitor geoV = new GeometryVisitor();
        loadedNode.depthFirstTraversal(geoV);                                  
        return geoV.getGeometry();
    }
    
        
    public static Material createModelsMaterials(AssetManager assMgr, String textureString) {            
        Material material;        
        if(textureString.contains("Scroll")) {                      
            String temp = textureString;                   
            temp = temp.substring(7,temp.length()-1);                    
            String delim = "[;]";
            String[] tokens = temp.split(delim);            
            String texture = tokens[0].trim();
            float scale = Float.parseFloat(tokens[1].trim());
            float speed = Float.parseFloat(tokens[2].trim());
            float distance = 0.6f;            
            if(tokens.length == 4) { distance =  Float.parseFloat(tokens[3].trim()); }                                
            material = new Material(assMgr, "MatDefs/scrolling.j3md");
            TextureKey key = new TextureKey("Textures/" + texture, false);
            key.setGenerateMips(true);
            Texture tex = assMgr.loadTexture(key);
            tex.setWrap(Texture.WrapMode.Repeat);
            material.setTexture("m_ColorMap", tex);
            material.setFloat("scaleTexture", scale);             
            material.setFloat("scrollOffset", speed);
            material.setFloat("distance", distance);
        } else if(textureString.contains("Anim")) {          
            String temp = textureString;
            temp = temp.substring(5,temp.length()-1);                    
            String delim = "[;]";
            String[] tokens = temp.split(delim);            
            String texture = tokens[0].trim();
            int u = Integer.parseInt(tokens[1].trim());
            int v = Integer.parseInt(tokens[2].trim());
            int speed = Integer.parseInt(tokens[3].trim());                 
            float distance = 0.6f;            
            if(tokens.length == 5) { distance =  Float.parseFloat(tokens[4].trim()); }                         
            material = new Material(assMgr, "MatDefs/AnimatedSprite.j3md");                
            TextureKey key = new TextureKey("Textures/" +  texture);
            key.setGenerateMips(true);
            Texture tex = assMgr.loadTexture(key);
            tex.setWrap(Texture.WrapMode.Repeat);
            material.setTexture("AniTexMap", tex);
            material.setInt("numTilesU", u);
            material.setInt("numTilesV", v);
            material.setInt("Speed", speed);
            material.setFloat("distance", distance);
            material.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
            material.getAdditionalRenderState().setAlphaTest(true);
            material.getAdditionalRenderState().setAlphaFallOff(0.05f); 
            material.getAdditionalRenderState().setWireframe(false);
            material.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
        } else {
            material = new Material(assMgr, "Common/MatDefs/Misc/Unshaded.j3md"); 
            TextureKey key = new TextureKey("Textures/" + textureString, false);
            key.setGenerateMips(true);
            Texture tex = assMgr.loadTexture(key);
            tex.setWrap(Texture.WrapMode.Repeat);            
            material.setTexture("ColorMap", tex);
            material.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
            material.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
        }                   
        return material;
    }
    
    // Getter methods
    
    public String getAudioFile(){ return properties.getProperty("audiofile"); }
    
    public InputStream getMidiFile() throws IOException { isMidiFile.reset(); return isMidiFile; }
    
    public String getLabel(){ return properties.getProperty("name"); }
    
    public boolean isTrackDefined(int track) { return trackTree.containsKey(track); }
        
    public String getStageTexture() { return stage_floor_texture; }
    
    public String getStageModel() { return stageFloorModelString; }
        
    public Vector3f getStageModelScale() { return stageFloorModelScaleVector; }
                
    public Vector3f getStageModelTranslation() { return stageFloorModelTranslationVector; }
        
    public String getStageEnvSphereTexture() { return stage_envsphere_texture; }
            
    public Float getStageGapBetweenTracksSize() { return stage_gapbetween_tracks; }
    
    public Vector3f getPlayerScale() { return player_scale; }
    
    public Boolean checkHighScore(int Points) { if(highScoreArray[MAX_HIGHSCORES -2].score < Points) return true; return false; }
    
    public HighScore getHighScore(int pos) { if(pos > MAX_HIGHSCORES) return null; return highScoreArray[pos]; }

    public void setStageCleared(Boolean cleared) { bStageCleared = cleared; }
    
    public Boolean getStageCleared() { return bStageCleared; }
    
    public int getNumTracks() { return tracks; }
           
    public int getAvailableTracksAtMeasure(int measure) { return tracksAvailableAtMeasure.get(tracksAvailableAtMeasure.floorKey(measure)); }
    
    public void setNumTracks(int numTracks) { tracks = numTracks; }

    Vector2f getMinMaxNotesForTrack(int track) { return  trackTree.get(track).trackMinMaxNotes; }
    
    Texts getTextForMeasure(int measure) { return  trackTexts.get(measure); }
            
    public String getPlayerModel() { return playerModel; }
    
    public Material getPlayerMaterial() { return playerMaterial; }
    
    public Material getPlayerMaterialHit() { return playerMaterialHit; }
    
    public Material getPlayerMaterialRevert() { return playerMaterialRevert; }
            
    public float getBPM() { return bpm; }
    
    public  String getFilename() { return filename; }
    
    public SceneAbstract getScene() { return scene; }
        
    // Get Functions
    
    public final boolean hasMidiFile()
    {
        if(properties.getProperty("midifile") != null && !properties.getProperty("midifile").isEmpty()){
            return true; 
        }
        return false;
    }
    
    
    public float getTrackOffsetPosition(int track)
    {
        TrackProperties tp = trackTree.get(track);
        return tp.trackOffsetPos;
    }
    
    public void incTrackOffsetPosition(int track)
    {
         TrackProperties tp = trackTree.get(track);
         
        tp.trackOffsetPos += 5.0f;
        
        if(tp.trackOffsetPos > scene.getPositionMaxX())
        {
            tp.trackOffsetPos = 0.0f;
        }                       
    }
    
    public void pingpongTrackOffsetPosition(int track) {
        TrackProperties tp = trackTree.get(track);         
        if(tp.pingpongOffset == false) { tp.trackOffsetPos += 5.0f; }
        else { tp.trackOffsetPos -= 5.0f; }        
        if(tp.trackOffsetPos >= scene.getPositionMaxX()) { tp.pingpongOffset = true; }                       
        else if(tp.trackOffsetPos <= 0) { tp.pingpongOffset = false; }
    }
        
    public int getCameraPositionPoints() { int key = cameraMap.size(); return key; }
    
    public int getCurrentCameraPositionPoints(int measure) {
        int key = cameraMap.floorKey(measure); return key; }
    
    public StageCameraPosition getCameraPosition(int measure) {
        int key = cameraMap.floorKey(measure); return cameraMap.get(key); }
       
    public MutableProperties getMutableProperties(int track, int note, int measure) {
        MutableProperties mp = trackTree.get(track).getProperties(note, measure);
        return mp; }
    
    public String getTrackType(int track, int note, int measure){
        MutableProperties mp = trackTree.get(track).getProperties(note, measure);
        return mp.type; }      
    
    public void setHighScoreMap(int pos, int value, String name, String date) {        
        highScoreArray[pos].score = value;       
        highScoreArray[pos].playerName = name;     
        highScoreArray[pos].date = date;    
        Arrays.sort(highScoreArray);
    }
                
    public void setMinMaxNoteOnTrack(int track, Vector2f minMaxVec) {
        if(minMaxVec.x == 127.0f) { minMaxVec.x = 0.0f; }        
        TrackProperties tp = trackTree.get(track);
        if(tp == null) { logger.log(Level.FINE,"{0} is not available because size: {1}",new Object[]{track,trackTree.size()});
            return;  }
        tp.trackMinMaxNotes = new Vector2f(minMaxVec);        
    }        
    
    // Output data as string 
    
    public String getStageAsStringForOutput()
    {
        String temp = "";
        temp = temp + "name=" + getLabel() + "\n";
        temp = temp + "#midifile=" + properties.getProperty("midifile") + "\n";
        temp = temp + "audiofile=" + getAudioFile() + "\n";

        temp = temp + "bpm=" + bpm + "\n";
        
        temp = temp + "player_model=" + playerModel + "\n";
        temp = temp + "player_tex=" + properties.getProperty("player_tex","NONE") + "\n";
        temp = temp + "player_scale=" + properties.getProperty("player_scale","1.0,1.0,1.0") + "\n";
        temp = temp + "player_texhit=" + properties.getProperty("player_texhit","NONE") + "\n";
        temp = temp + "player_tex_revert=" + properties.getProperty("player_tex_revert","NONE") + "\n";
        
        temp = temp + "stage_model=" + properties.getProperty("stage_model","floor.j3o") + "\n";
        temp = temp + "stage_model_scale=" + properties.getProperty("stage_model_scale","1.0,1.0,1.0") + "\n";
        temp = temp + "stage_model_translation=" + properties.getProperty("stage_model_translation","0.0,0.0,0.0") + "\n";
        temp = temp + "stage_texture=" + stage_floor_texture + "\n";
        temp = temp + "stage_sphere_texture=" + stage_envsphere_texture + "\n";
                
        temp = temp + "stage_movement_size=" + stage_gapbetween_tracks + "\n";
        
        if(getTracksAvailableMap().size() > 0) {
        temp = temp + "tracks=";
        for(Map.Entry<Integer,Integer> entry : getTracksAvailableMap().entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            temp = temp + "{" + key + "," + value + "}:" ;
        } 
        temp = temp.substring(0, temp.length()-1);
        temp = temp + "\n";
        }
        
        if(getTextsMap().size() > 0) {
        temp = temp + "texts=";
        for(Map.Entry<Integer,Texts> entry : getTextsMap().entrySet()) {
            Integer key = entry.getKey();
            Texts value = entry.getValue();
            temp = temp + "{" + key + "," + value.text + "," + value.time + "," + value.size + "}:" ;
        } 
        temp = temp.substring(0, temp.length()-1);    
        temp = temp + "\n";
        }
        
        
        temp = temp + "camera=";
        for(Map.Entry<Integer,StageCameraPosition> entry : getCameraMap().entrySet()) {
            Integer key = entry.getKey();
            StageCameraPosition value = entry.getValue();
            temp = temp + "{" + key + 
                    "," + value.cameraPosition.x + "," + value.cameraPosition.y + "," + value.cameraPosition.z + 
                    "," + value.cameraRotation.x + "," + value.cameraRotation.y + "," + value.cameraRotation.z + 
                    "," + value.cameraFrustrum.x + "," + value.cameraFrustrum.y + "," + value.cameraFrustrum.z + 
                    "}:" ;
        } 
        temp = temp.substring(0, temp.length()-1); 
        temp = temp + "\n";
                

        for(Map.Entry<Integer,TrackProperties> entry : getTrackPropertiesMap().entrySet()) {
            Integer key = entry.getKey();
            TrackProperties value = entry.getValue();
            temp = temp + "tr" + key +"_ranges=" + value.trackMinMaxNotes.x + "," + value.trackMinMaxNotes.y + "\n";
            temp = temp + "tr" + key + "=";
            for(Map.Entry<Integer,MutableProperties> entry2 : value.getTrackMutableMap().entrySet()) {
                Integer key2 = entry2.getKey();
                MutableProperties value2 = entry2.getValue();
            
                if(value2.skip == true) {
                    temp = temp + "{" + value2.measure + ",skip}:";                     
                }
                else {
                temp = temp + "{" + value2.measure + "," + 
                    value2.type + "," + value2.control  + value2.getControlTypePosDirFormed() + ","
                        + value2.getScaleAsString() + "," + value2.objectDuration + "," + value2.mass + 
                        "," + value2.soundfile + "," + value2.soundStartTime + "," + value2.soundVolume + "," + value2.objectModel + "," + value2.texture +  "," + (!value2.options.isEmpty() ? value2.options + "}:" : "}:");
                }
            } 
            temp = temp.substring(0, temp.length()-1); 
            temp = temp + "\n";
            
            for(Map.Entry<Integer,NavigableMap<Integer,MutableProperties>> entry3 : value.getNotesMutableProperties().entrySet()) {
                Integer key3 = entry3.getKey();
                NavigableMap<Integer,MutableProperties> value3 = entry3.getValue();            
                {
                    temp = temp + "tr" + key + "_";
                    for(Map.Entry<Integer,MutableProperties> entry4 : value3.entrySet()) 
                    {
                        Integer key4 = entry4.getKey();
                        MutableProperties value2 = entry4.getValue();
                        if(value2.skip == true) 
                        {
                            temp = temp + "{" + value2.measure + ",skip}:";                     
                        }
                        else 
                        {
                            temp = temp + key3 + "={" + key4 + "," + 
                            value2.type + "," + value2.control  + value2.getControlTypePosDirFormed() + ","
                               + value2.getScaleAsString() + "," + value2.objectDuration + "," + value2.mass
                               + "," + value2.soundfile  + "," + value2.soundStartTime  +  "," + value2.soundVolume 
                               + "," + value2.objectModel + "," + value2.texture + "," + (!value2.options.isEmpty() ? value2.options + "}:" : "}:");
                        }
                    }
                    temp = temp.substring(0, temp.length()-1); 
                    temp = temp + "\n";
                }
            } 
            temp = temp.substring(0, temp.length()-1); 
            temp = temp + "\n";
            
        }
        
        logger.log(Level.FINE,"{0}",new Object[]{temp});       
        return temp;
    }
        
    
} 
