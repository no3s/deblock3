/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ingamedebug;

/**
 *
 * @author no3z
 */

import com.jme3.audio.AudioNode;
import com.jme3.material.Material;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import deBlock3.AppStates.GameState;
import deBlock3.StageData;
import deBlock3.StageData.MutableProperties;
import deBlock3.StageData.Texts;
import deBlock3.StageData.TrackProperties;
import deBlock3.StageData.StageCameraPosition;
import deBlock3.Main;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

public class StageDebugGUI implements ActionListener {

    private static final Logger logger = Logger.getLogger(StageDebugGUI.class.getName());
    
    private JPanel comboPanel;
    private JFrame guiFrame;                    
    private JPanel pContainer;
    private StageData mStageData = null;
    private Boolean loaded = false;
    private JSpinner spinner = null;
    private JLabel text = null;
    private Main game = null;
    private Timer timer = null;
    private File fileToSave = null;
    
    public StageDebugGUI(Main g) {
        game = g;
     
    }

    public void setGUIStage(final StageData stageData){
                             
        mStageData = stageData;
        guiFrame = new JFrame();
        pContainer = new JPanel();
        
        //make sure the program exits when the frame closes
        guiFrame.setTitle("deBlock3 Editor");
        guiFrame.setSize(game.getContext().getSettings().getWidth()-20,490);
              
        //This will center the JFrame in the middle of the screen
        guiFrame.setLocation(10, 10);
        guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
        comboPanel = new JPanel();
        JLabel comboLbl2 = new JLabel(mStageData.getLabel().toString());                   
       
        spinner = new JSpinner(new SpinnerNumberModel(mStageData.bpm, 0, 600, 1));
        spinner.setValue(mStageData.bpm);        
        spinner.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
              mStageData.bpm = Float.parseFloat(spinner.getValue().toString());
            }
        });
        
        text = new JLabel();
        
        
        
        comboPanel.add(comboLbl2);      
        comboPanel.add(spinner);   
        comboPanel.add(text);
        
        
        
        pContainer.setLayout(new GridBagLayout()); 
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        guiFrame.add(pContainer, BorderLayout.CENTER);
        guiFrame.add(comboPanel, BorderLayout.SOUTH); 
        
        logger.log(Level.FINE," {0} Initializing Debug Menu GUI", new Object[]{stageData.getLabel()});

        final TrackPropTableModel tbproptbmod = new TrackPropTableModel(stageData.getTrackPropertiesMap());
        final JTable table1 = new JTable( tbproptbmod );
        JScrollPane scrollPane1 = new JScrollPane(table1); 
        
        final CameraPosTableModel tbcammod = new CameraPosTableModel(stageData.getCameraMap());
        final JTable table_cam = new JTable( tbcammod );
        JScrollPane scrollPane_cam = new JScrollPane(table_cam); 
        
        final TracksTableModel tbtracksmod = new TracksTableModel(stageData.getTracksAvailableMap());
        final JTable table_tracks = new JTable( tbtracksmod );
        JScrollPane scrollPane_tracks = new JScrollPane(table_tracks); 
        
        final TextsTrackTableModel tbtextsmod = new TextsTrackTableModel(stageData.getTextsMap());
        final JTable table_texts = new JTable( tbtextsmod );
        JScrollPane scrollPane_texts = new JScrollPane(table_texts); 
        
        final JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Cam", scrollPane_cam);
        tabbedPane.addTab("Tracks", scrollPane_tracks);
        tabbedPane.addTab("Texts", scrollPane_texts);
        
        final CurrentNotesTableModel tm2 = new CurrentNotesTableModel(new TreeMap<Integer,NavigableMap<Integer,StageData.MutableProperties>>());
        final JTable table2 = new JTable( tm2 );
        JScrollPane scrollPane2 = new JScrollPane(table2); 
        
        final MutablePropTableModel tm3 = new MutablePropTableModel(new TreeMap<Integer, StageData.MutableProperties>());
        final JTable table3 = new JTable( tm3 );
        JScrollPane scrollPane3 = new JScrollPane(table3); 
        
        c.weightx = 0.33;
        c.weighty = 0.33;
        c.gridx = 0;
        c.gridy = 0;
        pContainer.add(scrollPane1,c);        
         
        c.gridx = 1;
        c.weightx = 0.33;
        pContainer.add(tabbedPane,c);        
        
        c.weighty = 0.66;
        c.weightx = 0.10;
        c.gridx = 0;
        c.gridy = 1;
        pContainer.add(scrollPane2,c); 
                
        c.weightx = 0.9;                
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 2;
        pContainer.add(scrollPane3,c);         
        
        
        table3.getColumnModel().getColumn(0).setPreferredWidth(2);
        table3.getColumnModel().getColumn(1).setPreferredWidth(5);
        table3.getColumnModel().getColumn(2).setPreferredWidth(5);        
        table3.getColumnModel().getColumn(3).setPreferredWidth(9);
        table3.getColumnModel().getColumn(4).setPreferredWidth(9);
        table3.getColumnModel().getColumn(5).setPreferredWidth(9);        
        table3.getColumnModel().getColumn(6).setPreferredWidth(30);
        table3.getColumnModel().getColumn(7).setPreferredWidth(30);
        table3.getColumnModel().getColumn(8).setPreferredWidth(30);        
        table3.getColumnModel().getColumn(9).setPreferredWidth(30);        
        table3.getColumnModel().getColumn(10).setPreferredWidth(2);

        table3.getColumnModel().getColumn(11).setPreferredWidth(5);
        table3.getColumnModel().getColumn(12).setPreferredWidth(15);
        
        table3.getColumnModel().getColumn(13).setPreferredWidth(5);
        table3.getColumnModel().getColumn(14).setPreferredWidth(5);
                        
        table3.getColumnModel().getColumn(15).setPreferredWidth(15);
        table3.getColumnModel().getColumn(16).setPreferredWidth(15);
        table3.getColumnModel().getColumn(17).setPreferredWidth(2);
        table3.getColumnModel().getColumn(18).setPreferredWidth(15);

        JComboBox combotype = new JComboBox();
        combotype.addItem("NORMAL"); 
        combotype.addItem("GOOD"); 
        combotype.addItem("BAD"); 
        combotype.addItem("HEAL");               
        combotype.addItem("FIRE"); 
        combotype.addItem("SWAP");               
        table3.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(combotype));     
        
        JComboBox combocontrol = new JComboBox();
        combocontrol.addItem("user"); combocontrol.addItem("seek");        
        table3.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(combocontrol));        
        JComboBox combo = new JComboBox();
        combo.addItem("user"); combo.addItem("sceneinc"); combo.addItem("trackinc"); combo.addItem("trackpp");
        combo.addItem("player"); combo.addItem("sin"); combo.addItem("range");       
        
        final DefaultComboBoxModel cbmatmodel = new DefaultComboBoxModel();
        final JComboBox combomaterial = new JComboBox(cbmatmodel);
        cbmatmodel.addElement("NONE");   
        for(Map.Entry<String, Material> entry :  mStageData.materialsMap.entrySet()) {
            String key = entry.getKey();                        
                cbmatmodel.addElement(key);
            }
        
        final DefaultComboBoxModel cbsndmodel = new DefaultComboBoxModel();
        final JComboBox combosound = new JComboBox(cbsndmodel);
        cbsndmodel.addElement("NONE");
        for(Map.Entry<String, AudioNode> entry :  mStageData.soundsMap.entrySet()) {
            String key = entry.getKey();                        
                cbsndmodel.addElement(key);
            }
        
        final DefaultComboBoxModel cbgeomodel = new DefaultComboBoxModel();
        final JComboBox combogeo = new JComboBox(cbgeomodel);
                       
        for(Map.Entry<String, Geometry> entry :  mStageData.geometryMap.entrySet()) {
            String key = entry.getKey();                        
                cbgeomodel.addElement(key);
            }
        
        
        table3.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(combo));
        table3.getColumnModel().getColumn(4).setCellEditor(new DefaultCellEditor(combo));
        table3.getColumnModel().getColumn(5).setCellEditor(new DefaultCellEditor(combo));        
        table3.getColumnModel().getColumn(6).setCellEditor(new Vector3fCellEditor());
        table3.getColumnModel().getColumn(7).setCellEditor(new Vector3fCellEditor());
        table3.getColumnModel().getColumn(8).setCellEditor(new Vector3fCellEditor());
        table3.getColumnModel().getColumn(9).setCellEditor(new Vector3fCellEditor());           
        table3.getColumnModel().getColumn(12).setCellEditor(new DefaultCellEditor(combosound));
        table3.getColumnModel().getColumn(15).setCellEditor(new DefaultCellEditor(combogeo));   
        table3.getColumnModel().getColumn(16).setCellEditor(new DefaultCellEditor(combomaterial));
        
        
        table3.setCellSelectionEnabled(true);
        table3.setGridColor(Color.gray);
               
        final MutableProperties selection = new MutableProperties();
        
        ActionListener Copylistener = new ActionListener() {
        public void actionPerformed(ActionEvent event) {          
            int row = table3.getSelectedRow();
            if (row != -1) 
            {                        
                logger.log(Level.FINE,"Copy Data Row: {0} ", new Object[]{row});

                selection.type = (String) table3.getValueAt(row,1);
                selection.control = (String) table3.getValueAt(row,2);
                selection.controlTypes.xtype = (String) table3.getValueAt(row,3);
                selection.controlTypes.ytype = (String) table3.getValueAt(row,4);
                selection.controlTypes.ztype = (String) table3.getValueAt(row,5);
                selection.position.set( (Vector3f) table3.getValueAt(row,6) );
                selection.direction.set( (Vector3f) table3.getValueAt(row,7) );
                selection.velocity.set( (Vector3f) table3.getValueAt(row,8) );
                selection.scale.set( (Vector3f) table3.getValueAt(row,9) );                
                selection.objectDuration = (Float) table3.getValueAt(row,10);               
                selection.mass = (Float) table3.getValueAt(row,11);
                selection.soundfile = (String) table3.getValueAt(row,12);
                selection.soundStartTime = (Float) table3.getValueAt(row,13);
                selection.soundVolume = (Float) table3.getValueAt(row,14);
                  
                selection.objectModel = (String) table3.getValueAt(row,15);
                selection.texture = (String) table3.getValueAt(row,16);                                     
                selection.options = (String) table3.getValueAt(row,18);  
            }//end if
        }//end actionPerformed(ActionEvent)
       };

        ActionListener Pastelistener = new ActionListener(){
        public void actionPerformed(ActionEvent event) {
        
            int row = table3.getSelectedRow();
            if (row != -1) 
            {
                logger.log(Level.FINE,"Paste Data Row: {0} ", new Object[]{row});
                table3.setValueAt(selection.type, row, 1);
                table3.setValueAt(selection.control, row, 2);
                table3.setValueAt(selection.controlTypes.xtype, row, 3);
                table3.setValueAt(selection.controlTypes.ytype, row, 4);
                table3.setValueAt(selection.controlTypes.ztype, row, 5);
                table3.setValueAt(selection.position, row, 6);
                table3.setValueAt(selection.direction, row, 7);
                table3.setValueAt(selection.velocity, row, 8);
                table3.setValueAt(selection.scale, row, 9);
                table3.setValueAt(selection.objectDuration, row, 10);                
                table3.setValueAt(selection.mass, row, 11);
                table3.setValueAt(selection.soundfile, row, 12);
                table3.setValueAt(selection.soundStartTime, row, 13);
                table3.setValueAt(selection.soundVolume, row, 14);
                
                table3.setValueAt(selection.objectModel, row, 15);
                table3.setValueAt(selection.texture, row, 16);                                    
                table3.setValueAt(selection.options, row, 18);  
            }//end if
            
            }//end actionPerformed(ActionEvent)
        };      

       final KeyStroke strokePaste = KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK, false);
       table3.registerKeyboardAction(Pastelistener, "Paste", strokePaste, JComponent.WHEN_FOCUSED);

       final KeyStroke strokeCopy = KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK, false);
       table3.registerKeyboardAction(Copylistener, "Copy", strokeCopy, JComponent.WHEN_FOCUSED);
        
        
        text.setVisible(true);
        tabbedPane.setVisible(true);
        scrollPane1.setVisible(true);       
        comboPanel.setVisible(true);
        comboPanel.setVisible(true);                                         
        guiFrame.setVisible(true);
        
        table1.setRowSelectionAllowed(true);
        ListSelectionModel rowSelection = table1.getSelectionModel();
        rowSelection.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        rowSelection.addListSelectionListener( new ListSelectionListener() {
                    
            public void valueChanged(ListSelectionEvent e) {
                int selectedRow = table1.getSelectedRow();
                if(selectedRow == -1) { return; }
                int selectedChannel = Integer.parseInt(table1.getValueAt(selectedRow, 0).toString());
                
                int t = stageData.getTrackPropertiesMap().floorKey(selectedChannel);
                
                tm2.setMap(stageData.getTrackPropertiesMap().get(t).getNotesMutableProperties());
        
                tm3.setMap(stageData.getTrackPropertiesMap().get(t).getTrackMutableMap());
                
                tm2.fireTableDataChanged();
                tm3.fireTableDataChanged();
            }
        });              
                
        table2.setRowSelectionAllowed(true);
        ListSelectionModel rowSelection2 = table2.getSelectionModel();
        rowSelection2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        rowSelection2.addListSelectionListener( new ListSelectionListener() {
        
            public void valueChanged(ListSelectionEvent e) {
                int selectedRow = table1.getSelectedRow();
                int selectedNote = table2.getSelectedRow();                
                int note = Integer.parseInt(table2.getValueAt(selectedNote, 0).toString());      
                int selectedChannel = Integer.parseInt(table1.getValueAt(selectedRow, 0).toString());
                
                int t = stageData.getTrackPropertiesMap().floorKey(selectedChannel);
                
                if(note == -1)
                {
                    tm2.setMap(stageData.getTrackPropertiesMap().get(t).getNotesMutableProperties());        
                    tm3.setMap(stageData.getTrackPropertiesMap().get(t).getTrackMutableMap());                
                    tm2.fireTableDataChanged();
                    tm3.fireTableDataChanged();
                    return;
                }
                int t2 = stageData.getTrackPropertiesMap().get(t).getNotesMutableProperties().floorKey(note);                
                tm3.setMap(stageData.getTrackPropertiesMap().get(t).getNotesMutableProperties().get(t2));
                tm3.fireTableDataChanged();
            }
        });              
        
        //----------------------------------------------
                
        JMenuBar menuBar = new JMenuBar();
        
        JMenu save = new JMenu("Save");
        menuBar.add(save);
        
        JMenuItem save_item = new JMenuItem("Save");
        save_item.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
              if(fileToSave == null)
              {
                    JOptionPane.showMessageDialog(null, "No file specified!");                    
              }
              else
              {
                   FileWriter fw;
                    try {
                        fw = new FileWriter(fileToSave.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(mStageData.getStageAsStringForOutput());
                        bw.close();
                        JOptionPane.showMessageDialog(null, "Saved!");    
                    } catch (IOException ex) {
                        Logger.getLogger(StageDebugGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                    }                                      
              }
            }
        });
        save.add(save_item);
        
        JMenuItem saveas_item = new JMenuItem("SaveAs");
        saveas_item.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Specify a file to save");   
                int userSelection = fileChooser.showSaveDialog(null);
                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    fileToSave = fileChooser.getSelectedFile();
                    logger.log(Level.FINE,"Save as file: {0} ", new Object[]{fileToSave.getAbsolutePath()});
                    FileWriter fw;
                    try {
                        fw = new FileWriter(fileToSave.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(mStageData.getStageAsStringForOutput());
                        bw.close();
                    } catch (IOException ex) {
                        Logger.getLogger(StageDebugGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                    }                                                                
                }
            }
        });
        save.add(saveas_item);
        
        
        JMenu create = new JMenu("Create");
        menuBar.add(create);
        
        JMenuItem create_channel = new JMenuItem("Channel");
        create_channel.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {         
                String ans = JOptionPane.showInputDialog(null,"Enter insertion channel");
                if(ans.isEmpty()) return;
                final int channel = Integer.parseInt(ans);                                
                if(mStageData.isTrackDefined(channel)){
                    JOptionPane.showMessageDialog(null, "Track already defined!");
                    return; }                                    
                final Callable<Void> function = new Callable<Void>() {                            
                    public Void call() throws Exception {
                          TrackProperties track = new TrackProperties();
                            mStageData.getTrackPropertiesMap().put(channel, track);
                            tm2.fireTableDataChanged();
                            tm3.fireTableDataChanged();
                            tbproptbmod.fireTableDataChanged();
                            return null;
                        }                            
                };
                game.enqueue(function);
            }
        });
        create.add(create_channel);                
        
        JMenu create_measure = new JMenu("Stage Properties");
        menuBar.add(create_measure);
        
        JMenuItem create_camera = new JMenuItem("Camera");
        create_camera.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String t1 = JOptionPane.showInputDialog(null,"Enter insertion measure");
                if(t1.isEmpty()) return;
                final int measure = Integer.parseInt(t1);                
                if(mStageData.getCameraMap().containsKey(measure)){
                    JOptionPane.showMessageDialog(null, "Camera at measure already defined!");
                    return; }     
                
                                                                  
                StageCameraPosition cam = new StageCameraPosition();
                cam.cameraPosition = new Vector3f(mStageData.getScene().getCameraNode().getLocalTranslation());
                
                float[] axis = new float[3];
                mStageData.getScene().getCameraNode().getLocalRotation().toAngles(axis);
                Vector3f temp = new Vector3f(axis[0] * FastMath.RAD_TO_DEG,axis[1] * FastMath.RAD_TO_DEG,axis[2]* FastMath.RAD_TO_DEG);                    
                cam.cameraRotation = temp;
                cam.cameraFrustrum = new Vector3f(mStageData.getCameraMap().get(0).cameraFrustrum);
                mStageData.getCameraMap().put(measure, cam);
                
                tbcammod.fireTableDataChanged();
            }
        });
        create_measure.add(create_camera);
        
                
        JMenuItem create_Tracks = new JMenuItem("Tracks");
        create_Tracks.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String t1 = JOptionPane.showInputDialog(null,"Enter insertion measure");
                if(t1.isEmpty()) return;
                final int measure = Integer.parseInt(t1);         
                if(mStageData.getTracksAvailableMap().containsKey(measure)){
                    JOptionPane.showMessageDialog(null, "NumTracks  at measure already defined!");
                    return; }  
                String t2 = JOptionPane.showInputDialog(null,"Enter number of tracks");
                if(t2.isEmpty()) return;                
                final int num = Integer.parseInt(t2);                                                                    
                mStageData.getTracksAvailableMap().put(measure,num);
                tbtracksmod.fireTableDataChanged();
            }
        });
        create_measure.add(create_Tracks); 
        
        JMenuItem create_texts = new JMenuItem("Texts");
        create_texts.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String t1 = JOptionPane.showInputDialog(null,"Enter insertion measure");
                if(t1.isEmpty()) return;
                final int measure = Integer.parseInt(t1);   
                if(mStageData.getTextsMap().containsKey(measure)){
                    JOptionPane.showMessageDialog(null, "TextsMap at measure already defined!");
                    return; }  
                String text = JOptionPane.showInputDialog(null,"Enter insertion text:");       
                String t2 = JOptionPane.showInputDialog(null,"Enter insertion time");
                if(t2.isEmpty()) return;
                final float time = Float.parseFloat(t2);                
                String t3 = JOptionPane.showInputDialog(null,"Enter insertion size");
                if(t3.isEmpty()) return;
                final int size = Integer.parseInt(t3);                                       
                StageData.Texts t = new StageData.Texts(text,time,size);
                mStageData.getTextsMap().put(measure, t);
                tbtextsmod.fireTableDataChanged();
            }
        });
        create_measure.add(create_texts); 

        
        JMenu create_prop = new JMenu("Channel Properties");
        menuBar.add(create_prop);
        
        JMenuItem create_track = new JMenuItem("Track");                       
        create_track.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                final int selectedRow = table1.getSelectedRow();
                if(selectedRow < 0) return;  
                final int selectedChannel = Integer.parseInt(table1.getValueAt(selectedRow, 0).toString());
                String ans = JOptionPane.showInputDialog(null,"Enter insertion measure");
                if(ans.isEmpty()) return;
                final int measure = Integer.parseInt(ans);                
                                                                                                                     
                final Callable<Void> function = new Callable<Void>() {                            
                    public Void call() throws Exception {
                            String temp2 =  "{" + measure +",NORMAL,PosDirVel(X=" + (selectedChannel *  game.getStageData().getStageGapBetweenTracksSize()) 
                                 + "'Y=1'Z=30;0'0'1;1'1'-50),(1.0'1.0'1.0),200.0,1.0,none,0.0,0.0,box.j3o,NONE}" ;                                                                
                            mStageData.parseMutableProperties(mStageData.getTrackPropertiesMap().get(selectedChannel).getTrackMutableMap(),temp2);                            
                            tm2.fireTableDataChanged();
                            tm3.fireTableDataChanged();
                            return null;
                        }                            
                };
                game.enqueue(function);
            }
        });
        create_prop.add(create_track);     
        
        JMenuItem create_note = new JMenuItem("Note");
        create_note.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                final int selectedRow = table1.getSelectedRow();
                if(selectedRow < 0) return;    
                final int selectedChannel = Integer.parseInt(table1.getValueAt(selectedRow, 0).toString());
                String ans = JOptionPane.showInputDialog(null,"Enter insertion measure");
                if(ans.isEmpty()) return;
                final int measure = Integer.parseInt(ans);                
                String ans2 = JOptionPane.showInputDialog(null,"Enter note number");
                if(ans2.isEmpty()) return;
                final int note = Integer.parseInt(ans2);                                                                                                        
                final Callable<Void> function = new Callable<Void>() {                            
                    public Void call() throws Exception {                                                        
                        int set_channel = selectedChannel;
                        int ms = measure;
                        int set_note = note;
                        StageData.TrackProperties set_trackprop = null;
                        String temp2 ="{" + ms + ",NORMAL,PosDirVel(X=" + (set_channel * game.getStageData().getStageGapBetweenTracksSize()) 
                        + "'Y=1'Z=30;0'0'1;1'1'-50),(1.0'1.0'1.0),200.0,1.0,none,0.0,0.0,box.j3o,NONE}" ;
                            set_trackprop = mStageData.getTrackPropertiesMap().get(set_channel);
                            if(set_trackprop.noteInTrackProperties.containsKey(set_note))
                            { 
                                JOptionPane.showMessageDialog(null,"Track: " + set_channel + " note: " + set_note + " already exists!");
                                 return null;
                            }
                            NavigableMap<Integer,MutableProperties> nMap = new TreeMap<Integer,MutableProperties>();
                            mStageData.parseMutableProperties(nMap,temp2);
                            set_trackprop.noteInTrackProperties.put(set_note, nMap);

                        tm2.fireTableDataChanged();
                        tm3.fireTableDataChanged();
                        return null;
                    }                            
                };
                game.enqueue(function);         
            }
        });
        create_prop.add(create_note);     
        
        
        JMenu addmenu = new JMenu("Add");
        menuBar.add(addmenu);
        JMenuItem addMat = new JMenuItem("Material");
        addMat.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                final String ans = JOptionPane.showInputDialog(null,"Insert material asset");
                if(ans.isEmpty()) return;
                
                final Callable<Void> function = new Callable<Void>() {                            
                    public Void call() throws Exception {                        
                        mStageData.createMaterialFactory(ans);   
                        int index = cbmatmodel.getIndexOf(ans);
                        if ( index == -1 ) {
                           cbmatmodel.addElement(ans);  
                        }                                              
                        return null;
                    }
                };
                game.enqueue(function);                                                
            }
        });
        addmenu.add(addMat);
                        
        JMenuItem addSound = new JMenuItem("Sound");
        addSound.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                    
                final String ans = JOptionPane.showInputDialog(null,"Insert sound asset");
                if(ans.isEmpty()) return;
                
                final Callable<Void> function = new Callable<Void>() {                            
                    public Void call() throws Exception {                        
                        mStageData.createSoundFactory(ans,0,0);   
                        int index = cbsndmodel.getIndexOf(ans);
                        if ( index == -1 ) {
                           cbsndmodel.addElement(ans);  
                        }                                              
                        return null;
                    }
                };
                game.enqueue(function);               
            }
        });
        addmenu.add(addSound);        
        
        JMenuItem addObject = new JMenuItem("Object");
        addObject.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                    
                final String ans = JOptionPane.showInputDialog(null,"Insert object asset");
                if(ans.isEmpty()) return;
                
                final Callable<Void> function = new Callable<Void>() {                            
                    public Void call() throws Exception {                        
                        mStageData.createObjectGeometry(ans);
                        int index = cbgeomodel.getIndexOf(ans);
                        if ( index == -1 ) {
                           cbgeomodel.addElement(ans);  
                        }                                              
                        return null;
                    }
                };
                game.enqueue(function);               
            }
        });
        addmenu.add(addObject);     
                     
        
        JMenu menu = new JMenu("Exit");
        menuBar.add(menu);
        JMenuItem item = new JMenuItem("Exit");
        item.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        menu.add(item);

        guiFrame.setJMenuBar(menuBar);
        menuBar.setVisible(true);
        
        timer = new Timer(1000, null);
        ActionListener listener = new ActionListener() {                       
            public void actionPerformed(ActionEvent e) {
                if(game.getGameStateEnum() == Main.eGameState.GAME) {
                    text.setText(game.getStateManager().getState(GameState.class).getStats());
                }
            }
        };
        timer.addActionListener(listener);
        timer.start();
        
        logger.log(Level.FINE,"End Debug GUI: {0} ", new Object[]{stageData.getLabel()});                                     
        
        loaded = true;
    }
    
    public void exitStage(){   
        if(loaded) {
            timer.stop();
            guiFrame.removeAll();
            guiFrame.setVisible(false);
        }
        loaded = false;
        mStageData = null;
    }
    
    
    
    public void actionPerformed(ActionEvent e) {
//        if ("save".equals(e.getActionCommand())) {
//          saveButton.setEnabled(true);
//           System.out.println("[deBlock3]: save");
//        } 
    }
    
    //------------------
    
    public class CurrentNotesTableModel extends AbstractTableModel {

        protected TreeMap<Integer,NavigableMap<Integer,StageData.MutableProperties>> map;
        protected String[] columnNames;

        public CurrentNotesTableModel() {
            super();
        }


        public CurrentNotesTableModel(TreeMap map) {
            this();
            setMap(map);
        }

        public int getRowCount() {
            return map.size();
        }

        public int getColumnCount() {
            return 1;
        }

        public Object getValueAt(int row, int column) {
            Object[] entries=map.entrySet().toArray();
            if(row == -1) {
                return -1;
            }
            Map.Entry entry=(Map.Entry)entries[row];
            
            if (column==0) {
                return entry.getKey();
            } else {
                throw new IndexOutOfBoundsException("MapTableModel provides a 2-column table, column-index "+column+" is illegal.");
            }
        }
        
        @Override
        public String getColumnName(int column) {
            return "note_key";
        }

        public Map getMap() {
            return map;
        }

        public void setMap(TreeMap<Integer,NavigableMap<Integer,StageData.MutableProperties>> _map) {
            map = _map;
        }
    } 
    
    
    
    //______----------------------------
    
    
        public class MutablePropTableModel extends AbstractTableModel {

        protected NavigableMap<Integer,StageData.MutableProperties> map;       

        String[] columnNames = {"Measure",
                                "type",
                                "control",
                                "modex",
                                "modey",
                                "modez",
                                "pos",
                                "vel",
                                "dir",
                                "scale",
                                "duration",                                
                                "mass",
                                "sound",
                                "sndSrt",
                                "sndVol",
                                "object",
                                "texture", "skip", "options"};

        public MutablePropTableModel() { super(); }

        public MutablePropTableModel(NavigableMap map) {
            this(); setMap(map); }
        
        public int getRowCount() { return map.size(); }

        public int getColumnCount() { return columnNames.length; }

        @Override
        public String getColumnName(int col) { return columnNames[col];}
        
        public Object getValueAt(int row, int column) {
            Object[] entries=map.entrySet().toArray();
            Map.Entry entry=(Map.Entry)entries[row];
            MutableProperties prop = (MutableProperties)entry.getValue();
            if (column==0) {
                return entry.getKey();
            } else if (column==1) { // column==1
                return prop.type;
            } else if (column==2) { // column==1
                return prop.control;
            
            } else if (column==3) { // column==1
                return prop.controlTypes.xtype;
            } else if (column==4) { // column==1
                return prop.controlTypes.ytype;
            } else if (column==5) { // column==1
                return prop.controlTypes.ztype;
            }
            
            else if (column==6) { // column==1
                return prop.position;
            } else if (column==7) { // column==1
                return prop.direction;
            } else if (column==8) { // column==1
                return prop.velocity;
            } else if (column==9) { // column==1
                return prop.scale;
            } 
            
            
            else if (column==10) { // column==1
                return prop.objectDuration;           
            } else if (column==11) { // column==1
                return prop.mass;
            } else if (column==12) { // column==1
                return prop.soundfile;
                 } else if (column==13) { // column==1
                return prop.soundStartTime;
                 } else if (column==14) { // column==1
                return prop.soundVolume;
                
            } else if (column==15) { // column==1
                return prop.objectModel;
            } else if (column==16) { // column==1
                return prop.texture;
            } else if (column==17) { // column==1
                return prop.skip;            
              } else if (column==18) { // column==1
                return prop.options;
            }
            else {
                throw new IndexOutOfBoundsException("MapTableModel provides a 2-column table, column-index "+column+" is illegal.");
            }
        }
        
        @Override
        public void setValueAt(Object value, int row, int column) {
             Object[] entries=map.entrySet().toArray();
            Map.Entry entry=(Map.Entry)entries[row];
            final MutableProperties prop = (MutableProperties)entry.getValue();
            if (column==0) { // column==1
                int t = Integer.parseInt(value.toString());
                
                if(prop.measure != 0 && t >= 0)
                {
                    map.put(t, prop);
                    map.remove(prop.measure);
                    fireTableDataChanged();
                }
            }            
            else if (column==1) { // column==1
                prop.type = value.toString();
            } else if (column==2) { // column==1
                prop.control = value.toString();
            } 
            
            else if (column==3) { // column==1
                prop.getControlTypes().xtype = value.toString();
            } else if (column==4) { // column==1
                prop.getControlTypes().ytype= value.toString();
            } else if (column==5) { // column==1
                prop.getControlTypes().ztype= value.toString();
            }
            
            else if (column==6) { // column==1
                prop.position.set( (Vector3f)value );
            } else if (column==7) { // column==1
                prop.direction.set( (Vector3f)value );
            } else if (column==8) { // column==1
                prop.velocity.set( (Vector3f)value );
            }            
            
            else if (column==9) { // column==1
                prop.scale.set( (Vector3f)value );
            }  else if (column==10) { // column==1
                prop.objectDuration = Float.parseFloat(value.toString());
          
            } else if (column==11) { // column==1
                prop.mass = Float.parseFloat(value.toString());
            } else if (column==12) { // column==1
                prop.soundfile = value.toString();
                final Callable<Void> function = new Callable<Void>() {                            
                    public Void call() throws Exception {                                      
                        prop.sound = mStageData.soundsMap.get(prop.soundfile);
                        return null;
                    }
                };
                game.enqueue(function);       
                  } else if (column==13) { // column==1
                    prop.soundStartTime = Float.parseFloat(value.toString());
                  } else if (column==14) { // column==1
                    prop.soundVolume = Float.parseFloat(value.toString());                   
            } else if (column==15) { // column==1
                prop.objectModel = value.toString();
                final Callable<Void> function = new Callable<Void>() {                            
                    public Void call() throws Exception {                                      
                        prop.objectGeometry = mStageData.geometryMap.get(prop.objectModel);
                        return null;
                    }
                };
                game.enqueue(function);       
            } else if (column==16) { // column==1
                prop.texture = value.toString();
                    final Callable<Void> function = new Callable<Void>() {                            
                    public Void call() throws Exception {                                      
                        prop.material = mStageData.materialsMap.get(prop.texture);
                        return null;
                    }
                };
                game.enqueue(function);      
            } else if (column==17) { // column==1
                prop.skip = Boolean.parseBoolean(value.toString());
            } else if (column==18) { // column==1
                prop.options = value.toString().toUpperCase();
            }
            fireTableCellUpdated(row, column); 
        }
        
         @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

          @Override
        public boolean isCellEditable(int row, int col) {
            return true;
        }
        
          
        public NavigableMap getMap() {
            return map;
        }

        public void setMap(NavigableMap<Integer,StageData.MutableProperties> _map) {
            map = _map;
        }
    } 
        
public class TrackPropTableModel extends AbstractTableModel {

        protected TreeMap<Integer, StageData.TrackProperties> map;       

        String[] columnNames = {"Channel",
                                "minRange",
                                "maxRange"};

        public TrackPropTableModel() { super(); }

        public TrackPropTableModel(TreeMap<Integer, StageData.TrackProperties> map) {
            this(); setMap(map); }
        
        public int getRowCount() { return map.size(); }

        public int getColumnCount() { return columnNames.length; }

        @Override
        public String getColumnName(int col) { return columnNames[col];}
        
        public Object getValueAt(int row, int column) {
            Object[] entries=map.entrySet().toArray();
            Map.Entry entry=(Map.Entry)entries[row];
            TrackProperties prop = (TrackProperties)entry.getValue();
            if (column==0) {
                return entry.getKey();
            } else if (column==1) { // column==1
                return prop.trackMinMaxNotes.x;
            } else if (column==2) { // column==1
                return prop.trackMinMaxNotes.y;
            } else {
                throw new IndexOutOfBoundsException("MapTableModel provides a 2-column table, column-index "+column+" is illegal.");
            }
        }
        
        @Override
        public void setValueAt(Object value, int row, int column) {
             Object[] entries=map.entrySet().toArray();
            Map.Entry entry=(Map.Entry)entries[row];
            TrackProperties prop = (TrackProperties)entry.getValue();
            if (column==1) { // column==1
                prop.trackMinMaxNotes.x = Float.parseFloat(value.toString());
            } else if (column==2) { // column==1
               prop.trackMinMaxNotes.y = Float.parseFloat(value.toString());
            } 
            fireTableCellUpdated(row, column);
 
        }
        
         @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

          @Override
        public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col < 1) {
                return false;
            } else {
                return true;
            }
        }
        
          
        public NavigableMap getMap() {
            return map;
        }

        public void setMap(TreeMap<Integer, StageData.TrackProperties> _map) {
            map = _map;
        }
    }         

    //--------------------------------

    public class CameraPosTableModel extends AbstractTableModel {

        protected NavigableMap<Integer, StageData.StageCameraPosition> map;       

        String[] columnNames = {"Measure",
                                "PosX", "PosY", "PosZ",
                                "RotX", "RotY" , "RotZ",
                                "FOV", "AR"};

        public CameraPosTableModel() { super(); }

        public CameraPosTableModel(NavigableMap<Integer, StageData.StageCameraPosition> map) {
            this(); setMap(map); }
        
        public int getRowCount() { return map.size(); }

        public int getColumnCount() { return columnNames.length; }

        @Override
        public String getColumnName(int col) { return columnNames[col];}
        
        public Object getValueAt(int row, int column) {
            Object[] entries=map.entrySet().toArray();
            Map.Entry entry=(Map.Entry)entries[row];
            StageData.StageCameraPosition lc = (StageCameraPosition)entry.getValue();
            if (column==0) {
                return entry.getKey();
            } else if (column==1) { // column==1
                return lc.cameraPosition.x;
            } else if (column==2) { // column==1
                return lc.cameraPosition.y;
            } else if (column==3) { // column==1
                return lc.cameraPosition.z;
            } else if (column==4) { // column==1
                return lc.cameraRotation.x;
            } else if (column==5) { // column==1
                return lc.cameraRotation.y;
            } else if (column==6) { // column==1
                return lc.cameraRotation.z;
            } else if (column==7) { // column==1
                return lc.cameraFrustrum.x;
            } else if (column==8) { // column==1
                return lc.cameraFrustrum.y;
            
            }else {
                throw new IndexOutOfBoundsException("MapTableModel provides a 2-column table, column-index "+column+" is illegal.");
            }
        }
        
        @Override
        public void setValueAt(Object value, int row, int column) {
             Object[] entries=map.entrySet().toArray();
            Map.Entry entry=(Map.Entry)entries[row];
            StageData.StageCameraPosition lc = (StageCameraPosition)entry.getValue();
            if (column==0) { // column==1
                int t = Integer.parseInt(value.toString());
                int t2 = Integer.parseInt(entry.getKey().toString());
                
                if( t2 != 0 && t != 0)
                {
                    map.put(t, lc);
                    map.remove(t2);
                }
            }            
            else if (column==1) { // column==1
               lc.cameraPosition.x = (Float) value ;
            } else if (column==2) { // column==1
                lc.cameraPosition.y = (Float) value ;
            } else if (column==3) { // column==1
                lc.cameraPosition.z = (Float) value ;
            } else if (column==4) { // column==1
               lc.cameraRotation.x = (Float) value ;
            } else if (column==5) { // column==1
                lc.cameraRotation.y = (Float) value ;
            } else if (column==6) { // column==1
                lc.cameraRotation.z = (Float) value ;
            } else if (column==7) { // column==1
               lc.cameraFrustrum.x = (Float) value ;
            } else if (column==8) { // column==1
                lc.cameraFrustrum.y = (Float) value ;
            }
            
            fireTableCellUpdated(row, column);
            
        }
        
         @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

          @Override
        public boolean isCellEditable(int row, int col) {
                return true;            
        }
        
          
        public NavigableMap getMap() {
            return map;
        }

        public void setMap(NavigableMap<Integer, StageData.StageCameraPosition> _map) {
            map = _map;
        }
    }     
    
     //--------------------------------

   public class TracksTableModel extends AbstractTableModel {
         protected NavigableMap<Integer, Integer> map;       

        String[] columnNames = {"Measure",
                                "Tracks"};

        public TracksTableModel() { super(); }

        public TracksTableModel(NavigableMap<Integer, Integer> map) {
            this(); setMap(map); }
        
        public int getRowCount() { return map.size(); }

        public int getColumnCount() { return columnNames.length; }

        @Override
        public String getColumnName(int col) { return columnNames[col];}
        
        public Object getValueAt(int row, int column) {
            Object[] entries=map.entrySet().toArray();
            Map.Entry entry=(Map.Entry)entries[row];            
            if (column==0) {
                return entry.getKey();
            } else if (column==1) { // column==1
                return entry.getValue().toString();            
            }else {
                throw new IndexOutOfBoundsException("MapTableModel provides a 2-column table, column-index "+column+" is illegal.");
            }
        }
        
        @Override
        public void setValueAt(Object value, int row, int column) {
            Object[] entries=map.entrySet().toArray();
            Map.Entry entry=(Map.Entry)entries[row];
            Integer lc = Integer.parseInt(entry.getValue().toString());
            if (column==0) { // column==1
                int t = Integer.parseInt(value.toString());
                int t2 = Integer.parseInt(entry.getKey().toString());
                
                if( t2 != 0 && t != 0 && t2 != t)
                {
                    map.put(t, lc);
                    map.remove(t2);
                }
            }            
            else if (column==1) { // column==1
               entry.setValue((Integer) Integer.parseInt(value.toString()));            
            } 
            fireTableCellUpdated(row, column);
            
        }
        
         @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

          @Override
        public boolean isCellEditable(int row, int col) {
                return true;            
        }
        
          
        public NavigableMap getMap() {
            return map;
        }

        public void setMap(NavigableMap<Integer, Integer> _map) {
            map = _map;
        }         
    } 
   
      public class TextsTrackTableModel extends AbstractTableModel {

        protected NavigableMap<Integer, StageData.Texts> map;       

        String[] columnNames = {"Measure",
                                "Text",
                                "time" ,
                                "size"};

        public TextsTrackTableModel() { super(); }

        public TextsTrackTableModel(NavigableMap<Integer, StageData.Texts> map) {
            this(); setMap(map); }
        
        public int getRowCount() { return map.size(); }

        public int getColumnCount() { return columnNames.length; }

        @Override
        public String getColumnName(int col) { return columnNames[col];}
        
        public Object getValueAt(int row, int column) {
            Object[] entries=map.entrySet().toArray();
            Map.Entry entry=(Map.Entry)entries[row];
            StageData.Texts lc = (Texts)entry.getValue();
            if (column==0) {
                return entry.getKey();
            } else if (column==1) { // column==1
                return lc.text;
            } else if (column==2) { // column==1
                return lc.time;
            } else if (column==3) { // column==1
                return lc.size;
            }else {
                throw new IndexOutOfBoundsException("MapTableModel provides a 2-column table, column-index "+column+" is illegal.");
            }
        }
        
        @Override
        public void setValueAt(Object value, int row, int column) {
            Object[] entries=map.entrySet().toArray();
            Map.Entry entry=(Map.Entry)entries[row];
            StageData.Texts lc = (Texts)entry.getValue();
            if (column==0) { // column==1
                int t = Integer.parseInt(value.toString());
                int t2 = Integer.parseInt(entry.getKey().toString());
                
                if( t2 != 0 && t != 0 && t2 != t)
                {
                    map.put(t, lc);
                    map.remove(t2);
                }
            }            
            else if (column==1) { // column==1
               lc.text = value.toString().replace(",", ".");
            } else if (column==2) { // column==1
                lc.time = Float.parseFloat(value.toString());
            } else if (column==3) { // column==1
                lc.size = Integer.parseInt(value.toString());
            } 
            fireTableCellUpdated(row, column);
            
        }
        
         @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

          @Override
        public boolean isCellEditable(int row, int col) {
                return true;            
        }
        
          
        public NavigableMap getMap() {
            return map;
        }

        public void setMap(NavigableMap<Integer, StageData.Texts> _map) {
            map = _map;
        }
    }     
    

}    
    
