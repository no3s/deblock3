/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ingamedebug;

import com.jme3.math.Vector3f;
import static ingamedebug.Vector3fCellEditor.EDIT;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author no3z
 */
public class Vector3fCellEditor extends AbstractCellEditor
                                implements TableCellEditor,
                                    ActionListener {    

    Vector3f currentVector;
    JButton button;
    JDialog dialog;
    Vector3Dialog vectorEditor;
    static final String EDIT = "edit";
    
    public Vector3fCellEditor()
    {
        button = new JButton();
        button.setBackground(Color.WHITE);
        button.setActionCommand(EDIT);
        button.addActionListener(this);
        button.setBorderPainted(false);
        
        vectorEditor = new Vector3Dialog(null, false, this, this);        
        currentVector = new Vector3f();
    }
    
    public Object getCellEditorValue() {
        vectorEditor.setupDialog(currentVector, "EDIT");
        return currentVector;
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        currentVector = (Vector3f) value ;
        return button;
    }

    public void actionPerformed(ActionEvent e) {
        if (EDIT.equals(e.getActionCommand()))
        {
            vectorEditor.setVisible(true);

            fireEditingStopped();
        }
         else {             
             //currentVector.set(vectorEditor.getEditedVector());
             vectorEditor.setVisible(false);     
            fireEditingStopped();
         }        
    }

}
