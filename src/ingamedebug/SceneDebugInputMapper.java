package ingamedebug;

import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import deBlock3.StageData;
import deBlock3.SceneAbstract;

public class SceneDebugInputMapper implements AnalogListener { 

    SceneAbstract scene;

    public Vector3f cam_pos = new Vector3f();
    public Vector3f cam_dir ;
    public Vector2f frustrum = new Vector2f();
        
    public SceneDebugInputMapper(SceneAbstract scene, Vector3f cam_direction, float f1, float f2){
        this.scene = scene;
        cam_pos = scene.getCameraNode().getLocalTranslation();
        cam_dir = new Vector3f(cam_direction);
        frustrum.x = f1;
        frustrum.y = f2;
        setUpKeys();        
    }
    
     private void setUpKeys() {
        InputManager inputManager = scene.getGame().getInputManager();
        
        inputManager.addMapping("cam_pos_xp", new KeyTrigger(KeyInput.KEY_R));        
        inputManager.addMapping("cam_pos_xn", new KeyTrigger(KeyInput.KEY_T));
        inputManager.addMapping("cam_pos_yp", new KeyTrigger(KeyInput.KEY_Y));        
        inputManager.addMapping("cam_pos_yn", new KeyTrigger(KeyInput.KEY_U));
        inputManager.addMapping("cam_pos_zp", new KeyTrigger(KeyInput.KEY_I));        
        inputManager.addMapping("cam_pos_zn", new KeyTrigger(KeyInput.KEY_O));
        
        inputManager.addMapping("cam_dir_xp", new KeyTrigger(KeyInput.KEY_F));        
        inputManager.addMapping("cam_dir_xn", new KeyTrigger(KeyInput.KEY_G));
        inputManager.addMapping("cam_dir_yp", new KeyTrigger(KeyInput.KEY_H));        
        inputManager.addMapping("cam_dir_yn", new KeyTrigger(KeyInput.KEY_J));
        inputManager.addMapping("cam_dir_zp", new KeyTrigger(KeyInput.KEY_K));        
        inputManager.addMapping("cam_dir_zn", new KeyTrigger(KeyInput.KEY_L));
        
        inputManager.addMapping("frustrum_1p", new KeyTrigger(KeyInput.KEY_V));        
        inputManager.addMapping("frustrum_1n", new KeyTrigger(KeyInput.KEY_B));        
        inputManager.addMapping("frustrum_2p", new KeyTrigger(KeyInput.KEY_N));        
        inputManager.addMapping("frustrum_2n", new KeyTrigger(KeyInput.KEY_M));
        inputManager.addMapping("die", new KeyTrigger(KeyInput.KEY_END));
        
        inputManager.addMapping("ddp", new KeyTrigger(KeyInput.KEY_4));
        inputManager.addMapping("ddn", new KeyTrigger(KeyInput.KEY_5));
        
        inputManager.addMapping("info", new KeyTrigger(KeyInput.KEY_SPACE));
                
        inputManager.addListener(this, "cam_pos_xp");
        inputManager.addListener(this, "cam_pos_xn");
        inputManager.addListener(this, "cam_pos_yp");        
        inputManager.addListener(this, "cam_pos_yn");
        inputManager.addListener(this, "cam_pos_zp");
        inputManager.addListener(this, "cam_pos_zn");
        
        inputManager.addListener(this, "cam_dir_xp");
        inputManager.addListener(this, "cam_dir_xn");
        inputManager.addListener(this, "cam_dir_yp");        
        inputManager.addListener(this, "cam_dir_yn");
        inputManager.addListener(this, "cam_dir_zp");
        inputManager.addListener(this, "cam_dir_zn");
                inputManager.addListener(this, "die");
        inputManager.addListener(this, "ddp");
        inputManager.addListener(this, "ddn");        
                
        inputManager.addListener(this, "frustrum_1p");
        inputManager.addListener(this, "frustrum_1n");                  
        inputManager.addListener(this, "frustrum_2p");
        inputManager.addListener(this, "frustrum_2n");  
        
        inputManager.addListener(this, "info"); 
    }                         
            
    public void onAnalog(String binding, float value, float tpf) {
            if (value < 0) {
                return;
            }
            
              StageData.StageCameraPosition cam = scene.getStageData().getCameraPosition(scene.getMeasure());                               

            if (binding.equals("cam_pos_xp"))    {cam_pos.x += .01f;scene.getCameraNode().setLocalTranslation(cam_pos);}
            else if(binding.equals("cam_pos_xn")){cam_pos.x -= .01f;scene.getCameraNode().setLocalTranslation(cam_pos);}
            else if(binding.equals("cam_pos_yp")){cam_pos.y += .01f;scene.getCameraNode().setLocalTranslation(cam_pos);}
            else if(binding.equals("cam_pos_yn")){cam_pos.y -= .01f;scene.getCameraNode().setLocalTranslation(cam_pos);}
            else if(binding.equals("cam_pos_zp")){cam_pos.z += .01f;scene.getCameraNode().setLocalTranslation(cam_pos);}
            else if(binding.equals("cam_pos_zn")){cam_pos.z -= .01f;scene.getCameraNode().setLocalTranslation(cam_pos);}
            
            else if(binding.equals("cam_dir_xp")){cam_dir.x += .025f;scene.getCameraNode().setLocalRotation(new Quaternion().fromAngles(cam_dir.x * FastMath.DEG_TO_RAD,cam_dir.y* FastMath.DEG_TO_RAD,cam_dir.z* FastMath.DEG_TO_RAD));}
            else if(binding.equals("cam_dir_xn")){cam_dir.x -= .025f;scene.getCameraNode().setLocalRotation(new Quaternion().fromAngles(cam_dir.x * FastMath.DEG_TO_RAD,cam_dir.y* FastMath.DEG_TO_RAD,cam_dir.z* FastMath.DEG_TO_RAD));}
            else if(binding.equals("cam_dir_yp")){cam_dir.y += .025f;scene.getCameraNode().setLocalRotation(new Quaternion().fromAngles(cam_dir.x * FastMath.DEG_TO_RAD,cam_dir.y* FastMath.DEG_TO_RAD,cam_dir.z* FastMath.DEG_TO_RAD));}
            else if(binding.equals("cam_dir_yn")){cam_dir.y -= .025f;scene.getCameraNode().setLocalRotation(new Quaternion().fromAngles(cam_dir.x * FastMath.DEG_TO_RAD,cam_dir.y* FastMath.DEG_TO_RAD,cam_dir.z* FastMath.DEG_TO_RAD));}
            else if(binding.equals("cam_dir_zp")){cam_dir.z += .025f;scene.getCameraNode().setLocalRotation(new Quaternion().fromAngles(cam_dir.x * FastMath.DEG_TO_RAD,cam_dir.y* FastMath.DEG_TO_RAD,cam_dir.z* FastMath.DEG_TO_RAD));}
            else if(binding.equals("cam_dir_zn")){cam_dir.z -= .025f;scene.getCameraNode().setLocalRotation(new Quaternion().fromAngles(cam_dir.x * FastMath.DEG_TO_RAD,cam_dir.y* FastMath.DEG_TO_RAD,cam_dir.z* FastMath.DEG_TO_RAD));}
            
            else if(binding.equals("ddp")){scene.debugDelay += 0.025f;}
            else if(binding.equals("ddn")){scene.debugDelay -= 0.025f;}   
            else if(binding.equals("frustrum_1p")){
                frustrum.x += 0.0025f;
                scene.getCameraNode().getCamera().setFrustumPerspective(frustrum.x,frustrum.y,0.01f,1000.0f);
            }
            else if(binding.equals("frustrum_1n")){
                frustrum.x -= 0.0025f;
                scene.getCameraNode().getCamera().setFrustumPerspective(frustrum.x,frustrum.y,0.01f,1000.0f);            
            }
            else if(binding.equals("frustrum_2p")){
                frustrum.y += 0.00025f;
                scene.getCameraNode().getCamera().setFrustumPerspective(frustrum.x,frustrum.y,0.01f,1000.0f);
            }
            else if(binding.equals("frustrum_2n")){
                frustrum.y -= 0.00025f;
                scene.getCameraNode().getCamera().setFrustumPerspective(frustrum.x,frustrum.y,0.01f,1000.0f);            
            }
            else if(binding.equals("info")){                                              
                System.out.println("[deBlock3]: " + "{" + scene.getMeasure() +"," + cam_pos.toString() + " rotation: " + cam_dir.toString() + " frus: " + frustrum.toString() + " ,0.0}");                                                             
            }   
            else if(binding.equals("die")){                                              
                scene.getPlayer().die();
            }   
            cam.cameraPosition.x = cam_pos.x;
              
            cam.cameraPosition.y = cam_pos.y;

            cam.cameraPosition.z = cam_pos.z;

            cam.cameraRotation.x = cam_dir.x;

            cam.cameraRotation.y = cam_dir.y;

            cam.cameraRotation.z = cam_dir.z;

            cam.cameraFrustrum.x = frustrum.x;

            cam.cameraFrustrum.y = frustrum.y;   
    }

        
}
